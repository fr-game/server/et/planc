using ETModel;

namespace ETHotfix
{
	[Config(AppType.ClientM )]
	public partial class EquipDataNumericConfigCategory : ACategory<EquipDataNumericConfig>
	{
	}

	public class EquipDataNumericConfig: IConfig
	{
		public long Id { get; set; }
		public string FieldName;
		public string Name;
		public string Desc;
		public double MinValue;
		public double MaxValue;
	}
}
