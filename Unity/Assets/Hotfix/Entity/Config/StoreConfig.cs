using ETModel;

namespace ETHotfix
{
	[Config(AppType.ClientM)]
	public partial class StoreConfigCategory : ACategory<StoreConfig>
	{
	}

	public class StoreConfig: IConfig
	{
		public long Id { get; set; }
		public string StoreName;
		public double TaxRate;
		public long[] ItemConfigList;
	}
}
