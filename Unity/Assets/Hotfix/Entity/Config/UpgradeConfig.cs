using ETModel;

namespace ETHotfix
{
	[Config(AppType.ClientM)]
	public partial class UpgradeConfigCategory : ACategory<UpgradeConfig>
	{
	}

	public class UpgradeConfig: IConfig
	{
		public long Id { get; set; }
		public long[] NeedItemList;
		public double Probability;
		public int Gold;
	}
}
