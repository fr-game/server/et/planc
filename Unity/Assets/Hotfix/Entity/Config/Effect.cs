using ETModel;

namespace ETHotfix
{
	[Config(AppType.ClientM)]
	public partial class EffectCategory : ACategory<Effect>
	{
	}

	public class Effect: IConfig
	{
		public long Id { get; set; }
		public string ChangeFieldName;
		public string ChangeField;
		public double ChangeValue;
		public long BufferId;
	}
}
