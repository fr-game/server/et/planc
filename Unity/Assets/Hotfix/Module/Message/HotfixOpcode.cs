using ETModel;
namespace ETHotfix
{
	[Message(HotfixOpcode.C2R_Login)]
	public partial class C2R_Login : IRequest {}

	[Message(HotfixOpcode.R2C_Login)]
	public partial class R2C_Login : IResponse {}

	[Message(HotfixOpcode.C2G_LoginGate)]
	public partial class C2G_LoginGate : IRequest {}

	[Message(HotfixOpcode.G2C_LoginGate)]
	public partial class G2C_LoginGate : IResponse {}

	[Message(HotfixOpcode.G2C_TestHotfixMessage)]
	public partial class G2C_TestHotfixMessage : IMessage {}

	[Message(HotfixOpcode.C2M_TestActorRequest)]
	public partial class C2M_TestActorRequest : IActorLocationRequest {}

	[Message(HotfixOpcode.M2C_TestActorResponse)]
	public partial class M2C_TestActorResponse : IActorLocationResponse {}

	[Message(HotfixOpcode.PlayerInfo)]
	public partial class PlayerInfo : IMessage {}

	[Message(HotfixOpcode.C2G_PlayerInfo)]
	public partial class C2G_PlayerInfo : IRequest {}

	[Message(HotfixOpcode.G2C_PlayerInfo)]
	public partial class G2C_PlayerInfo : IResponse {}

///<summary>
/// unit获取背包请求
///</summary>
	[Message(HotfixOpcode.Actor_Unit_GetInventoryInfosRequest)]
	public partial class Actor_Unit_GetInventoryInfosRequest : IActorLocationRequest {}

///<summary>
/// unit获取背包响应
///</summary>
	[Message(HotfixOpcode.Actor_Unit_GetInventoryInfosResponse)]
	public partial class Actor_Unit_GetInventoryInfosResponse : IActorLocationResponse {}

///<summary>
///背包物品信息
/// Item的传输：推送端将Item的各组件编码成bytes，放入map中。map中键为组件名，值为组件的bytes。接收方收到后将bytes解码成组件对象挂到Item中。
///</summary>
	[Message(HotfixOpcode.ItemInfo)]
	public partial class ItemInfo : IMessage {}

///<summary>
///Item的组件信息
///</summary>
	[Message(HotfixOpcode.ComponentData)]
	public partial class ComponentData : IMessage {}

///<summary>
/// unit给背包添加物品请求
///</summary>
	[Message(HotfixOpcode.Actor_Unit_AddInventoryItemRequest)]
	public partial class Actor_Unit_AddInventoryItemRequest : IActorLocationRequest {}

///<summary>
/// unit给背包添加物品响应
///</summary>
	[Message(HotfixOpcode.Actor_Unit_AddInventoryItemResponse)]
	public partial class Actor_Unit_AddInventoryItemResponse : IActorLocationResponse {}

///<summary>
/// unit给背包移除物品请求
///</summary>
	[Message(HotfixOpcode.Actor_Unit_RemoveInventoryItemRequest)]
	public partial class Actor_Unit_RemoveInventoryItemRequest : IActorLocationRequest {}

///<summary>
/// unit给背包移除物品响应
///</summary>
	[Message(HotfixOpcode.Actor_Unit_RemoveInventoryItemResponse)]
	public partial class Actor_Unit_RemoveInventoryItemResponse : IActorLocationResponse {}

///<summary>
/// unit给背包整理请求
///</summary>
	[Message(HotfixOpcode.Actor_Unit_SortBagRequest)]
	public partial class Actor_Unit_SortBagRequest : IActorMessage {}

///<summary>
/// unit给背包更改金币请求
///</summary>
	[Message(HotfixOpcode.Actor_Unit_AddGoldRequest)]
	public partial class Actor_Unit_AddGoldRequest : IActorLocationRequest {}

///<summary>
/// unit给背包更改金币响应
///</summary>
	[Message(HotfixOpcode.Actor_Unit_AddGoldResponse)]
	public partial class Actor_Unit_AddGoldResponse : IActorLocationResponse {}

///<summary>
/// 请求地图的物品信息请求
///</summary>
	[Message(HotfixOpcode.C2M_GetWorldItemRequest)]
	public partial class C2M_GetWorldItemRequest : IRequest {}

///<summary>
/// 请求地图的物品信息响应
///</summary>
	[Message(HotfixOpcode.M2C_GetWorldItemResponse)]
	public partial class M2C_GetWorldItemResponse : IResponse {}

///<summary>
/// 广播世界物品信息
///</summary>
	[Message(HotfixOpcode.M2C_BroadcastWorldItem)]
	public partial class M2C_BroadcastWorldItem : IMessage {}

///<summary>
/// 广播世界物品删除信息
///</summary>
	[Message(HotfixOpcode.M2C_BroadcastWorldRemove)]
	public partial class M2C_BroadcastWorldRemove : IMessage {}

///<summary>
/// 世界物品加锁解锁请求
///</summary>
	[Message(HotfixOpcode.Actor_Item_LockingWorldItemRequest)]
	public partial class Actor_Item_LockingWorldItemRequest : IActorLocationRequest {}

///<summary>
/// 世界物品加锁解锁响应
///</summary>
	[Message(HotfixOpcode.Actor_Item_LockingWorldItemResponse)]
	public partial class Actor_Item_LockingWorldItemResponse : IActorLocationResponse {}

///<summary>
/// 使用世界物品请求 ,这个时候删除item或者标记为已经使用。然后通知其他客户端
///</summary>
	[Message(HotfixOpcode.Actor_Item_UsingWorldItemRequest)]
	public partial class Actor_Item_UsingWorldItemRequest : IActorLocationRequest {}

///<summary>
/// 使用世界物品响应
///</summary>
	[Message(HotfixOpcode.Actor_Item_UsingWorldItemResponse)]
	public partial class Actor_Item_UsingWorldItemResponse : IActorLocationResponse {}

///<summary>
/// 客户端发起交易请求
///</summary>
	[Message(HotfixOpcode.C2M_InitTradeRequest)]
	public partial class C2M_InitTradeRequest : IRequest {}

///<summary>
/// 客户端发起交易响应
///</summary>
	[Message(HotfixOpcode.M2C_InitTradeResponse)]
	public partial class M2C_InitTradeResponse : IResponse {}

///<summary>
/// 服务端通知被交易人
///</summary>
	[Message(HotfixOpcode.M2C_NoticeInitTrade)]
	public partial class M2C_NoticeInitTrade : IActorMessage {}

//string SourcePlayerIdName = 2;//交易发起人姓名
///<summary>
/// 客户端回应是否同意开始交易。请求
///</summary>
	[Message(HotfixOpcode.C2M_AnswerAgreeTradeRequest)]
	public partial class C2M_AnswerAgreeTradeRequest : IRequest {}

///<summary>
/// 客户端回应是否同意开始交易。响应
///</summary>
	[Message(HotfixOpcode.M2C_AnswerAgreeTradeResponse)]
	public partial class M2C_AnswerAgreeTradeResponse : IResponse {}

///<summary>
/// 服务端广播交易开始给两个交易者。
///</summary>
	[Message(HotfixOpcode.M2C_BroadcastTradeStart)]
	public partial class M2C_BroadcastTradeStart : IActorMessage {}

///<summary>
/// 客户端取消交易请求
///</summary>
	[Message(HotfixOpcode.C2M_CancelTradeRequest)]
	public partial class C2M_CancelTradeRequest : IRequest {}

///<summary>
/// 客户端取消交易响应
///</summary>
	[Message(HotfixOpcode.M2C_CancelTradeResponse)]
	public partial class M2C_CancelTradeResponse : IResponse {}

///<summary>
/// 服务端通知另一个交易参与者交易取消了。
///</summary>
	[Message(HotfixOpcode.M2C_NoticeTradeCanceled)]
	public partial class M2C_NoticeTradeCanceled : IActorMessage {}

///<summary>
///客户端放置交易商品和金币
///</summary>
	[Message(HotfixOpcode.C2M_PutTradeCommodityRequest)]
	public partial class C2M_PutTradeCommodityRequest : IRequest {}

///<summary>
///客户端放置交易商品和金币响应
///</summary>
	[Message(HotfixOpcode.M2C_PutTradeCommodityResponse)]
	public partial class M2C_PutTradeCommodityResponse : IResponse {}

///<summary>
///服务端通另一个交易参与者交易消息
///</summary>
	[Message(HotfixOpcode.M2C_NoticePutTradeCommodity)]
	public partial class M2C_NoticePutTradeCommodity : IActorMessage {}

///<summary>
///交易货物
///</summary>
	[Message(HotfixOpcode.Commodity)]
	public partial class Commodity : IMessage {}

///<summary>
///客户端确认交易请求
///</summary>
	[Message(HotfixOpcode.C2M_AffirmTradeRequest)]
	public partial class C2M_AffirmTradeRequest : IRequest {}

///<summary>
///客户端确认交易响应
///</summary>
	[Message(HotfixOpcode.M2C_AffirmTradeResponse)]
	public partial class M2C_AffirmTradeResponse : IResponse {}

///<summary>
///服务端通知客户端确认交易请求
///</summary>
	[Message(HotfixOpcode.M2C_NoticeAffirmTrade)]
	public partial class M2C_NoticeAffirmTrade : IActorMessage {}

///<summary>
///服务端通知客户端交易结束,开始增加/减少 Item和金币。
///</summary>
	[Message(HotfixOpcode.M2C_NoticeTradeResult)]
	public partial class M2C_NoticeTradeResult : IActorMessage {}

///<summary>
///商店购买物品
///</summary>
	[Message(HotfixOpcode.Actor_Unit_BuyItemRequest)]
	public partial class Actor_Unit_BuyItemRequest : IActorMessage {}

///<summary>
///商店出售物品
///</summary>
	[Message(HotfixOpcode.Actor_Unit_SalesItemRequest)]
	public partial class Actor_Unit_SalesItemRequest : IActorMessage {}

}
namespace ETHotfix
{
	public static partial class HotfixOpcode
	{
		 public const ushort C2R_Login = 10001;
		 public const ushort R2C_Login = 10002;
		 public const ushort C2G_LoginGate = 10003;
		 public const ushort G2C_LoginGate = 10004;
		 public const ushort G2C_TestHotfixMessage = 10005;
		 public const ushort C2M_TestActorRequest = 10006;
		 public const ushort M2C_TestActorResponse = 10007;
		 public const ushort PlayerInfo = 10008;
		 public const ushort C2G_PlayerInfo = 10009;
		 public const ushort G2C_PlayerInfo = 10010;
		 public const ushort Actor_Unit_GetInventoryInfosRequest = 10011;
		 public const ushort Actor_Unit_GetInventoryInfosResponse = 10012;
		 public const ushort ItemInfo = 10013;
		 public const ushort ComponentData = 10014;
		 public const ushort Actor_Unit_AddInventoryItemRequest = 10015;
		 public const ushort Actor_Unit_AddInventoryItemResponse = 10016;
		 public const ushort Actor_Unit_RemoveInventoryItemRequest = 10017;
		 public const ushort Actor_Unit_RemoveInventoryItemResponse = 10018;
		 public const ushort Actor_Unit_SortBagRequest = 10019;
		 public const ushort Actor_Unit_AddGoldRequest = 10020;
		 public const ushort Actor_Unit_AddGoldResponse = 10021;
		 public const ushort C2M_GetWorldItemRequest = 10022;
		 public const ushort M2C_GetWorldItemResponse = 10023;
		 public const ushort M2C_BroadcastWorldItem = 10024;
		 public const ushort M2C_BroadcastWorldRemove = 10025;
		 public const ushort Actor_Item_LockingWorldItemRequest = 10026;
		 public const ushort Actor_Item_LockingWorldItemResponse = 10027;
		 public const ushort Actor_Item_UsingWorldItemRequest = 10028;
		 public const ushort Actor_Item_UsingWorldItemResponse = 10029;
		 public const ushort C2M_InitTradeRequest = 10030;
		 public const ushort M2C_InitTradeResponse = 10031;
		 public const ushort M2C_NoticeInitTrade = 10032;
		 public const ushort C2M_AnswerAgreeTradeRequest = 10033;
		 public const ushort M2C_AnswerAgreeTradeResponse = 10034;
		 public const ushort M2C_BroadcastTradeStart = 10035;
		 public const ushort C2M_CancelTradeRequest = 10036;
		 public const ushort M2C_CancelTradeResponse = 10037;
		 public const ushort M2C_NoticeTradeCanceled = 10038;
		 public const ushort C2M_PutTradeCommodityRequest = 10039;
		 public const ushort M2C_PutTradeCommodityResponse = 10040;
		 public const ushort M2C_NoticePutTradeCommodity = 10041;
		 public const ushort Commodity = 10042;
		 public const ushort C2M_AffirmTradeRequest = 10043;
		 public const ushort M2C_AffirmTradeResponse = 10044;
		 public const ushort M2C_NoticeAffirmTrade = 10045;
		 public const ushort M2C_NoticeTradeResult = 10046;
		 public const ushort Actor_Unit_BuyItemRequest = 10047;
		 public const ushort Actor_Unit_SalesItemRequest = 10048;
	}
}
