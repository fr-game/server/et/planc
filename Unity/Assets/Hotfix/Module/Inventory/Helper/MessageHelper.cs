using System;
using System.Collections.Generic;
using ETModel;
using Google.Protobuf.Collections;

namespace ETHotfix
{
    public class MessageHelper
    {
        /// <summary>
        /// 反序列化背包
        /// </summary>
        /// <param name="res"></param>
        public static void DeSerializeInventory(Actor_Unit_GetInventoryInfosResponse res)
        {
            //已经创建好的背包组件。
            InventoryCmp inventoryCmp = ETModel.Game.Scene.GetComponent<InventoryCmp>();
            //背包基础属性
            inventoryCmp.Gold = res.Gold;
            inventoryCmp.Diamond = res.Diamond;
            inventoryCmp.MaxSize = res.MaxSize;
            inventoryCmp.InventoryStatus = EnumHelper.FromString<InventoryStatus>(res.InventoryState);
            inventoryCmp.Items = new SortedDictionary<int, Item>();
            //背包物品 1.加载物品
            List<Item> itemList = new List<Item>();
            foreach (ItemInfo info in res.ItemInfos)
            {
                Item item = MessageHelper.DeSerializeItemInfo(info);
                itemList.Add(item);
            }

            //背包物品 2.放入背包
            for (int i = 0; i < res.MaxSize; i++)
            {
                inventoryCmp.Items[i] = null;
                foreach (Item item in itemList)
                {
                    if (item.GetComponent<InventoryDataCmp>()?.Index == i)
                    {
                        inventoryCmp.Items[i] = item;
                    }
                }

                if (inventoryCmp.Items[i] != null)
                {
                    itemList.Remove(inventoryCmp.Items[i]);
                }
            }
        }

        /// <summary>
        /// 序列化背包
        /// </summary>
        /// <returns></returns>
        public static Actor_Unit_GetInventoryInfosResponse SerializeInventory()
        {
            //返回的结果
            Actor_Unit_GetInventoryInfosResponse response = new Actor_Unit_GetInventoryInfosResponse();
            //已经创建好的背包组件。
            InventoryCmp inventoryCmp = ETModel.Game.Scene.GetComponent<InventoryCmp>();
            //背包基础属性
            response.Gold = inventoryCmp.Gold;
            response.Diamond = inventoryCmp.Diamond;
            response.MaxSize = inventoryCmp.MaxSize;
            response.InventoryState = inventoryCmp.InventoryStatus.ToString();
            response.ItemInfos = new RepeatedField<ItemInfo>();
            foreach (var item in inventoryCmp.Items.Values)
            {
                if (item == null)
                {
                    continue;
                }
                response.ItemInfos.Add(SerializeItemInfo(item));
            }
            return response;
        }

        /// <summary>
        /// 序列化交易商品
        /// </summary>
        /// <param name="itemList"></param>
        /// <param name="price"></param>
        /// <returns></returns>
        public static ETHotfix.Commodity SerializeCommodity(List<Item> itemList, int price)
        {
            ETHotfix.Commodity commodity = new ETHotfix.Commodity();
            commodity.Price = price;
            commodity.TraderId = ETModel.Game.Scene.GetComponent<UnitComponent>().MyUnit.Id;
            commodity.Items = new RepeatedField<ETHotfix.ItemInfo>();
            foreach (Item item in itemList)
            {
                ETHotfix.ItemInfo itemInfo = SerializeItemInfo(item);
                commodity.Items.Add(itemInfo);
            }

            return commodity;
        }

        /// <summary>
        /// 序列化交易用信息
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public static ItemInfo SerializeItemInfo(Item item)
        {
            ItemInfo itemInfo = new ItemInfo();
            if(item == null)
            {
                return itemInfo;
            }
            itemInfo.CreateTime = item.CreateTime;
            itemInfo.ItemConfigId = item.ItemConfigId;
            itemInfo.ItemId = item.Id;
            itemInfo.ComponentDatas = new RepeatedField<ComponentData>();
            foreach (ETModel.Component cmp in item.GetComponents())
            {
                itemInfo.ComponentDatas.Add(SerializeComponent(cmp));
            }

            return itemInfo;
        }

        /// <summary>
        /// 反序列化交易商品
        /// </summary>
        /// <param name="itemList"></param>
        /// <param name="price"></param>
        /// <returns></returns>
        public static ETModel.Commodity DeSerializeCommodity(ETHotfix.Commodity commodity)
        {
            ETModel.Commodity commodityModel = new ETModel.Commodity();
            commodityModel.Price = commodity.Price;
            commodityModel.Trader = ETModel.Game.Scene.GetComponent<UnitComponent>().Get(commodity.TraderId);
            commodityModel.ItemList = new List<Item>();
            foreach (ItemInfo itemInfo in commodity.Items)
            {
                Item item = DeSerializeItemInfo(itemInfo);
                commodityModel.ItemList.Add(item);
            }

            return commodityModel;
        }

        /// <summary>
        /// 反序列化商品信息
        /// </summary>
        /// <param name="itemInfo"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public static Item DeSerializeItemInfo(ItemInfo itemInfo)
        {
            if (itemInfo.ItemId <= 0)
            {
                return null;
            }
            Item item = ItemFactory.CreateItemFromRecord(itemInfo.ItemConfigId);
            item.Id = itemInfo.ItemId;
            item.CreateTime = itemInfo.CreateTime;
            foreach (ComponentData componentData in itemInfo.ComponentDatas)
            {
                item.AddComponent(DeSerializeComponent(componentData));
            }

            return item;
        }

        /// <summary>
        /// 序列化组件
        /// </summary>
        /// <param name="cmp"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public static ComponentData SerializeComponent(ETModel.Component cmp)
        {
            ComponentData tmpCmpData = new ComponentData();
            tmpCmpData.TypeName = cmp.GetType().ToString();
            ETModel.Entity componentEntity = cmp.Entity;
            ETModel.Component componentParent = cmp.Parent;
            cmp.Entity = null;
            cmp.Parent = null;
            tmpCmpData.CmpDataBytes = JsonHelper.ToJson(cmp);
            cmp.Entity = componentEntity;
            cmp.Parent = componentParent;
            return tmpCmpData;
        }

        /// <summary>
        /// 反序列化组件信息
        /// </summary>
        /// <param name="componentData"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public static ETModel.Component DeSerializeComponent(ComponentData componentData)
        {
            var fromJson = (ETModel.Component) ETModel.JsonHelper.FromJson(Type.GetType(componentData.TypeName), componentData.CmpDataBytes);
            fromJson.EndDeSerialize();
            return fromJson;
        }
    }
}