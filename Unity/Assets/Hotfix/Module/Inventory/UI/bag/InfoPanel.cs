/**
 * 背包系统_物品详情面板
 */

using System;
using System.Threading.Tasks;
using ETModel;
using FairyGUI;
using UnityEngine;

namespace ETHotfix
{
    public class InfoPanel: BaseUIForms
    {
        public InfoPanel()
        {
            this.pakName = "Inventory";
            this.cmpName = "infoPanel";
            this.CurrentUIType.NeedClearingStack = false;
            this.CurrentUIType.UIForms_ShowMode = UIFormsShowMode.ReverseChange;
            this.CurrentUIType.UIForms_Type = UIFormsType.Window;
        }

        //面板组件
        private GComponent _gCmp;

        //物品索引    
        private int _index = -1;

        //物品名称
        private GTextField _itemNameText;

        //图标
        private GLoader _itemIcon;

        //使用等级限制
        private GTextField _levelText;

        //价格
        private GTextField _priceText;

        //描述信息
        private GTextField _descText;

        //基础属性
        private GTextField _baseData;

        //拓展属性
        private GList _equipDataList;

        //装备拓展属性
        private GTextField _equipExtData;

        //应用按钮
        private GButton _useBtn;

        //关闭按钮
        private GButton _closeBtn;

        /// <summary>
        /// 初始化
        /// </summary>
        public override void InitUI()
        {
            this._gCmp = this.GObject.asCom;
            //获取各组件
            _itemNameText = this._gCmp.GetChild("itemNameText").asTextField;
            _itemIcon = this._gCmp.GetChild("itemIcon").asLoader;
            _levelText = this._gCmp.GetChild("levelText").asTextField;
            _priceText = this._gCmp.GetChild("priceText").asTextField;
            _descText = this._gCmp.GetChild("descText").asTextField;
            _baseData = this._gCmp.GetChild("baseData").asTextField;
            _equipDataList = this._gCmp.GetChild("equipDataList").asList;
            _equipExtData = this._gCmp.GetChild("equipExtData").asTextField;
            _useBtn = this._gCmp.GetChild("useBtn").asButton;
            _closeBtn = this._gCmp.GetChild("closeBtn").asButton;
            //btnClick
            _useBtn.onClick.Add(this.UseItem);
            _closeBtn.onClick.Add(this.Close);
            //drag
            this._gCmp.draggable = true;
        }

        /// <summary>
        /// 使用按钮
        /// </summary>
        /// <param name="context"></param>
        private void UseItem(EventContext context)
        {
            UI.Tip("使用物品");
        }

        /// <summary>
        /// 设置面板数据
        /// </summary>
        /// <param name="index"></param>
        public void SetPanelInfo(int index)
        {
            this._index = index;
            Item item = ETModel.Game.Scene.GetComponent<InventoryCmp>().GetItemByIndex(index);
            if (item == null)
            {
                return;
            }

            //基础信息
            _itemNameText.text = item.GetName();
            _itemNameText.color = item.GetNameColor();
            _itemIcon.url = item.GetIcon();
            _levelText.text = item.GetItemConfig().UsingLevel.ToString();
            _priceText.text = item.GetItemConfig().Price.ToString();
            _descText.text = item.GetDescription();
            _baseData.text = string.Empty;
            this._useBtn.title = ItemType.Equipment == EnumHelper.FromString<ItemType>(item.GetItemConfig().ItemType)? LocalLanguage.Equip
                    : LocalLanguage.Use;
            if (ItemType.Equipment == EnumHelper.FromString<ItemType>(item.GetItemConfig().ItemType))
            {
                //强化等级
                _baseData.text = $"{LocalLanguage.ForgeLevel}:{item.GetComponent<EquipmentDataCmp>().ForgeLevel.ToString()}";
                //属性信息_拓展属性拼装
                _equipExtData.text = item.GetComponent<EquipmentDataCmp>().GetExtDataDesc();
                //基础信息
                this._equipDataList.RemoveChildrenToPool();
                foreach (var equipData in item.GetComponent<EquipmentDataCmp>().EquipDataList)
                {
                    //池子中取出一个对象
                    GComponent equipDataCmp = this._equipDataList.AddItemFromPool("ui://Inventory/ItemDataCmp").asCom;
                    equipDataCmp.GetChild("dataNameText").asTextField.text = equipData.GetName();
                    equipDataCmp.GetChild("dataValueText").asTextField.text = ((int) equipData.CurrentValue).ToString();
                }
            }
            else
            {
                this._equipDataList.RemoveChildrenToPool();
                _equipExtData.text = string.Empty;
                //基础信息
                foreach (var effectData in item.EffectList)
                {
                    //池子中取出一个对象
                    GComponent equipDataCmp = this._equipDataList.AddItemFromPool("ui://Inventory/ItemDataCmp").asCom;
                    equipDataCmp.GetChild("dataNameText").asTextField.text = effectData.ChangeFieldName;
                    equipDataCmp.GetChild("dataValueText").asTextField.text = ((int) effectData.ChangeValue).ToString();
                }
            }
        }

        public override Task HideEvent()
        {
            this._index = -1;
            this.Close();
            return base.HideEvent();
        }

        public override void Display()
        {
            GRoot.inst.ShowPopup(this.GObject);
            this.GObject.Center();
        }
    }
}