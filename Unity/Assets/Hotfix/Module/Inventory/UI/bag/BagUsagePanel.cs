/**
 * 背包系统_背包内选择用途面板
 */

using System.Threading.Tasks;
using ETModel;
using FairyGUI;
using UnityEditor.IMGUI.Controls;
using UnityEngine;
using UnityEngine.SocialPlatforms;

namespace ETHotfix
{
    public class BagUsagePanel: BaseUIForms
    {
        public BagUsagePanel()
        {
            this.pakName = "Inventory";
            this.cmpName = "usagePanel";
            this.CurrentUIType.NeedClearingStack = true;
            this.CurrentUIType.UIForms_ShowMode = UIFormsShowMode.Normal;
            this.CurrentUIType.UIForms_Type = UIFormsType.Window;
        }

        //物品索引
        private int _index;

        //面板组件
        private GComponent _gCmp;

        //物品图标
        private GLoader _itemIcon;

        //物品名称
        private GTextField _itemNameText;

        //描述信息
        private GTextField _descText;

        //使用
        private GButton _useBtn;

        //丢弃
        private GButton _dropBtn;

        //出售
        private GButton _saleBtn;

        //查看
        private GButton _viewBtn;

        //关闭按钮
        private GButton _closeBtn;

        /// <inheritdoc />
        /// <summary>
        /// 初始化
        /// </summary>
        public override void InitUI()
        {
            //获取各组件
            _gCmp = this.GObject.asCom;
            _itemIcon = this._gCmp.GetChild("itemIcon").asLoader;
            _itemNameText = this._gCmp.GetChild("itemNameText").asTextField;
            _useBtn = this._gCmp.GetChild("useBtn").asButton;
            _dropBtn = this._gCmp.GetChild("dropBtn").asButton;
            _saleBtn = this._gCmp.GetChild("saleBtn").asButton;
            _viewBtn = this._gCmp.GetChild("viewBtn").asButton;
            _closeBtn = this._gCmp.GetChild("closeBtn").asButton;
            _descText = this._gCmp.GetChild("descText").asTextField;
            //按钮事件
            //使用
            _useBtn.onClick.Add(this.UseItem);
            //丢弃
            _dropBtn.onClick.Add(this.DropItem);
            //出售
            _saleBtn.onClick.Add(this.SaleItem);
            //查看
            _viewBtn.onClick.Add(this.ViewItem);
            //关闭按钮
            _closeBtn.onClick.Add(this.Close);
            //可拖动
            this._gCmp.draggable = true;
        }

        /// <summary>
        /// 使用
        /// </summary>
        /// <exception cref="NotImplementedException"></exception>
        private void UseItem()
        {
        }

        /// <summary>
        /// 丢弃
        /// </summary>
        /// <exception cref="NotImplementedException"></exception>
        public void DropItem()
        {
            Item item = ETModel.Game.Scene.GetComponent<InventoryCmp>().GetItemByIndex(this._index);
            if (item.GetCanMany())
            {
                EnterNumPanel enterNumPanel = (EnterNumPanel) UI.Open(typeof(EnterNumPanel));
                object[] param = new object[2];
                param[0] = item;
                param[1] = 1;
                enterNumPanel.SetPanelInfo(LocalLanguage.DropNum, LocalLanguage.AskDrop, item.GetComponent<InventoryDataCmp>().CountNum,
                                           DropItemAskCallBack, param);
            }
            else
            {
                //询问是否
                AskPanel askPanel = (AskPanel) UI.Open(typeof(AskPanel));
                object[] param = new object[2];
                param[0] = item;
                param[1] = 1;
                askPanel.SetPanelInfo(LocalLanguage.AskDrop, DropItemAskCallBack, param);
            }
        }

        /// <summary>
        /// 丢弃的回调
        /// </summary>
        /// <param name="obj"></param>
        private async void DropItemAskCallBack(object[] obj)
        {
            bool dropInventoryItem = await ETModel.Game.Scene.GetComponent<InventoryCmp>().DropInventoryItem((Item) obj[0], (int) obj[1]);
            if (!dropInventoryItem)
            {
                //提示丢弃物品失败。
                UI.Tip(LocalLanguage.DropItemFailed);
            }
            else
            {
                this.Close();
            }
        }

        /// <summary>
        /// 出售
        /// </summary>
        /// <exception cref="NotImplementedException"></exception>
        private void SaleItem()
        {
            UI.Tip(LocalLanguage.OnlyShopCanSale);
        }

        /// <summary>
        /// 查看详情
        /// </summary>
        private void ViewItem()
        {
            //展示UI传入参数_index;
            InfoPanel infoPanel = (InfoPanel) UI.Open(typeof(InfoPanel));
            infoPanel.SetPanelInfo(this._index);
        }

        /// <summary>
        /// 设置面板信息
        /// </summary>
        /// <param name="index">背包的位置</param>
        public void SetPanelInfo(int index)
        {
            this._index = index;
            Item item = ETModel.Game.Scene.GetComponent<InventoryCmp>().GetItemByIndex(index);
            _itemNameText.text = item.GetName();
            _itemNameText.color = item.GetNameColor();
            _itemIcon.url = item.GetIcon();
            _descText.text = item.GetDescription();
            this._useBtn.title = ItemType.Equipment == EnumHelper.FromString<ItemType>(item.GetItemConfig().ItemType)? LocalLanguage.Equip
                    : LocalLanguage.Use;
        }

        public override Task HideEvent()
        {
            this._index = -1;
            //popUp关闭触发这个事件，所以要手动关闭掉这个类。
            this.Close();
            return base.HideEvent();
        }

        public override void Display()
        {
            GRoot.inst.ShowPopup(this.Window);
            this.Window.Center();
        }
    }
}