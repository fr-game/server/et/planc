/**
 * 背包面板
 */

using System.Threading.Tasks;
using ETModel;
using FairyGUI;
using UnityEngine;

namespace ETHotfix
{
    public class BagPanel: BaseUIForms
    {
        public BagPanel()
        {
            this.pakName = "Inventory";
            this.cmpName = "bagPanelCmp";
            this.CurrentUIType.NeedClearingStack = false;
            this.CurrentUIType.UIForms_ShowMode = UIFormsShowMode.Normal;
            this.CurrentUIType.UIForms_Type = UIFormsType.Window;
        }

        public static BagPanel Instance;

        /// <summary>
        /// 是否首次开启
        /// </summary>
        public bool FirstOpen = true;

        //背包组件
        private GComponent _gCmp;

        //列表
        private GList _itemList;

        //金币数量
        private GTextField _goldText;

        //钻石数量
        private GTextField _diamondText;

        //关闭按钮
        private GButton _closeBtn;

        //整理按钮
        private GButton _sortBtn;

        /// <summary>
        /// 初始化
        /// </summary>
        public override void InitUI()
        {
            Instance = this;
            //获取各组件
            _gCmp = this.GObject.asCom;
            _itemList = this._gCmp.GetChild("itemList").asList;
            _goldText = this._gCmp.GetChild("goldText").asTextField;
            _diamondText = this._gCmp.GetChild("diamondText").asTextField;

            _closeBtn = this._gCmp.GetChild("closeBtn").asButton;
            _sortBtn = this._gCmp.GetChild("sortBtn").asButton;
            //按钮事件
            _closeBtn.onClick.Add(CloseBag);
            _sortBtn.onClick.Add(SortBag);
            //开启虚拟列表
            _gCmp.Center();
            this._itemList.SetVirtual();
            this._itemList.defaultItem = "ui://Inventory/itemCellCmp";
            this._itemList.itemRenderer = RenderListItem;
            this._itemList.onClickItem.Add(ItemClickCallback);
            this._itemList.onRightClickItem.Add(ItemRightClick);
            this._itemList.numItems = ETModel.Game.Scene.GetComponent<InventoryCmp>().MaxSize;
            this._gCmp.SetPivot(0.5f, 0.5f);
        }

        /// <summary>
        /// 右击物品直接使用
        /// </summary>
        /// <param name="context"></param>
        private void ItemRightClick(EventContext context)
        {
            GComponent gItem = (GComponent) context.data;
            if (gItem.GetChild("itemIcon").asLoader.url == "")
            {
                return;
            }

            int childIndex = this._itemList.GetChildIndex(gItem);
            //打开一个新的弹出窗口。
            BagUsagePanel panel = (BagUsagePanel) UI.Open(typeof(BagUsagePanel));
            panel.SetPanelInfo(childIndex);
        }

        private void ItemClickCallback(EventContext context)
        {
            GComponent gItem = (GComponent) context.data;
            if (gItem.GetChild("itemIcon").asLoader.url == "")
            {
                return;
            }

            int childIndex = this._itemList.GetChildIndex(gItem);
            //打开一个新的弹出窗口。
            BagUsagePanel panel = (BagUsagePanel) UI.Open(typeof(BagUsagePanel));
            panel.SetPanelInfo(childIndex);
        }

        private void RenderListItem(int index, GObject item)
        {
            GComponent gItem = (GComponent) item;
            Item itemData = ETModel.Game.Scene.GetComponent<InventoryCmp>().Items[index];
            if (itemData == null)
            {
                gItem.GetChild("itemIcon").asLoader.url = "";
                gItem.GetChild("effectImg").asLoader.url = "";
                gItem.GetChild("numText").asTextField.text = "";
            }
            else
            {
                gItem.GetChild("itemIcon").asLoader.url = itemData.GetIcon();
                gItem.GetChild("effectImg").asLoader.url = itemData.GetEffectIcon();
                gItem.GetChild("numText").asTextField.text = itemData.GetComponent<InventoryDataCmp>().CountNum.ToString();
            }
        }

        public void UpdateBag()
        {
            InventoryCmp inventoryCmp = ETModel.Game.Scene.GetComponent<InventoryCmp>();
            _goldText.text = inventoryCmp.Gold.ToString();
            this._diamondText.text = inventoryCmp.Diamond.ToString();
            this._itemList.RefreshVirtualList();
        }

        private void SortBag()
        {
            bool sortInventory = ETModel.Game.Scene.GetComponent<InventoryCmp>().SortInventory();
            if (!sortInventory)
            {
                //"操作频繁 ，稍后再试
                UI.Tip(LocalLanguage.TooOften);
            }
            else
            {
                this._itemList.RefreshVirtualList();
            }
        }

        public void CloseBag()
        {
            //先关闭所有的popUp
            GRoot.inst.HidePopup();
            this._gCmp.TweenScale(new Vector2(0f, 0f), 0.3f);
        }

        public override void Display()
        {
            if (this.FirstOpen)
            {
                base.Display();
                this.FirstOpen = false;
                this._itemList.RefreshVirtualList();
            }
            else
            {
                this._gCmp.TweenScale(new Vector2(1, 1), 0.3f);
            }

            UpdateBag();
        }

        /// <summary>
        /// 退出事件
        /// </summary>
        /// <returns></returns>
        public override Task HideEvent()
        {
         
            Instance = null;
            this.FirstOpen = true;
           // this._itemList.numItems = 0;
            return Task.CompletedTask;
        }
    }
}