/**
 * 背包系统_询问面板
 */

using System;
using System.Threading.Tasks;
using FairyGUI;
using UnityEngine;

namespace ETHotfix
{
    public class AskPanel: BaseUIForms
    {
        public AskPanel()
        {
            this.pakName = "Inventory";
            this.cmpName = "askPanelCmp";
            this.CurrentUIType.NeedClearingStack = false;
            this.CurrentUIType.UIForms_ShowMode = UIFormsShowMode.ReverseChange;
            this.CurrentUIType.UIForms_Type = UIFormsType.Window;
        }

        //面板组件
        private GComponent _gCmp;

        //询问内容
        private GTextField _contentText;

        //yes
        private GButton _yesBtn;

        //no
        private GButton _noBtn;

        //需要执行的动作
        private Action<object[]> callBack;

        //否时执行的动作
        private Action noCallBack;

        //需要透传的参数
        private object[] param;

        /// <inheritdoc />
        /// <summary>
        /// 初始化
        /// </summary>
        public override void InitUI()
        {
            //获取各组件
            _gCmp = this.GObject.asCom;
            _noBtn = this._gCmp.GetChild("noBtn").asButton;
            _yesBtn = this._gCmp.GetChild("yesBtn").asButton;
            _contentText = this._gCmp.GetChild("contentText").asTextField;
            //按钮事件
            //no
            _noBtn.onClick.Add(() =>
            {
                this.noCallBack?.Invoke();
                this.Close();
            });
            //yes
            _yesBtn.onClick.Add(() =>
            {
                this.Close();
                callBack(this.param);
            });
            //drag
            this._gCmp.draggable = true;
        }

        /// <summary>
        /// 设置面板数据
        /// </summary>
        /// <param name="askContent">询问内容</param>
        /// <param name="callBack">回调方法</param>
        /// <param name="param"></param>
        /// <param name="noCallBack"></param>
        public void SetPanelInfo(string askContent, Action<object[]> callBack, object[] param, Action noCallBack = null)
        {
            this._contentText.text = askContent;
            this.callBack = callBack;
            this.param = param;
            this.noCallBack = noCallBack;
        }

        public override Task HideEvent()
        {
            this.callBack = null;
            this.Close();
            return base.HideEvent();
        }

        public override void Display()
        {
            GRoot.inst.ShowPopup(this._gCmp);
            this._gCmp.Center();
        }
    }
}