/**
 * 背包系统_交易物品详情面板
 */

using System;
using System.Threading.Tasks;
using ETModel;
using FairyGUI;
using UnityEditor;

namespace ETHotfix
{
    public class StoreInfoPanel: BaseUIForms
    {
        public StoreInfoPanel()
        {
            this.pakName = "Inventory";
            this.cmpName = "storeInfoPanel";
            this.CurrentUIType.NeedClearingStack = false;
            this.CurrentUIType.UIForms_ShowMode = UIFormsShowMode.Normal;
            this.CurrentUIType.UIForms_Type = UIFormsType.Window;
        }

        //面板组件
        private GComponent _gCmp;

        //回调
        private Action<object[]> callback;

        //物品
        private Item _item;

        //物品名称
        private GTextField _itemNameText;

        //图标
        private GLoader _itemIcon;

        //使用等级限制
        private GTextField _levelText;

        //价格
        private GTextField _priceText;

        //描述信息
        private GTextField _descText;

        //基础属性
        private GTextField _baseData;

        //拓展属性
        private GList _equipDataList;

        //装备拓展属性
        private GTextField _equipExtData;

        //应用按钮
        private GButton _useBtn;

        //关闭按钮
        private GButton _closeBtn;

        /// <summary>
        /// 初始化
        /// </summary>
        public override void InitUI()
        {
            this._gCmp = this.GObject.asCom;
            //获取各组件
            _itemNameText = this._gCmp.GetChild("itemNameText").asTextField;
            _itemIcon = this._gCmp.GetChild("itemIcon").asLoader;
            _levelText = this._gCmp.GetChild("levelText").asTextField;
            _priceText = this._gCmp.GetChild("priceText").asTextField;
            _descText = this._gCmp.GetChild("descText").asTextField;
            _baseData = this._gCmp.GetChild("baseData").asTextField;
            _equipDataList = this._gCmp.GetChild("equipDataList").asList;
            _equipExtData = this._gCmp.GetChild("equipExtData").asTextField;
            _useBtn = this._gCmp.GetChild("useBtn").asButton;
            _closeBtn = this._gCmp.GetChild("closeBtn").asButton;
            //btnClick
            _useBtn.onClick.Add(this.BtnClick);
            _closeBtn.onClick.Add(this.Close);

            //drag
            this._gCmp.draggable = true;
            this._gCmp.onRemovedFromStage.Add(this.Close);
        }

        /// <summary>
        /// 点击了 useBtn。这个按钮可以是交易/取回
        /// </summary>
        /// <param name="context"></param>
        private void BtnClick(EventContext context)
        {
            if (this.callback == null)
            {
                this.Close();
            }

            if (this._item.GetCanMany())
            {
                //输入数量面板
                EnterNumPanel enterNumPanel = (EnterNumPanel) UI.Open(typeof(EnterNumPanel));
                object[] param = { this._item, 0 };
                enterNumPanel.SetPanelInfo(LocalLanguage.EnterNum, string.Empty, this._item.GetComponent<InventoryDataCmp>().CountNum, this.callback,
                                           param);
            }
            else
            {
                this.callback?.Invoke(new object[] { this._item, 1 });
                this.Close();
            }
        }

        /// <summary>
        /// 设置面板数据
        /// </summary>
        /// <param name="item">物品</param>
        /// <param name="btnTitle">按钮名字</param>
        /// <param name="callBack">是指回调，如果为空则直接关闭信息面板</param>
        public void SetPanelInfo(Item item, string btnTitle,Action<object[]> callBack)
        {
            _useBtn.title = btnTitle;
            this._item = item;
            this.callback = callBack;
            this.callback += (obj) => { this.Close();};
            //基础信息
            _itemNameText.text = item.GetName();
            _itemNameText.color = item.GetNameColor();
            _itemIcon.url = item.GetIcon();
            _levelText.text = item.GetItemConfig().UsingLevel.ToString();
            _priceText.text = item.GetItemConfig().Price.ToString();
            _descText.text = item.GetDescription();
            _baseData.text = string.Empty;
            if (ItemType.Equipment == EnumHelper.FromString<ItemType>(item.GetItemConfig().ItemType))
            {
                //强化等级
                _baseData.text = $"{LocalLanguage.ForgeLevel}:{item.GetComponent<EquipmentDataCmp>().ForgeLevel.ToString()}";
                //属性信息_拓展属性拼装
                _equipExtData.text = item.GetComponent<EquipmentDataCmp>().GetExtDataDesc();
                //基础信息
                this._equipDataList.RemoveChildrenToPool();
                foreach (var equipData in item.GetComponent<EquipmentDataCmp>().EquipDataList)
                {
                    //池子中取出一个对象
                    GComponent equipDataCmp = this._equipDataList.AddItemFromPool("ui://Inventory/ItemDataCmp").asCom;
                    equipDataCmp.GetChild("dataNameText").asTextField.text = equipData.GetName();
                    equipDataCmp.GetChild("dataValueText").asTextField.text = ((int) equipData.CurrentValue).ToString();
                }
            }
            else
            {
                this._equipDataList.RemoveChildrenToPool();
                _equipExtData.text = string.Empty;
                //基础信息
                foreach (var effectData in item.EffectList)
                {
                    //池子中取出一个对象
                    GComponent equipDataCmp = this._equipDataList.AddItemFromPool("ui://Inventory/ItemDataCmp").asCom;
                    equipDataCmp.GetChild("dataNameText").asTextField.text = effectData.ChangeFieldName;
                    equipDataCmp.GetChild("dataValueText").asTextField.text = ((int) effectData.ChangeValue).ToString();
                }
            }
        }

        public override Task HideEvent()
        {
            this.Close();
            return base.HideEvent();
        }

        public override void Display()
        {
            GRoot.inst.ShowPopup(this.GObject);
            this.GObject.Center();
        }
    }
}