/**
 * 商店面板
 */

using System;
using System.Threading.Tasks;
using ETModel;
using FairyGUI;
using UnityEngine;

namespace ETHotfix
{
    public class StorePanel: BaseUIForms
    {
        public StorePanel()
        {
            this.pakName = "Inventory";
            this.cmpName = "storePanel";
            this.CurrentUIType.NeedClearingStack = false;
            this.CurrentUIType.UIForms_ShowMode = UIFormsShowMode.Normal;
            this.CurrentUIType.UIForms_Type = UIFormsType.Window;
        }

        //背包组件
        private GComponent _gCmp;

        //商店数据组件
        private StoreCmp storeCmp;

        //商店名称
        private GTextField _storeNameText;

        //商店物品列表
        private GList _storeItemsGList;

        //我的商品列表
        private GList _myItemList;

        //金币数量
        private GTextField _goldText;

        //钻石数量
        private GTextField _diamondText;

        //关闭按钮
        private GButton _closeBtn;

        public static StorePanel Instance = null;

        /// <summary>
        /// 初始化
        /// </summary>
        public override void InitUI()
        {
            //获取各组件
            _gCmp = this.GObject.asCom;
            _storeNameText = this._gCmp.GetChild("storeNameText").asTextField;
            _storeItemsGList = this._gCmp.GetChild("storeItemsGList").asList;
            _myItemList = this._gCmp.GetChild("myItemList").asList;
            _goldText = this._gCmp.GetChild("goldText").asTextField;
            _diamondText = this._gCmp.GetChild("diamondText").asTextField;
            _closeBtn = this._gCmp.GetChild("closeBtn").asButton;
            //按钮事件
            _closeBtn.onClick.Add(() => { this.Close(); });

            //组件的设置。
            this._gCmp.SetPivot(0.5f, 0.5f);
            this._gCmp.TweenScale(new Vector2(0, 0), 0.1f);
            _gCmp.Center();
            Instance = this;
        }

        /// <summary>
        /// 设置面板数据，放到Init之后，不然storeCmp没有数据
        /// </summary>
        /// <param name="storeCmp"></param>
        public void SetPanelInfo(StoreCmp storeCmp)
        {
            this.storeCmp = storeCmp;
            _storeNameText.text = storeCmp.GetStoreName();
            _goldText.text = ETModel.Game.Scene.GetComponent<InventoryCmp>().Gold.ToString();
            _diamondText.text = ETModel.Game.Scene.GetComponent<InventoryCmp>().Diamond.ToString();
            //开启虚拟列表_商店列表
            this._storeItemsGList.SetVirtual();
            this._storeItemsGList.defaultItem = "ui://Inventory/storeItemCellCmp";
            this._storeItemsGList.itemRenderer = RenderStoreListItem;
            this._storeItemsGList.onClickItem.Add(this.StoreItemClickEvent);
            this._storeItemsGList.onRightClickItem.Add(StoreItemRightClick);
            this._storeItemsGList.numItems = storeCmp.ItemList.Count;
            //开启虚拟列表_背包物品列表
            this._myItemList.SetVirtual();
            this._myItemList.defaultItem = "ui://Inventory/itemCellCmp";
            this._myItemList.itemRenderer = RenderBagListItem;
            this._myItemList.onClickItem.Add(BagItemClickEvent);
            this._myItemList.onRightClickItem.Add(BagItemRightClick);
            this._myItemList.numItems = ETModel.Game.Scene.GetComponent<InventoryCmp>().MaxSize;
        }

        /// <summary>
        /// 右键点击背包部分的物品
        /// </summary>
        /// <param name="context"></param>
        /// <exception cref="NotImplementedException"></exception>
        private void BagItemRightClick(EventContext context)
        {
            GComponent gItem = (GComponent) context.data;
            if (gItem.GetChild("itemIcon").asLoader.url == "")
            {
                return;
            }

            int index = this._myItemList.GetChildIndex(gItem);
            Item itemData = ETModel.Game.Scene.GetComponent<InventoryCmp>().GetItemByIndex(index);
            int num = itemData.GetItemConfig().MaxNum;
            this.BagItemClickCallback(new object[] { itemData, num });
        }

        /// <summary>
        /// 左键点击背包部分的物品
        /// </summary>
        /// <param name="context"></param>
        /// <exception cref="NotImplementedException"></exception>
        private void BagItemClickEvent(EventContext context)
        {
            GComponent gItem = (GComponent) context.data;
            if (gItem.GetChild("itemIcon").asLoader.url == "")
            {
                return;
            }

            int index = this._myItemList.GetChildIndex(gItem);
            Item itemData = ETModel.Game.Scene.GetComponent<InventoryCmp>().GetItemByIndex(index);
            //打开一个新的弹出窗口。
            StoreInfoPanel panel = (StoreInfoPanel) UI.Open(typeof(StoreInfoPanel));
            panel.SetPanelInfo(itemData, LocalLanguage.Sale,this.BagItemClickCallback);
        }

        /// <summary>
        /// 背包部分物品点击的回调
        /// </summary>
        /// <param name="context"></param>
        private async void BagItemClickCallback(object[] obj)
        {
            Item item = (Item) obj[0];
            int num = (int) obj[1];
            var sales = await this.storeCmp.Sales(item, num);
            if (sales)
            {
                //刷新背包和背包部分
                BagPanel.Instance?.UpdateBag();
                _myItemList.RefreshVirtualList();
                _goldText.text = ETModel.Game.Scene.GetComponent<InventoryCmp>().Gold.ToString();
                _diamondText.text = ETModel.Game.Scene.GetComponent<InventoryCmp>().Diamond.ToString();
            }
            else
            {
                UI.Tip(LocalLanguage.Nothing);
            }
        }

        /// <summary>
        /// 渲染背包部分的列表
        /// </summary>
        /// <param name="index"></param>
        /// <param name="item"></param>
        /// <exception cref="NotImplementedException"></exception>
        private void RenderBagListItem(int index, GObject item)
        {
            GComponent gItem = (GComponent) item;
            Item itemData = ETModel.Game.Scene.GetComponent<InventoryCmp>().Items[index];
            if (itemData == null)
            {
                gItem.GetChild("itemIcon").asLoader.url = "";
                gItem.GetChild("effectImg").asLoader.url = "";
                gItem.GetChild("numText").asTextField.text = "";
            }
            else
            {
                gItem.GetChild("itemIcon").asLoader.url = itemData.GetIcon();
                gItem.GetChild("effectImg").asLoader.url = itemData.GetEffectIcon();
                gItem.GetChild("numText").asTextField.text = itemData.GetComponent<InventoryDataCmp>().CountNum.ToString();
            }
        }

        /// <summary>
        /// 右击物品直接购买
        /// </summary>
        /// <param name="context"></param>
        private void StoreItemRightClick(EventContext context)
        {
            GComponent gItem = (GComponent) context.data;
            if (gItem.GetChild("itemIcon").asLoader.url == "")
            {
                return;
            }

            int index = this._storeItemsGList.GetChildIndex(gItem);
            Item itemData = this.storeCmp.ItemList[index];
            int num = itemData.GetItemConfig().MaxNum;
            this.StoreItemCallback(new object[] { itemData, num });
        }

        /// <summary>
        /// 左击 弹出选项
        /// </summary>
        /// <param name="context"></param>
        private void StoreItemClickEvent(EventContext context)
        {
            GComponent gItem = (GComponent) context.data;
            if (gItem.GetChild("itemIcon").asLoader.url == "")
            {
                return;
            }

            int index = this._storeItemsGList.GetChildIndex(gItem);
            Item itemData = this.storeCmp.ItemList[index];
            //打开一个新的弹出窗口。
            StoreInfoPanel panel = (StoreInfoPanel) UI.Open(typeof(StoreInfoPanel));
            panel.SetPanelInfo(itemData, LocalLanguage.Buy,this.StoreItemCallback);
        }

        /// <summary>
        /// 商店物品点击的回调
        /// </summary>
        /// <param name="obj"></param>
        private async void StoreItemCallback(object[] obj)
        {
            Item item = (Item) obj[0];
            int num = (int) obj[1];
            int result = await this.storeCmp.Buy(item, num);
            switch (result)
            {
                case 0:
                    UI.Tip(LocalLanguage.InventoryLocking);
                    break;

                case 1:
                    UI.Tip(LocalLanguage.NotSufficientFunds);
                    break;
                case 2:
                    UI.Tip(LocalLanguage.InventoryFull);
                    break;
                case 3:
                    //刷新背包和背包部分
                    BagPanel.Instance?.UpdateBag();
                    _myItemList.RefreshVirtualList();
                    _goldText.text = ETModel.Game.Scene.GetComponent<InventoryCmp>().Gold.ToString();
                    _diamondText.text = ETModel.Game.Scene.GetComponent<InventoryCmp>().Diamond.ToString();
                    break;
            }
        }

        /// <summary>
        /// 渲染商店列表
        /// </summary>
        /// <param name="index"></param>
        /// <param name="item"></param>
        private void RenderStoreListItem(int index, GObject item)
        {
            GComponent gItem = (GComponent) item;
            Item itemData = this.storeCmp.ItemList[index];
            if (itemData == null)
            {
                gItem.GetChild("itemIcon").asLoader.url = "";
                gItem.GetChild("priceText").asTextField.text = "";
                gItem.GetChild("itemNameText").asTextField.text = "";
            }
            else
            {
                gItem.GetChild("itemIcon").asLoader.url = itemData.GetIcon();
                gItem.GetChild("priceText").asTextField.text = itemData.GetItemConfig().Price.ToString();
                gItem.GetChild("itemNameText").asTextField.text = itemData.GetName();
            }
        }

        public override void Display()
        {
            this._gCmp.TweenScale(new Vector2(1, 1), 1f);
            base.Display();
        }

        /// <summary>
        /// 退出事件
        /// </summary>
        /// <returns></returns>
        public override Task HideEvent()
        {
            //先关闭所有的popUp
            GRoot.inst.HidePopup();
            this.Close();
            Instance = null;
            return Task.CompletedTask;
        }
    }
}