/**
 * 背包系统_输入数值面板 
 */

using System;
using System.Threading.Tasks;
using ETModel;
using FairyGUI;
using UnityEngine;

namespace ETHotfix
{
    public class EnterNumPanel: BaseUIForms
    {
        public EnterNumPanel()
        {
            this.pakName = "Inventory";
            this.cmpName = "enterNumPanel";
            this.CurrentUIType.NeedClearingStack = false;
            this.CurrentUIType.UIForms_ShowMode = UIFormsShowMode.ReverseChange;
            this.CurrentUIType.UIForms_Type = UIFormsType.Window;
        }

        //面板组件
        private GComponent _gCmp;

        //输入内容提示
        private GTextField _contentText;

        //yes
        private GButton _yesBtn;

        //no
        private GButton _noBtn;
        
        //bigNumBtn
        private GButton _bigNumBtn;
        
        //numberInput
        private GTextInput _numberInput;
        
        //需要执行的动作
        private Action<object[]> callBack;

        //需要透传的参数
        private object[] param;
        //最大数
        private int maxNum;
        //询问啥
        private string askContent;
        
        /// <inheritdoc />
        /// <summary>
        /// 初始化
        /// </summary>
        public override void InitUI()
        {
          
            //获取各组件
            _gCmp = this.GObject.asCom;
            _noBtn = this._gCmp.GetChild("noBtn").asButton;
            _yesBtn = this._gCmp.GetChild("yesBtn").asButton;
            _bigNumBtn = this._gCmp.GetChild("bigNumBtn").asButton;
            _numberInput = this._gCmp.GetChild("numberInput").asTextInput;
            _numberInput.text = "1";
            _contentText = this._gCmp.GetChild("contentText").asTextField;
            //按钮事件
            //no
            _noBtn.onClick.Add(this.Close);
            //yes
            _yesBtn.onClick.Add(() =>
            {
                
                //询问是否
                if(!string.IsNullOrEmpty(askContent))
                {
                    AskPanel askPanel = (AskPanel) UI.Open(typeof(AskPanel));
                    this.param[1] = int.Parse(this._numberInput.text);
                    this.callBack += ClosePanel;
                    askPanel.SetPanelInfo(askContent, this.callBack,this.param);
                }
                else
                {
                    this.param[1] = int.Parse(this._numberInput.text);
                    this.callBack += ClosePanel;
                    this.callBack.Invoke(this.param);
                }
                
               // this.Close();
            });
            //numberInput
            _numberInput.onChanged.Add(() =>
            {
                if(int.Parse(this._numberInput.text) >this.maxNum)
                {
                    this._numberInput.text = this.maxNum.ToString();
                }

                if (int.Parse(this._numberInput.text) < 1)
                {
                    this._numberInput.text = "1";
                }
            });
            //bigNumBtn
            this._bigNumBtn.onClick.Add(() =>
            {
                this._numberInput.text = this.maxNum.ToString();
            });
            
            //drag
            this._gCmp.draggable = true;

        }

        private void ClosePanel(object[] obj)
        {
            this.Close();
        }

        /// <summary>
        /// 设置面板数据
        /// </summary>
        /// <param name="enterContent">输入的文本</param>
        /// <param name="askContent">一会询问的内容</param>
        /// <param name="maxNum">最大数</param>
        /// <param name="callBack">回调方法</param>
        /// <param name="param">回调参数</param>
        public void SetPanelInfo(string enterContent,string askContent,int maxNum, Action<object[]> callBack,object[] param)
        {
            this._contentText.text = enterContent;
            this.askContent = askContent;
            this.callBack = callBack;
            //5个0.做一个冗余。
            this.param = param ?? new object[] {0,0,0,0,0};
            this.maxNum = maxNum;
        }

        public override Task HideEvent()
        {
            this.callBack = null;
            this.askContent = null;
            this.callBack = null;
            this.param = null;
            this.maxNum = -1;
            this.Close();
            return base.HideEvent();
        }

        public override void Display()
        {
            GRoot.inst.ShowPopup(this.Window);
            this.Window.Center();
        }
    }
}