/**
 * 背包系统_倒计时
 */

using System;
using System.Threading.Tasks;
using ETModel;
using FairyGUI;
using UnityEngine;
using UnityEngine.SocialPlatforms;

namespace ETHotfix
{
    public class CDTimer: BaseUIForms
    {
        public CDTimer()
        {
            this.pakName = "Inventory";
            this.cmpName = "CDBar";
            this.CurrentUIType.NeedClearingStack = false;
            this.CurrentUIType.UIForms_ShowMode = UIFormsShowMode.ReverseChange;
            this.CurrentUIType.UIForms_Type = UIFormsType.Window;
        }

        //面板组件
        private GComponent _gCmp;

        //bar
        private GProgressBar _bar;

        //倒计时的tween，用于kill掉倒计时。
        private GTweener gTweener;

        //计时结束后需要执行的动作
        private Action<object[]> callBack;

        /// <inheritdoc />
        /// <summary>
        /// 初始化
        /// </summary>
        public override void InitUI()
        {
            //获取各组件
            _gCmp = this.GObject.asCom;
            _bar = this._gCmp.GetChild("bar").asProgress;
            _bar.value = 0;
            _bar.max = 100;
        }

        /// <summary>
        /// 设置定时器
        /// </summary>
        /// <param name="callBack"></param>
        /// <param name="param"></param>
        /// <param name="duration"></param>
        public void StartTimer(Action<object[]> callBack, object[] param, float duration)
        {
            this.callBack = callBack;
            gTweener = this._bar.TweenValue(100, duration).OnComplete(() =>
            {
                this.callBack?.Invoke(param);
                this.Close();
            });
        }

        /// <summary>
        /// 触碰空闲区域，UI关闭，并中断bar的tween
        /// </summary>
        /// <returns></returns>
        public override Task HideEvent()
        {
            //杀死tweener,不再执行 。
            this.gTweener?.Kill();
            this.callBack = null;
            this.Close();
            return base.HideEvent();
        }

        public override void Display()
        {
            GRoot.inst.ShowPopup(this._gCmp);
            this._gCmp.Center();
        }
    }
}