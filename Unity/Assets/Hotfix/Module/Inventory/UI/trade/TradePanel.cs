/**
 * 交易面板
 * TODO
 */

using System;
using System.Threading.Tasks;
using ETModel;
using FairyGUI;
using UnityEngine;

namespace ETHotfix
{
    public class TradePanel: BaseUIForms
    {
        public TradePanel()
        {
            this.pakName = "Inventory";
            this.cmpName = "tradePanel";
            this.CurrentUIType.NeedClearingStack = false;
            this.CurrentUIType.UIForms_ShowMode = UIFormsShowMode.Normal;
            this.CurrentUIType.UIForms_Type = UIFormsType.Window;
        }

        public static TradePanel Instance;

        //背包组件
        private GComponent _gCmp;

        //交易方头像
        private GLoader _otherIcon;

        //交易方名称
        private GTextField _otherNameText;

        //交易方金币数量
        private GTextField _otherGoldText;

        //交易方状态
        private GTextField _otherStatusText;

        //交易方的交换的物品
        private GList _otherItemList;

        //背包的金币数量
        private GTextField _bagGoldText;

        //我的交易状态
        private GTextField _myStatusText;

        //我的金币输入框
        private GTextInput _myGoldInput;

        //我的物品框
        private GList _myItemList;

        //背包的物品框
        private GList _bgItemList;

        //no按钮
        private GButton _noBtn;

        //yes按钮
        private GButton _yesBtn;

        /// <summary>
        /// 初始化
        /// </summary>
        public override void InitUI()
        {
            Instance = this;
            //获取各组件
            _gCmp = this.GObject.asCom;
            _otherIcon = this._gCmp.GetChild("otherIcon").asLoader;
            _otherNameText = this._gCmp.GetChild("otherNameText").asTextField;
            _otherGoldText = this._gCmp.GetChild("otherGoldText").asTextField;
            _otherStatusText = this._gCmp.GetChild("otherStatusText").asTextField;
            _otherItemList = this._gCmp.GetChild("otherItemList").asList;
            _bagGoldText = this._gCmp.GetChild("bagGoldText").asTextField;
            _myStatusText = this._gCmp.GetChild("myStatusText").asTextField;
            _myGoldInput = this._gCmp.GetChild("myGoldInput").asTextInput;
            _myItemList = this._gCmp.GetChild("_myItemList").asList;
            _bgItemList = this._gCmp.GetChild("bgItemList").asList;
            _noBtn = this._gCmp.GetChild("noBtn").asButton;
            _yesBtn = this._gCmp.GetChild("yesBtn").asButton;
            //位置设置
            this._gCmp.SetPivot(0.5f, 0.5f);
            this._gCmp.Center();
            //按钮事件
            _myGoldInput.onChanged.Add(OnGoldInputChanged);
             _noBtn.onClick.Add(this.NoBtnClickEvent);
            _yesBtn.onClick.Add(this.YesBtnClickEvent);
            //开启虚拟列表_背包
            this._bgItemList.SetVirtual();
            this._bgItemList.defaultItem = "ui://Inventory/itemCellCmp";
            this._bgItemList.itemRenderer = RenderBagListItem;
            this._bgItemList.onClickItem.Add(this.BagItemClickEvent);
            this._bgItemList.onRightClickItem.Add(this.BagItemRightClickEvent);
            this._bgItemList.numItems = ETModel.Game.Scene.GetComponent<InventoryCmp>().MaxSize;
            //开启虚拟列表_我的商品
            this._myItemList.SetVirtual();
            this._myItemList.defaultItem = "ui://Inventory/itemCellCmp";
            this._myItemList.itemRenderer = RenderMyTradeItem;
            this._myItemList.onClickItem.Add(this.MyItemClickEvent);
            this._myItemList.onRightClickItem.Add(this.MyItemRightClickEvent);
            this._myItemList.numItems = 5;
            //开启虚拟列表_对方的商品
            this._otherItemList.SetVirtual();
            this._otherItemList.defaultItem = "ui://Inventory/itemCellCmp";
            this._otherItemList.itemRenderer = RenderAnotherTradeItem;
            this._otherItemList.onClickItem.Add(AnotherItemClickEvent);
            this._otherItemList.numItems = 5;
        }

        /// <summary>
        /// 取消按钮。取消交易。
        /// </summary>
        /// <param name="context"></param>
        private void NoBtnClickEvent(EventContext context)
        {
            ETModel.Game.Scene.GetComponent<TradeMgrCmpClient>().CancelTrade();
        }

        /// <summary>
        /// 确定按钮
        /// </summary>
        /// <param name="context"></param>
        private void YesBtnClickEvent(EventContext context)
        {
             ETModel.Game.Scene.GetComponent<TradeMgrCmpClient>().ConfirmTrade();
        }

        /// <summary>
        /// 点击对方物品
        /// </summary>
        /// <param name="context"></param>
        /// <exception cref="NotImplementedException"></exception>
        private void AnotherItemClickEvent(EventContext context)
        {
            GComponent gItem = (GComponent) context.data;
            if (gItem.GetChild("itemIcon").asLoader.url == "")
            {
                return;
            }

            int index = this._bgItemList.GetChildIndex(gItem);
            Trade currentTrade = ETModel.Game.Scene.GetComponent<TradeMgrCmpClient>().CurrentTrade;
            Item item = currentTrade.AnotherTradeItemList[index];
            TradeInfoPanel tradeInfoPanel = (TradeInfoPanel) UI.Open(typeof(TradeInfoPanel));
            tradeInfoPanel.SetPanelInfo(index, item, LocalLanguage.Close, null);
        }


        /// <summary>
        /// 我要交易物品的右键点击事件。
        /// </summary>
        /// <param name="context"></param>
        /// <exception cref="NotImplementedException"></exception>
        private void MyItemRightClickEvent(EventContext context)
        {
            GComponent gItem = (GComponent) context.data;
            if (gItem.GetChild("itemIcon").asLoader.url == "")
            {
                return;
            }

            int index = this._bgItemList.GetChildIndex(gItem);
            Item item = ETModel.Game.Scene.GetComponent<TradeMgrCmpClient>().CurrentTrade.MyTradeItemList[index];
            this.MyTradeItemCallback(new object[] { item, item.GetComponent<InventoryDataCmp>().CountNum });
        }

        /// <summary>
        /// 我的交易物品的左键点击事件
        /// </summary>
        /// <param name="context"></param>
        private void MyItemClickEvent(EventContext context)
        {
            GComponent gItem = (GComponent) context.data;
            if (gItem.GetChild("itemIcon").asLoader.url == "")
            {
                return;
            }

            int index = this._bgItemList.GetChildIndex(gItem);
            Trade currentTrade = ETModel.Game.Scene.GetComponent<TradeMgrCmpClient>().CurrentTrade;
            Item item = currentTrade.MyTradeItemList[index];
            TradeInfoPanel tradeInfoPanel = (TradeInfoPanel) UI.Open(typeof(TradeInfoPanel));
            tradeInfoPanel.SetPanelInfo(index, item, LocalLanguage.Cancel, this.MyTradeItemCallback);
        }

        /// <summary>
        /// 我用于交易物品的点击回调事
        /// </summary>
        /// <param name="param"></param>
        public void MyTradeItemCallback(object[] param)
        {
            //要执行的item
            Item item = (Item) param[0];
            //数量
            int count = (int) param[1];
            //从交易列表移除到背包列表
            bool addMyTradeItemList = ETModel.Game.Scene.GetComponent<TradeMgrCmpClient>().RemoveMyTradeItemList(item, count);
            if (addMyTradeItemList)
            {
                //更新Ui。
                this.RefreshTradePanel();
            }
            else
            {
                //提示不能y移除该物品
                UI.Tip(LocalLanguage.CantRemoveItem);
            }
        }

        /// <summary>
        /// 背包物品右键点击，直接将该物品以及所有数量放入交易栏。
        /// </summary>
        /// <param name="context"></param>
        private void BagItemRightClickEvent(EventContext context)
        {
            GComponent gItem = (GComponent) context.data;
            if (gItem.GetChild("itemIcon").asLoader.url == "")
            {
                return;
            }

            int index = this._bgItemList.GetChildIndex(gItem);
            Item item = ETModel.Game.Scene.GetComponent<TradeMgrCmpClient>().CurrentTrade.MyItemList[index];
            this.BagItemCallback(new object[] { item, item.GetComponent<InventoryDataCmp>().CountNum });
        }

        /// <summary>
        /// 背包物品左键点击
        /// 展示选择用途面板，展示相关信息，选择是否放入交易栏。
        /// </summary>
        /// <param name="context"></param>
        private void BagItemClickEvent(EventContext context)
        {
            GComponent gItem = (GComponent) context.data;
            if (gItem.GetChild("itemIcon").asLoader.url == "")
            {
                return;
            }

            int index = this._bgItemList.GetChildIndex(gItem);
            Trade currentTrade = ETModel.Game.Scene.GetComponent<TradeMgrCmpClient>().CurrentTrade;
            Item item = currentTrade.MyItemList[index];
            TradeInfoPanel tradeInfoPanel = (TradeInfoPanel) UI.Open(typeof(TradeInfoPanel));
            tradeInfoPanel.SetPanelInfo(index, item, LocalLanguage.Trade, this.BagItemCallback);
        }

        /// <summary>
        /// 背包物品的点击回调事件 
        /// </summary>
        /// <param name="param"></param>
        public void BagItemCallback(object[] param)
        {
            //要执行的item
            Item item = (Item) param[0];
            //数量
            int count = (int) param[1];
            bool addMyTradeItemList = ETModel.Game.Scene.GetComponent<TradeMgrCmpClient>().AddMyTradeItemList(item, count);
            if (addMyTradeItemList)
            {
                //更新Ui。
                this.RefreshTradePanel();
            }
            else
            {
                //提示不能添加该物品
                UI.Tip(LocalLanguage.CantAddItem);
            }
        }

        /// <summary>
        /// 输入金额
        /// </summary> 
        private void OnGoldInputChanged()
        {
            if (Int32.Parse(this._myGoldInput.text) > ETModel.Game.Scene.GetComponent<TradeMgrCmpClient>().CurrentTrade.Gold)
            {
                this._myGoldInput.text = ETModel.Game.Scene.GetComponent<TradeMgrCmpClient>().CurrentTrade.Gold.ToString();
            }

            ETModel.Game.Scene.GetComponent<TradeMgrCmpClient>().UpdateMyTradeGold(Int32.Parse(this._myGoldInput.text));
        }

        /// <summary>
        /// 渲染我的背包
        /// </summary>
        /// <param name="index"></param>
        /// <param name="item"></param>
        /// <exception cref="NotImplementedException"></exception>
        private void RenderBagListItem(int index, GObject item)
        {
            GComponent gItem = (GComponent) item;
            Item itemData = ETModel.Game.Scene.GetComponent<TradeMgrCmpClient>().CurrentTrade.MyItemList[index];
            if (itemData == null)
            {
                gItem.GetChild("itemIcon").asLoader.url = "";
                gItem.GetChild("effectImg").asLoader.url = "";
                gItem.GetChild("numText").asTextField.text = "";
            }
            else
            {
                gItem.GetChild("itemIcon").asLoader.url = itemData.GetIcon();
                gItem.GetChild("effectImg").asLoader.url = itemData.GetEffectIcon();
                gItem.GetChild("numText").asTextField.text = itemData.GetComponent<InventoryDataCmp>().CountNum.ToString();
            }
        }

        /// <summary>
        /// 渲染我的交易列表
        /// </summary>
        /// <param name="index"></param>
        /// <param name="item"></param>
        private void RenderMyTradeItem(int index, GObject item)
        {
            GComponent gItem = (GComponent) item;
            Item itemData = ETModel.Game.Scene.GetComponent<TradeMgrCmpClient>().CurrentTrade.MyTradeItemList[index];
            if (itemData == null)
            {
                gItem.GetChild("itemIcon").asLoader.url = "";
                gItem.GetChild("effectImg").asLoader.url = "";
                gItem.GetChild("numText").asTextField.text = "";
            }
            else
            {
                gItem.GetChild("itemIcon").asLoader.url = itemData.GetIcon();
                gItem.GetChild("effectImg").asLoader.url = itemData.GetEffectIcon();
                gItem.GetChild("numText").asTextField.text = itemData.GetComponent<InventoryDataCmp>().CountNum.ToString();
            }
        }

        /// <summary>
        /// 渲染对方的交易列表
        /// </summary>
        /// <param name="index"></param>
        /// <param name="item"></param>
        private void RenderAnotherTradeItem(int index, GObject item)
        {
            GComponent gItem = (GComponent) item;
            Item itemData = ETModel.Game.Scene.GetComponent<TradeMgrCmpClient>().CurrentTrade.AnotherTradeItemList[index];
            if (itemData == null)
            {
                gItem.GetChild("itemIcon").asLoader.url = "";
                gItem.GetChild("effectImg").asLoader.url = "";
                gItem.GetChild("numText").asTextField.text = "";
            }
            else
            {
                gItem.GetChild("itemIcon").asLoader.url = itemData.GetIcon();
                gItem.GetChild("effectImg").asLoader.url = itemData.GetEffectIcon();
                gItem.GetChild("numText").asTextField.text = itemData.GetComponent<InventoryDataCmp>().CountNum.ToString();
            }
        }

        /// <summary>
        /// 刷新背包
        /// </summary>
        public void RefreshTradePanel()
        {
            Trade trade = ETModel.Game.Scene.GetComponent<TradeMgrCmpClient>().CurrentTrade;
            //todo 头像  _otherIcon.url = trade.AnotherTrader.
            //todo 名字_otherNameText.text = trade.AnotherTrader.xxx
            _otherGoldText.text = trade.MyTradeGold.ToString();
            _otherStatusText.text = trade.AnotherTradeState.ToString();
            _bagGoldText.text = trade.Gold.ToString();
            _myStatusText.text = trade.MyTradeState.ToString();
            _myGoldInput.text = trade.MyTradeGold.ToString();
            //刷新列表
            this._bgItemList.RefreshVirtualList();
            this._myItemList.RefreshVirtualList();
            this._otherItemList.RefreshVirtualList();
        }

        public override void Display()
        {
            base.Display();
            this._gCmp.TweenScale(new Vector2(1, 1), 0.3f);
            RefreshTradePanel();
        }

        /// <summary>
        /// 退出事件
        /// </summary>
        /// <returns></returns>
        public override Task HideEvent()
        {
            Instance = null;
            //先关闭所有的popUp
            GRoot.inst.HidePopup();
            return Task.CompletedTask;
        }
    }
}