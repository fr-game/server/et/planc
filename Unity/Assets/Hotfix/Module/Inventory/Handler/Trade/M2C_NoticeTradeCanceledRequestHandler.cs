/**
 * 服务端通知另一个交易参与者交易取消了 或者交易发生异常了。
 */

using ETModel;

namespace ETHotfix
{
    [MessageHandler]
    public class M2C_NoticeTradeCanceledRequestHandler: AMHandler<M2C_BroadcastTradeStart>
    {
        protected override void Run(ETModel.Session session, M2C_BroadcastTradeStart message)
        {
            if (ETModel.Game.Scene.GetComponent<TradeMgrCmpClient>().CurrentTrade.Id != message.TradeId)
            {
                Log.Error($"交易异常");
            }
            //关闭交易
            ETModel.Game.Scene.GetComponent<TradeMgrCmpClient>().DestroyTrade();
            //关闭交易
            UI.Close(typeof(TradePanel));
        }
    }
}