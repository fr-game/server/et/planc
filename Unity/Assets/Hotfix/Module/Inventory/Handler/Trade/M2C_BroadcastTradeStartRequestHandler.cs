/**
 * 服务端通知客户端，交易开始了。
 */

using ETModel;

namespace ETHotfix
{
    [MessageHandler]
    public class M2C_BroadcastTradeStartRequestHandler: AMHandler<M2C_BroadcastTradeStart>
    {
        protected override void Run(ETModel.Session session, M2C_BroadcastTradeStart message)
        {
            if (ETModel.Game.Scene.GetComponent<TradeMgrCmpClient>().CurrentTrade.MyTradeState != TradeState.WaitingOtherYnc ||
                ETModel.Game.Scene.GetComponent<TradeMgrCmpClient>().CurrentTrade.Id != message.TradeId)
            {
                //关闭交易
                ETModel.Game.Scene.GetComponent<TradeMgrCmpClient>().DestroyTrade();
                UI.Close(typeof(TradePanel));
            }
            else
            {
                ETModel.Game.Scene.GetComponent<TradeMgrCmpClient>().CurrentTrade.MyTradeState = TradeState.Trading;
                ETModel.Game.Scene.GetComponent<TradeMgrCmpClient>().CurrentTrade.AnotherTradeState = TradeState.Trading;
                TradePanel.Instance?.RefreshTradePanel();
            }
           
        }
    }
}