/**
 * 服务端通知被交易人 交易请求。
 * 接收到交易请求。
 */
using ETModel;

namespace ETHotfix
{
    [MessageHandler]
    public class M2C_NoticeInitTradeRequestHandler:AMHandler<M2C_NoticeInitTrade>
    {
        protected override async void Run(ETModel.Session session, M2C_NoticeInitTrade message)
        {
            //这里发出了相关事件。
           await ETModel.Game.Scene.GetComponent<TradeMgrCmpClient>().ReceiveTrade(message.SourcePlayerId);
            //记录交易ID
            ETModel.Game.Scene.GetComponent<TradeMgrCmpClient>().CurrentTrade.Id = message.TradeId;
        }
    }
}