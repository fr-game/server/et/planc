/**
 *服务端通知另一个交易参与者确认交易，等待交易结束了
 */

using ETModel;

namespace ETHotfix
{
    [MessageHandler]
    public class M2C_NoticeAffirmTradeRequestHandler: AMHandler<M2C_NoticeAffirmTrade>
    {
        protected override void Run(ETModel.Session session, M2C_NoticeAffirmTrade message)
        {
            if (ETModel.Game.Scene.GetComponent<TradeMgrCmpClient>().CurrentTrade.AnotherTradeState != TradeState.WaitingOtherYnc ||
                ETModel.Game.Scene.GetComponent<TradeMgrCmpClient>().CurrentTrade.Id != message.TradeId)
            {
                //关闭交易
                ETModel.Game.Scene.GetComponent<TradeMgrCmpClient>().DestroyTrade();
                UI.Close(typeof(TradePanel));
            }
            else
            {
                //同意交易
                ETModel.Game.Scene.GetComponent<TradeMgrCmpClient>().AnotherConfirmTrade();
               // 展示UI。展示出当前的状态为交易中。
               TradePanel.Instance?.RefreshTradePanel();
            }
            
        }
    }
}