/**
 *服务端通知另一个交易参与者的交易消息
 */

using ETModel;

namespace ETHotfix
{
    [MessageHandler]
    public class M2C_NoticePutTradeCommodityRequestHandler: AMHandler<M2C_NoticePutTradeCommodity>
    {
        protected override void Run(ETModel.Session session, M2C_NoticePutTradeCommodity message)
        {
            if (ETModel.Game.Scene.GetComponent<TradeMgrCmpClient>().CurrentTrade.AnotherTradeState != TradeState.WaitingOtherYnc ||
                ETModel.Game.Scene.GetComponent<TradeMgrCmpClient>().CurrentTrade.Id != message.TradeId)
            {
                //关闭交易
                ETModel.Game.Scene.GetComponent<TradeMgrCmpClient>().DestroyTrade();
            }
            else
            {
                ETModel.Game.Scene.GetComponent<TradeMgrCmpClient>().CurrentTrade.MyTradeState = TradeState.Trading;
                //收到货后，自身状态也变成了Trading
                ETModel.Game.Scene.GetComponent<TradeMgrCmpClient>().CurrentTrade.AnotherTradeState = TradeState.Trading;
                //接收货物
                ETModel.Game.Scene.GetComponent<TradeMgrCmpClient>()
                        .UpdateAnotherTradeCommodity(MessageHelper.DeSerializeCommodity(message.Commodity));
                // 展示UI。展示出当前的状态为交易中。
                TradePanel.Instance?.RefreshTradePanel();
            }
        }
    }
}