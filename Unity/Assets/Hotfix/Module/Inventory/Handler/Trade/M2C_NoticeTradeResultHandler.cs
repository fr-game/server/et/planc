/**
 *服务端通知另一个交易参与者确认交易，交易结束。
 */

using ETModel;

namespace ETHotfix
{
    [MessageHandler]
    public class M2C_NoticeTradeResultHandler: AMHandler<M2C_NoticeTradeResult>
    {
        protected override async void Run(ETModel.Session session, M2C_NoticeTradeResult message)
        {
            //关闭交易
            ETModel.Game.Scene.GetComponent<TradeMgrCmpClient>().DestroyTrade();
            //更新背包
            //发送请求获取背包信息。
            Actor_Unit_GetInventoryInfosResponse res =
                    (Actor_Unit_GetInventoryInfosResponse) await Game.Scene.GetComponent<SessionComponent>().Session
                            .Call(new Actor_Unit_GetInventoryInfosRequest());
            if (res.Error != ErrorCode.ERR_Success)
            {
                Log.Error("获取背包失败");
            }
            MessageHelper.DeSerializeInventory(res);
            //刷新背包
            BagPanel.Instance.UpdateBag();
            //关闭交易面板
            UI.Close(typeof(TradePanel));
        }
    }
}