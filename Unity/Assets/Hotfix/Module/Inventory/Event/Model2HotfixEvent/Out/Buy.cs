/**
  * 背包系统_购买完成事件。
  */

using System.Collections.Generic;
using ETModel;
using Google.Protobuf.Collections;

namespace ETHotfix
{
    [Event(ETModel.EventIdType.Buy)]
    public class Buy: AEvent<List<Item>,int,string>
    {
        public override void Run(List<Item> items,int money,string moneyType)
        {
           
            if (GameSettings.GameMode == GameMode.net)
            {
                Actor_Unit_BuyItemRequest request = new Actor_Unit_BuyItemRequest();
                request.MoneyType = moneyType;
                request.Money = money;
                request.Item = new RepeatedField<ItemInfo>();
                foreach (Item item in items)
                {
                    request.Item.Add(MessageHelper.SerializeItemInfo(item));
                }
                //同步服务器。
                Game.Scene.GetComponent<SessionComponent>().Session
                        .Send(request);
            }
            else
            {
                //持久化到本地硬盘
                ETModel.Game.EventSystem.Run(ETModel.EventIdType.InventoryUpdateEvent);
            }
        }
    }
}