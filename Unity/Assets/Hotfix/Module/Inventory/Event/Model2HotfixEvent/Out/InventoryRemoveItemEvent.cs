/**
 * 背包移除物品
 */

using ETModel;

namespace ETHotfix
{
    [Event(ETModel.EventIdType.InventoryRemoveItemDataEvent)]
    public class InventoryRemoveItemEvent: AEvent<long, int>
    {
        public override async void Run(long itemId, int num)
        {
            Log.Debug("背包移除");
            if (GameSettings.GameMode == GameMode.net)
            {
                Actor_Unit_RemoveInventoryItemResponse response = (Actor_Unit_RemoveInventoryItemResponse) await Game.Scene
                        .GetComponent<SessionComponent>().Session
                        .Call(new Actor_Unit_RemoveInventoryItemRequest { ItemId = itemId, ItemCount = num });
                if (response.Error != ErrorCode.ERR_Success)
                {
                    UI.Tip(LocalLanguage.Nothing);
                }
            }
            else
            {
                //持久化到本地。
                ETModel.Game.EventSystem.Run(ETModel.EventIdType.InventoryUpdateEvent);
            }
        }
    } //class_end
}