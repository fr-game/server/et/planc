/**
 * 添加Item到背包事件_发送给服务器或者同步到本地
 */

using System.Collections.Generic;
using ETModel;
using Google.Protobuf.Collections;

namespace ETHotfix
{
    //添加实体Item
    [Event(ETModel.EventIdType.InventoryAddItemEvent)]
    public class InventoryAddItemEvent0: AEvent<List<Item>>
    {
        public override async void Run(List<Item> items)
        {
            Log.Debug("添加Item");
            if (GameSettings.GameMode == GameMode.net)
            {
                Actor_Unit_AddInventoryItemRequest request = new Actor_Unit_AddInventoryItemRequest();
                request.Item =new RepeatedField<ItemInfo>();
                foreach (var item in items)
                {
                    request.Item.Add(MessageHelper.SerializeItemInfo(item));
                }
                Actor_Unit_AddInventoryItemResponse response = (Actor_Unit_AddInventoryItemResponse) await Game.Scene.GetComponent<SessionComponent>()
                        .Session.Call(request);
                if (response.Error != ErrorCode.ERR_Success)
                {
                    UI.Tip(LocalLanguage.Nothing);
                }
            }
            else
            {
                //持久化到本地。
                ETModel.Game.EventSystem.Run(ETModel.EventIdType.InventoryUpdateEvent);
            }
        }
    } //class_end
  
 
}