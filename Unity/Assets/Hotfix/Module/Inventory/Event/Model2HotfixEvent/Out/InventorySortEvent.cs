/**
 * 背包排序
 */

using ETModel;

namespace ETHotfix
{
    [Event(ETModel.EventIdType.InventorySortEvent)]
    public class InventorySortEvent:AEvent
    {
        public override void Run()
        {
            Log.Debug("背包排序");
            if (GameSettings.GameMode == GameMode.net)
            {
                //同步服务器
                Game.Scene.GetComponent<SessionComponent>().Session.Send(new Actor_Unit_SortBagRequest());
            }
            else
            {
                //持久化到本地。
                ETModel.Game.EventSystem.Run(ETModel.EventIdType.InventoryUpdateEvent);
            }

        }
    }//class_end
}