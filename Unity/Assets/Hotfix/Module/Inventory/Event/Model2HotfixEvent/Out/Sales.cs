/**
  * 背包系统_出售完成事件。
  */

using ETModel;

namespace ETHotfix
{
    [Event(ETModel.EventIdType.Sales)]
    public class Sales: AEvent<long,int,int>
    {
        public override void Run(long itemId,int num,int price)
        {
            
            if (GameSettings.GameMode == GameMode.net)
            {
                Actor_Unit_SalesItemRequest request = new Actor_Unit_SalesItemRequest();
                request.Num = num;
                request.Price = price;
                request.ItemId = itemId;
                //同步服务器。
                Game.Scene.GetComponent<SessionComponent>().Session.Send(request);
            }
            else
            {
                //持久化到本地硬盘
                ETModel.Game.EventSystem.Run(ETModel.EventIdType.InventoryUpdateEvent);
            }
        }
    }
}