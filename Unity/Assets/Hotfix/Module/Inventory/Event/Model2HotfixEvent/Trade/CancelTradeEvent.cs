/**
 * 背包系统_取消交易的事件
 */

using System;
using ETModel;

namespace ETHotfix
{
    [Event(ETModel.EventIdType.CancelTradeEvent)]
    public class CancelTradeEvent: AEvent
    {
        public override async void Run()
        {
            try
            {
                //发送请求
                M2C_CancelTradeResponse response = (M2C_CancelTradeResponse) await Game.Scene.GetComponent<SessionComponent>().Session
                        .Call(new C2M_CancelTradeRequest { TradeId = ETModel.Game.Scene.GetComponent<TradeMgrCmpClient>().TradeId ,UnitId = ETModel.Game.Scene.GetComponent<UnitComponent>().MyUnit.Id});
                UI.Close(typeof(TradePanel));
            }
            catch (Exception e)
            {
                UI.Tip(e.Message);
            }
        }
    }
}