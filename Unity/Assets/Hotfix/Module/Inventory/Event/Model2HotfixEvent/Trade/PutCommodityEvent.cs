/**
 * 背包系统_交易，放置交易商品事件
    */

using System;
using ETModel;
using Google.Protobuf.Collections;

namespace ETHotfix
{
    [Event(ETModel.EventIdType.PutCommodity)]
    public class PutCommodityEvent: AEvent
    {
        public override async void Run()
        {
            try
            {
                TradeMgrCmpClient tradeMgrCmpClient = ETModel.Game.Scene.GetComponent<TradeMgrCmpClient>();

                //发送请求
                M2C_PutTradeCommodityResponse response = (M2C_PutTradeCommodityResponse) await Game.Scene.GetComponent<SessionComponent>().Session
                        .Call(new C2M_PutTradeCommodityRequest
                        {
                                TradeId = ETModel.Game.Scene.GetComponent<TradeMgrCmpClient>().TradeId,
                                Commodity = MessageHelper.SerializeCommodity(tradeMgrCmpClient.CurrentTrade.MyTradeItemList,
                                                                             tradeMgrCmpClient.CurrentTrade.MyTradeGold),UnitId = ETModel.Game.Scene.GetComponent<UnitComponent>().MyUnit.Id
                        });
                if (response.Error != ErrorCode.ERR_Success)
                {
                    ETModel.Game.Scene.GetComponent<TradeMgrCmpClient>()?.DestroyTrade();
                    //关闭UI
                    UI.Close(TradePanel.Instance.GetType());
                }
            }
            catch (Exception e)
            {
                UI.Tip(e.Message);
            }
        }
    }
}