/**
 * 背包系统_接收到交易请求事件
 */

using System;
using ETModel;

namespace ETHotfix
{
    [Event(ETModel.EventIdType.ReceiveTradeEvent)]
    public class ReceiveTradeEvent: AEvent<long>
    {
        public override void Run(long unitId)
        {
            //开启询问UI 
            Unit unit = ETModel.Game.Scene.GetComponent<UnitComponent>().Get(unitId);
            AskPanel askPanel = (AskPanel) UI.Open(typeof(AskPanel));
            object[] param = { unitId };
            askPanel.SetPanelInfo(LocalLanguage.AskTrade(unit.Id.ToString()), OpenTradePanel, param, CloseTrade);
        }

        private void CloseTrade()
        {
            //关闭交易
            ETModel.Game.Scene.GetComponent<TradeMgrCmpClient>().DestroyTrade();
            //发送请求告知服务器_拒绝
            Game.Scene.GetComponent<SessionComponent>().Session.Send(new C2M_AnswerAgreeTradeRequest { Agree = false });
        }

        private async void OpenTradePanel(object[] obj)
        {
            long unitId = (long) obj[0];
            ETModel.Game.Scene.GetComponent<TradeMgrCmpClient>().CurrentTrade.MyTradeState = TradeState.WaitingOtherYnc;
            ETModel.Game.Scene.GetComponent<TradeMgrCmpClient>().CurrentTrade.AnotherTradeState = TradeState.WaitingOtherYnc;
            //发送请求告知服务器_接受
            M2C_AnswerAgreeTradeResponse response = (M2C_AnswerAgreeTradeResponse) await Game.Scene.GetComponent<SessionComponent>().Session
                    .Call(new C2M_AnswerAgreeTradeRequest { Agree = true,UnitId = ETModel.Game.Scene.GetComponent<UnitComponent>().MyUnit.Id });
            if (response.Error != ErrorCode.ERR_Success)
            {
                //关闭交易
                ETModel.Game.Scene.GetComponent<TradeMgrCmpClient>().DestroyTrade();
                //错误提示
                UI.Tip(LocalLanguage.TradeStarFail);
            }
            //开启UI
            TradePanel tradePanel = (TradePanel) UI.Open(typeof(TradePanel));
            tradePanel.RefreshTradePanel();
        }
    }
}