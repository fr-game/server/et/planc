/**
 * 背包系统_交易开始事件
 */

using System;
using ETModel;

namespace ETHotfix
{
    [Event(ETModel.EventIdType.StartTradeEvent)]
    public class StartTradeEvent: AEvent<long>
    {
        public override async void Run(long unitId)
        {
            try
            {
                //发送请求
                M2C_InitTradeResponse response = (M2C_InitTradeResponse) await Game.Scene.GetComponent<SessionComponent>().Session
                        .Call(new C2M_InitTradeRequest()
                        {
                                SourcePlayerId = ETModel.Game.Scene.GetComponent<UnitComponent>().MyUnit.Id,
                                AnotherPlayerId = unitId,
                                TradeId = ETModel.Game.Scene.GetComponent<TradeMgrCmpClient>().TradeId
                        });
                if (response.Error != ErrorCode.ERR_Success)
                {
                    ETModel.Game.Scene.GetComponent<TradeMgrCmpClient>().DestroyTrade();
                    //交易开启失败
                    UI.Tip(LocalLanguage.TradeStarFail);
                    return;
                }

                //开始等待对方确认。
                ETModel.Game.Scene.GetComponent<TradeMgrCmpClient>().CurrentTrade.MyTradeState = TradeState.WaitingOtherYnc;
            }
            catch (Exception e)
            {
                ETModel.Game.Scene.GetComponent<TradeMgrCmpClient>().DestroyTrade();
                UI.Tip(e.Message);
            }
            //开启UI
            TradePanel tradePanel = (TradePanel) UI.Open(typeof(TradePanel));
            tradePanel.RefreshTradePanel();
        }
    }
}