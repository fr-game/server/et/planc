/**
 * 背包系统_交易，确认交易，等待交易结束
    */

using System;
using ETModel;
using Google.Protobuf.Collections;

namespace ETHotfix
{
    [Event(ETModel.EventIdType.ConfirmTradeEvent)]
    public class ConfirmTradeEvent: AEvent
    {
        public override async void Run()
        {
            try
            {
                //发送请求
                M2C_AffirmTradeResponse response = (M2C_AffirmTradeResponse) await Game.Scene.GetComponent<SessionComponent>().Session
                        .Call(new C2M_AffirmTradeRequest
                        {
                                TradeId = ETModel.Game.Scene.GetComponent<TradeMgrCmpClient>().TradeId,UnitId = ETModel.Game.Scene.GetComponent<UnitComponent>().MyUnit.Id
                        });
                if (response.Error != ErrorCode.ERR_Success)
                {
                    ETModel.Game.Scene.GetComponent<TradeMgrCmpClient>()?.DestroyTrade();
                    UI.Close(typeof(TradePanel));
                }
                //更新Ui的状态
            }
            catch (Exception e)
            {
                UI.Tip(e.Message);
            }
        }
    }
}