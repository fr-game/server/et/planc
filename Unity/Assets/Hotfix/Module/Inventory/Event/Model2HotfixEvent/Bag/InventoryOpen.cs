/**
  * 背包系统_打开背包
  */

using ETModel;

namespace ETHotfix
{
    [Event(ETModel.EventIdType.InventoryOpen)]
    public class InventoryOpen: AEvent
    {
        public override void Run()
        {
            //UI打开
            BagPanel bagPanel = BagPanel.Instance;
            if (bagPanel == null)
            {
                bagPanel = (BagPanel) UI.Open(typeof(BagPanel));
            }
            else
            {
                bagPanel.Display();
            }
        }
    }
}