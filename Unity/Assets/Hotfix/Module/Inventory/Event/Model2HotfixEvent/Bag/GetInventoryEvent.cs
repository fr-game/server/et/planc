/**
 * 通过服务器获取背包信息。
 * 一些基础属性，如上次的整理时间，这些放在Awake中。
 */

using System;
using System.Collections.Generic;
using ETModel;

namespace ETHotfix
{
    [Event(ETModel.EventIdType.GetInventoryFromNet)]
    public class GetInventoryEvent: AEvent
    {
        public override async void Run()
        {
            //发送请求获取背包信息。
            Actor_Unit_GetInventoryInfosResponse res =
                    (Actor_Unit_GetInventoryInfosResponse) await Game.Scene.GetComponent<SessionComponent>().Session
                            .Call(new Actor_Unit_GetInventoryInfosRequest());
            if (res.Error != ErrorCode.ERR_Success)
            {
                Log.Error("获取背包失败");
            }
            MessageHelper.DeSerializeInventory(res);
        }
    } //class_end
}