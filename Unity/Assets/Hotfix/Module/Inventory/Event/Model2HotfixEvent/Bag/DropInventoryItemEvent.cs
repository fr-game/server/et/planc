/**
  * 背包系统_增加物品
  */

using ETModel;

namespace ETHotfix
{
    [Event(ETModel.EventIdType.DropInventoryItemEvent)]
    public class DropInventoryItemEvent: AEvent
    {
        public override void Run()
        {
            //UI打开
            BagPanel.Instance?.UpdateBag();
        }
    }
}