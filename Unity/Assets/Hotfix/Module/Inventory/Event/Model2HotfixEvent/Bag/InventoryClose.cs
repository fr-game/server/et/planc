/**
  * 背包系统_关闭背包
  */
 
 using ETModel;
 
 namespace ETHotfix
 {
     [Event(ETModel.EventIdType.InventoryClose)]
     public class InventoryClose:AEvent
     {
         public override void Run()
         {
             BagPanel.Instance?.CloseBag();
         }
     }
 }