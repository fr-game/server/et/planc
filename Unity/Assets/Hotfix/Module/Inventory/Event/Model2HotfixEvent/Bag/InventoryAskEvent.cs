/**
  * 背包系统_询问数量事件 无用。
  */
 
 using ETModel;
 
 namespace ETHotfix
 {
     [Event(ETModel.EventIdType.InventoryAskEvent)]
     public class InventoryAskEvent:AEvent<string,Item>
     {
         public override void Run(string param,Item item)
         {
             //UI打开
         }
     }
 }