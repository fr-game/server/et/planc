/**
  * 背包系统_确认某事件的事件 无用
 * 事件包含：丢弃/购买/使用
  */
 
 using ETModel;
 
 namespace ETHotfix
 {
     [Event(ETModel.EventIdType.InventoryAffirmEvent)]
     public class InventoryAffirmEvent:AEvent<string,Item,int>
     {
         public override void Run(string param,Item item,int i)
         {
             //UI打开
         }
     }
 }