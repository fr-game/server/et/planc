 

using ETModel;

namespace ETHotfix
{
    [Event(ETModel.EventIdType.TestHotfixSubscribMonoEvent)]
    public class TestEvent: AEvent<string>
    {
        public override void Run(string param)
        {
            Log.Debug("这就是个测试事件");
        }
    }
}