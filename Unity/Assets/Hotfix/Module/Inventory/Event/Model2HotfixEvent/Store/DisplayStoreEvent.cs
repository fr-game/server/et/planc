/**
  * 背包系统_打开商店。
  */

using ETModel;

namespace ETHotfix
{
    [Event(ETModel.EventIdType.DisplayStoreEvent)]
    public class DisplayStoreEvent: AEvent<ETModel.StoreCmp>
    {
        public override void Run(ETModel.StoreCmp storeCmp)
        {
            if (StorePanel.Instance != null)
            {
                return;
            }
            StorePanel storePanel =  (StorePanel) UI.Open(typeof(StorePanel));
            storePanel.SetPanelInfo(storeCmp);
        }
    }
}