﻿using System;
using System.IO;
using System.Threading;
using Google.Protobuf;
using UnityEngine;

namespace ETModel
{
   

    public class Init: MonoBehaviour
    {
        public GameMode GameMode = GameMode.local;

        private async void Start()
        {
            try
            {
               // PlayerPrefs.SetString($"InventoryData_{52222232221}", "");
                SynchronizationContext.SetSynchronizationContext(OneThreadSynchronizationContext.Instance);

                DontDestroyOnLoad(gameObject);
                Game.EventSystem.Add(DLLType.Model, typeof(Init).Assembly);

                Game.Scene.AddComponent<GlobalConfigComponent>();
                Game.Scene.AddComponent<NetOuterComponent>();
                Game.Scene.AddComponent<ResourcesComponent>();
                Game.Scene.AddComponent<PlayerComponent>();
                Game.Scene.AddComponent<UnitComponent>();
                Game.Scene.AddComponent<ClientFrameComponent>();

                // 下载ab包
                await BundleHelper.DownloadBundle();
                Game.Hotfix.LoadHotfixAssembly();
                //UI
                Game.Scene.AddComponent<UIManagerComponent>();
                // 加载配置
                Game.Scene.GetComponent<ResourcesComponent>().LoadBundle("config.unity3d");
                Game.Scene.AddComponent<ConfigComponent>();
                Game.Scene.GetComponent<ResourcesComponent>().UnloadBundle("config.unity3d");
                Game.Scene.AddComponent<OpcodeTypeComponent>();
                Game.Scene.AddComponent<MessageDispatherComponent>();
                Game.Hotfix.GotoHotfix();
                //背包系统
                Game.Scene.AddComponent<InventoryConfigMrgCmp>();
                InventoryCmp inventoryCmp = Game.Scene.AddComponent<InventoryCmp>();
                GameSettings.GameMode = GameMode;
                inventoryCmp.InitInventory();
                Game.EventSystem.Run(EventIdType.InventoryOpen);
                //测试时间  
                long now = TimeHelper.Now();
                await Game.Scene.GetComponent<TimerComponent>().WaitAsync(1000);
                Log.Debug($"时间{TimeHelper.Now()}");
                Log.Debug($"时间{TimeHelper.Now() - now}");
            }
            catch (Exception e)
            {
                Log.Error(e);
            }
        }

        private void Update()
        {
            OneThreadSynchronizationContext.Instance.Update();
            Game.Hotfix.Update?.Invoke();
            Game.EventSystem.Update();
        }

        private void LateUpdate()
        {
            Game.Hotfix.LateUpdate?.Invoke();
            Game.EventSystem.LateUpdate();
        }

        private void OnApplicationQuit()
        {
            Game.Hotfix.OnApplicationQuit?.Invoke();
            Game.Close();
        }
    }
}