﻿namespace ETModel
{
    public static class EventIdType
    {
        public const string RecvHotfixMessage = "RecvHotfixMessage";
        public const string BehaviorTreeRunTreeEvent = "BehaviorTreeRunTreeEvent";
        public const string BehaviorTreeOpenEditor = "BehaviorTreeOpenEditor";
        public const string BehaviorTreeClickNode = "BehaviorTreeClickNode";
        public const string BehaviorTreeAfterChangeNodeType = "BehaviorTreeAfterChangeNodeType";
        public const string BehaviorTreeCreateNode = "BehaviorTreeCreateNode";
        public const string BehaviorTreePropertyDesignerNewCreateClick = "BehaviorTreePropertyDesignerNewCreateClick";
        public const string BehaviorTreeMouseInNode = "BehaviorTreeMouseInNode";
        public const string BehaviorTreeConnectState = "BehaviorTreeConnectState";
        public const string BehaviorTreeReplaceClick = "BehaviorTreeReplaceClick";
        public const string BehaviorTreeRightDesignerDrag = "BehaviorTreeRightDesignerDrag";
        public const string SessionRecvMessage = "SessionRecvMessage";
        public const string NumbericChange = "NumbericChange";
        public const string MessageDeserializeFinish = "MessageDeserializeFinish";
        public const string SceneChange = "SceneChange";
        public const string FrameUpdate = "FrameUpdate";
        public const string LoadingBegin = "LoadingBegin";
        public const string LoadingFinish = "LoadingFinish";
        public const string TestHotfixSubscribMonoEvent = "TestHotfixSubscribMonoEvent";
        public const string MaxModelEvent = "MaxModelEvent";
        public const string GetInventoryFromNet = "GetInventoryFromNet";
        public const string GetInventoryFromLocalDisk = "GetInventoryFromLocakDisk";
        public const string InventoryUpdateEvent = "InventoryUpdateEvent";
        public const string AddItemEvent = "AddItemEvent";
        public const string InventoryAddItemEvent = "InventoryAddItemEvent";
        public const string InventoryAskEvent = "InventoryAskEvent";
        public const string InventoryAffirmEvent = "InventoryAffirmEvent";
        public const string InventoryOpen = "InventoryOpen";
        public const string InventoryClose = "InventoryClose";
        public const string DisplayStoreEvent = "DisplayStoreEvent";
        public const string DropInventoryItemEvent = "DropInventoryItemEvent";
        public const string InventorySortEvent = "InventorySortEvent";
        public const string InventoryRemoveItemDataEvent = "InventoryRemoveItemEvent";
        public const string StartTradeEvent = "StartTradeEvent";
        public const string ReceiveTradeEvent = "ReceiveTradeEvent";
        public const string CancelTradeEvent = "CancelTradeEvent";
        public const string PutCommodity = "PutCommodity";
        public const string ConfirmTradeEvent = "ConfirmTradeEvent";
        public const string Buy = "BuyEvent";
        public const string Sales = "SalesEvent";
        public const string Login = "Login";
    }
}