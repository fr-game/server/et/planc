﻿using System;
using System.IO;
using System.Threading;
using Google.Protobuf;
using UnityEngine;

namespace ETModel
{
    public class Test: MonoBehaviour
    {
    

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.A))
            {
                Game.Scene.GetComponent<InventoryCmp>().Open();
            }

            if (Input.GetKeyDown(KeyCode.S))
            {
                Game.Scene.GetComponent<InventoryCmp>().Close();
            }
            
            if (Input.GetKeyDown(KeyCode.D))
            {

                Game.Scene.GetComponent<InventoryCmp>().AddInventoryItem(1, 1);
            }
            if (Input.GetKeyDown(KeyCode.F))
            {

                Game.Scene.GetComponent<InventoryCmp>().AddInventoryItem(2, 1);
            }
            if (Input.GetKeyDown(KeyCode.G))
            {

                Game.Scene.GetComponent<InventoryCmp>().AddInventoryItem(3, 1);
            }
            if (Input.GetKeyDown(KeyCode.Z))
            {
                StoreCmp store = Game.Scene.GetComponent<StoreCmp>();
                if (store == null)
                {
                    store = Game.Scene.AddComponent<StoreCmp, long>(1);
                }
                store.DisplayStore();
            }
            if (Input.GetKeyDown(KeyCode.L))
            {
                Game.EventSystem.Run(EventIdType.Login);
            }
        }

        private void OnGUI()
        {
            GUI.Label( new Rect( 10 , 10, 800, 30) ,
                      "A 打开背包; S 关闭背包 ；D 增加1个草鞋 ；F 增加1个内裤；G 增加1个神仙水; Z 打开商店 L 登陆" );
        }
    }
}