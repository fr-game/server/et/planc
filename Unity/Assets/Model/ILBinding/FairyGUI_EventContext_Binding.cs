using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;

using ILRuntime.CLR.TypeSystem;
using ILRuntime.CLR.Method;
using ILRuntime.Runtime.Enviorment;
using ILRuntime.Runtime.Intepreter;
using ILRuntime.Runtime.Stack;
using ILRuntime.Reflection;
using ILRuntime.CLR.Utils;

namespace ILRuntime.Runtime.Generated
{
    unsafe class FairyGUI_EventContext_Binding
    {
        public static void Register(ILRuntime.Runtime.Enviorment.AppDomain app)
        {
            BindingFlags flag = BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static | BindingFlags.DeclaredOnly;
            FieldInfo field;
            Type[] args;
            Type type = typeof(FairyGUI.EventContext);

            field = type.GetField("data", flag);
            app.RegisterCLRFieldGetter(field, get_data_0);
            app.RegisterCLRFieldSetter(field, set_data_0);


        }



        static object get_data_0(ref object o)
        {
            return ((FairyGUI.EventContext)o).data;
        }
        static void set_data_0(ref object o, object v)
        {
            ((FairyGUI.EventContext)o).data = (System.Object)v;
        }


    }
}
