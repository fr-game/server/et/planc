using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;

using ILRuntime.CLR.TypeSystem;
using ILRuntime.CLR.Method;
using ILRuntime.Runtime.Enviorment;
using ILRuntime.Runtime.Intepreter;
using ILRuntime.Runtime.Stack;
using ILRuntime.Reflection;
using ILRuntime.CLR.Utils;

namespace ILRuntime.Runtime.Generated
{
    unsafe class ETModel_InventoryDataCmp_Binding
    {
        public static void Register(ILRuntime.Runtime.Enviorment.AppDomain app)
        {
            BindingFlags flag = BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static | BindingFlags.DeclaredOnly;
            FieldInfo field;
            Type[] args;
            Type type = typeof(ETModel.InventoryDataCmp);

            field = type.GetField("Index", flag);
            app.RegisterCLRFieldGetter(field, get_Index_0);
            app.RegisterCLRFieldSetter(field, set_Index_0);
            field = type.GetField("CountNum", flag);
            app.RegisterCLRFieldGetter(field, get_CountNum_1);
            app.RegisterCLRFieldSetter(field, set_CountNum_1);


        }



        static object get_Index_0(ref object o)
        {
            return ((ETModel.InventoryDataCmp)o).Index;
        }
        static void set_Index_0(ref object o, object v)
        {
            ((ETModel.InventoryDataCmp)o).Index = (System.Int32)v;
        }
        static object get_CountNum_1(ref object o)
        {
            return ((ETModel.InventoryDataCmp)o).CountNum;
        }
        static void set_CountNum_1(ref object o, object v)
        {
            ((ETModel.InventoryDataCmp)o).CountNum = (System.Int32)v;
        }


    }
}
