using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;

using ILRuntime.CLR.TypeSystem;
using ILRuntime.CLR.Method;
using ILRuntime.Runtime.Enviorment;
using ILRuntime.Runtime.Intepreter;
using ILRuntime.Runtime.Stack;
using ILRuntime.Reflection;
using ILRuntime.CLR.Utils;

namespace ILRuntime.Runtime.Generated
{
    unsafe class ETModel_ItemConfig_Binding
    {
        public static void Register(ILRuntime.Runtime.Enviorment.AppDomain app)
        {
            BindingFlags flag = BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static | BindingFlags.DeclaredOnly;
            FieldInfo field;
            Type[] args;
            Type type = typeof(ETModel.ItemConfig);

            field = type.GetField("ItemType", flag);
            app.RegisterCLRFieldGetter(field, get_ItemType_0);
            app.RegisterCLRFieldSetter(field, set_ItemType_0);


        }



        static object get_ItemType_0(ref object o)
        {
            return ((ETModel.ItemConfig)o).ItemType;
        }
        static void set_ItemType_0(ref object o, object v)
        {
            ((ETModel.ItemConfig)o).ItemType = (System.String)v;
        }


    }
}
