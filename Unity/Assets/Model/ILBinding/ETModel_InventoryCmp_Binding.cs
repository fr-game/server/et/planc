using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;

using ILRuntime.CLR.TypeSystem;
using ILRuntime.CLR.Method;
using ILRuntime.Runtime.Enviorment;
using ILRuntime.Runtime.Intepreter;
using ILRuntime.Runtime.Stack;
using ILRuntime.Reflection;
using ILRuntime.CLR.Utils;

namespace ILRuntime.Runtime.Generated
{
    unsafe class ETModel_InventoryCmp_Binding
    {
        public static void Register(ILRuntime.Runtime.Enviorment.AppDomain app)
        {
            BindingFlags flag = BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static | BindingFlags.DeclaredOnly;
            MethodBase method;
            FieldInfo field;
            Type[] args;
            Type type = typeof(ETModel.InventoryCmp);
            args = new Type[]{};
            method = type.GetMethod("SortInventory", flag, null, args, null);
            app.RegisterCLRMethodRedirection(method, SortInventory_0);
            args = new Type[]{typeof(System.Int32)};
            method = type.GetMethod("GetItemByIndex", flag, null, args, null);
            app.RegisterCLRMethodRedirection(method, GetItemByIndex_1);
            args = new Type[]{typeof(ETModel.Item), typeof(System.Int32)};
            method = type.GetMethod("DropInventoryItem", flag, null, args, null);
            app.RegisterCLRMethodRedirection(method, DropInventoryItem_2);

            field = type.GetField("Gold", flag);
            app.RegisterCLRFieldGetter(field, get_Gold_0);
            app.RegisterCLRFieldSetter(field, set_Gold_0);
            field = type.GetField("Diamond", flag);
            app.RegisterCLRFieldGetter(field, get_Diamond_1);
            app.RegisterCLRFieldSetter(field, set_Diamond_1);
            field = type.GetField("MaxSize", flag);
            app.RegisterCLRFieldGetter(field, get_MaxSize_2);
            app.RegisterCLRFieldSetter(field, set_MaxSize_2);
            field = type.GetField("InventoryStatus", flag);
            app.RegisterCLRFieldGetter(field, get_InventoryStatus_3);
            app.RegisterCLRFieldSetter(field, set_InventoryStatus_3);
            field = type.GetField("Items", flag);
            app.RegisterCLRFieldGetter(field, get_Items_4);
            app.RegisterCLRFieldSetter(field, set_Items_4);


        }


        static StackObject* SortInventory_0(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            StackObject* ptr_of_this_method;
            StackObject* __ret = ILIntepreter.Minus(__esp, 1);

            ptr_of_this_method = ILIntepreter.Minus(__esp, 1);
            ETModel.InventoryCmp instance_of_this_method = (ETModel.InventoryCmp)typeof(ETModel.InventoryCmp).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack));
            __intp.Free(ptr_of_this_method);

            var result_of_this_method = instance_of_this_method.SortInventory();

            __ret->ObjectType = ObjectTypes.Integer;
            __ret->Value = result_of_this_method ? 1 : 0;
            return __ret + 1;
        }

        static StackObject* GetItemByIndex_1(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            StackObject* ptr_of_this_method;
            StackObject* __ret = ILIntepreter.Minus(__esp, 2);

            ptr_of_this_method = ILIntepreter.Minus(__esp, 1);
            System.Int32 @index = ptr_of_this_method->Value;

            ptr_of_this_method = ILIntepreter.Minus(__esp, 2);
            ETModel.InventoryCmp instance_of_this_method = (ETModel.InventoryCmp)typeof(ETModel.InventoryCmp).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack));
            __intp.Free(ptr_of_this_method);

            var result_of_this_method = instance_of_this_method.GetItemByIndex(@index);

            object obj_result_of_this_method = result_of_this_method;
            if(obj_result_of_this_method is CrossBindingAdaptorType)
            {    
                return ILIntepreter.PushObject(__ret, __mStack, ((CrossBindingAdaptorType)obj_result_of_this_method).ILInstance);
            }
            return ILIntepreter.PushObject(__ret, __mStack, result_of_this_method);
        }

        static StackObject* DropInventoryItem_2(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            StackObject* ptr_of_this_method;
            StackObject* __ret = ILIntepreter.Minus(__esp, 3);

            ptr_of_this_method = ILIntepreter.Minus(__esp, 1);
            System.Int32 @num = ptr_of_this_method->Value;

            ptr_of_this_method = ILIntepreter.Minus(__esp, 2);
            ETModel.Item @item = (ETModel.Item)typeof(ETModel.Item).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack));
            __intp.Free(ptr_of_this_method);

            ptr_of_this_method = ILIntepreter.Minus(__esp, 3);
            ETModel.InventoryCmp instance_of_this_method = (ETModel.InventoryCmp)typeof(ETModel.InventoryCmp).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack));
            __intp.Free(ptr_of_this_method);

            var result_of_this_method = instance_of_this_method.DropInventoryItem(@item, @num);

            object obj_result_of_this_method = result_of_this_method;
            if(obj_result_of_this_method is CrossBindingAdaptorType)
            {    
                return ILIntepreter.PushObject(__ret, __mStack, ((CrossBindingAdaptorType)obj_result_of_this_method).ILInstance);
            }
            return ILIntepreter.PushObject(__ret, __mStack, result_of_this_method);
        }


        static object get_Gold_0(ref object o)
        {
            return ((ETModel.InventoryCmp)o).Gold;
        }
        static void set_Gold_0(ref object o, object v)
        {
            ((ETModel.InventoryCmp)o).Gold = (System.Int32)v;
        }
        static object get_Diamond_1(ref object o)
        {
            return ((ETModel.InventoryCmp)o).Diamond;
        }
        static void set_Diamond_1(ref object o, object v)
        {
            ((ETModel.InventoryCmp)o).Diamond = (System.Int32)v;
        }
        static object get_MaxSize_2(ref object o)
        {
            return ((ETModel.InventoryCmp)o).MaxSize;
        }
        static void set_MaxSize_2(ref object o, object v)
        {
            ((ETModel.InventoryCmp)o).MaxSize = (System.Int32)v;
        }
        static object get_InventoryStatus_3(ref object o)
        {
            return ((ETModel.InventoryCmp)o).InventoryStatus;
        }
        static void set_InventoryStatus_3(ref object o, object v)
        {
            ((ETModel.InventoryCmp)o).InventoryStatus = (ETModel.InventoryStatus)v;
        }
        static object get_Items_4(ref object o)
        {
            return ((ETModel.InventoryCmp)o).Items;
        }
        static void set_Items_4(ref object o, object v)
        {
            ((ETModel.InventoryCmp)o).Items = (System.Collections.Generic.SortedDictionary<System.Int32, ETModel.Item>)v;
        }


    }
}
