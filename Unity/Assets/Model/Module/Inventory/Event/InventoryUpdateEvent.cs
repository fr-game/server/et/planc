/**
 * 本地背包更新事件
 */

using Boo.Lang;
using UnityEngine;

namespace ETModel
{
    [Event(ETModel.EventIdType.InventoryUpdateEvent)]
    public class InventoryUpdateEvent: AEvent
    {
        public override void Run()
        {
            Log.Debug("持久化到本地");

            InventoryCmp inventoryCmp = Game.Scene.GetComponent<InventoryCmp>();
            InventoryDistData data = new InventoryDistData();
            data.InventoryState = inventoryCmp.InventoryStatus.ToString();
            data.Gold = inventoryCmp.Gold;
            data.Diamond = inventoryCmp.Diamond;
            data.MaxSize = inventoryCmp.MaxSize;
            List<ItemInfo> infos = new List<ItemInfo>();
            for (int i = 0; i < inventoryCmp.MaxSize; i++)
            {
                Item inventoryCmpItem = inventoryCmp.Items[i];
                if (inventoryCmpItem != null)
                {
                    ItemInfo tempInfo = new ItemInfo();
                    tempInfo.ItemId = inventoryCmpItem.Id;
                    tempInfo.CreateTime = inventoryCmpItem.CreateTime;
                    tempInfo.ItemConfigId = inventoryCmpItem.ItemConfigId;
                    List<ComponentData> componentDatas = new List<ComponentData>();
                    foreach (var component in inventoryCmpItem.GetComponents())
                    {
                        
                        ComponentData tmpCmpData = new ComponentData();
                        tmpCmpData.TypeName = component.GetType().ToString();
                        Entity componentEntity = component.Entity;
                        Component componentParent = component.Parent;
                        component.Entity = null;
                        component.Parent = null;
                        tmpCmpData.CmpDataBytes = JsonHelper.ToJson(component).ToUtf8();
                        component.Entity = componentEntity;
                        component.Parent = componentParent;
                        componentDatas.Add(tmpCmpData);
                    }

                    tempInfo.ComponentDatas = componentDatas.ToArray();
                    infos.Add(tempInfo);
                }
            }
            data.ItemInfos = infos.ToArray();
            string json = JsonHelper.ToJson(data);
            Log.Debug("持久化的文本:" + json);
            PlayerPrefs.SetString($"InventoryData_{52222232221}", json);
        }
    } //class_end
}