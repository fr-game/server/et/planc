/**
 * 从本地加载/存储背包
 */

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using NPOI.Util;
using UnityEngine;

namespace ETModel
{
    [Event(EventIdType.GetInventoryFromLocalDisk)]
    public class GetInventoryFromLocalDisk: AEvent
    {
        public override async void Run()
        {
            Log.Debug("从本地加载背包配置");
            bool loadRes = await this.LoadInventory();
            if (!loadRes)
            {
                Log.Debug("生成新的背包");
                await GenNewInventory();
                Item item = null;
                Game.EventSystem.Run(EventIdType.InventoryUpdateEvent);
            }
        }

        private Task<bool> LoadInventory()
        {
            //获取背包持久化文件
            string dataString = PlayerPrefs.GetString($"InventoryData_{52222232221}");
            //装成对象
            InventoryDistData data = JsonHelper.FromJson<InventoryDistData>(dataString);
            if (data == null)
            {
                return Task.FromResult(false);
            }
            //已经创建好的背包组件。
            InventoryCmp inventoryCmp = ETModel.Game.Scene.GetComponent<InventoryCmp>();
            //背包基础属性
            inventoryCmp.Gold = data.Gold;
            inventoryCmp.Diamond = data.Diamond;
            inventoryCmp.MaxSize = data.MaxSize;
            inventoryCmp.InventoryStatus = EnumHelper.FromString<InventoryStatus>(data.InventoryState);
            inventoryCmp.Items = new SortedDictionary<int, Item>();
            //背包物品 1.加载物品
            List<Item> itemList = new List<Item>();
            foreach (ItemInfo info in data.ItemInfos)
            {
                Item item = ItemFactory.CreateItemFromRecord(info.ItemConfigId);
                item.Id = info.ItemId;
                item.CreateTime = info.CreateTime;
                //加载Component
                for (int i = 0; i < info.ComponentDatas.Length; i++)
                {
                    ComponentData infoComponentData = info.ComponentDatas[i];
                    string cmpDataJson = infoComponentData.CmpDataBytes.ToStr();
                    Component fromJson = (Component) JsonHelper.FromJson(Type.GetType(infoComponentData.TypeName), cmpDataJson);
                    fromJson.EndDeSerialize();
                    item.AddComponent(fromJson);
                    itemList.Add(item);
                }
            }

            //背包物品 2.放入背包
            for (int i = 0; i < data.MaxSize; i++)
            {
                inventoryCmp.Items[i] = null;
                foreach (Item item in itemList)
                {
                    if (item.GetComponent<InventoryDataCmp>()?.Index == i)
                    {
                        inventoryCmp.Items[i] = item;
                    }
                }

                if (inventoryCmp.Items[i] != null)
                {
                    itemList.Remove(inventoryCmp.Items[i]);
                }
            }
            Log.Debug("获取到了背包");

            return Task.FromResult(true);
        }

        /// <summary>
        /// 生成新的背包
        /// </summary>
        /// <returns></returns>
        private Task GenNewInventory()
        {
            Log.Debug("生成新的背包GenNewInventory");
            //已经创建好的背包组件。
            InventoryCmp inventoryCmp = Game.Scene.GetComponent<InventoryCmp>();
            //背包基础属性
            inventoryCmp.Gold = 100;
            inventoryCmp.Diamond = 0;
            inventoryCmp.InventoryStatus = InventoryStatus.Normal;
            inventoryCmp.Items = new SortedDictionary<int, Item>();
            //创建单元格
            for (int i = 0; i < inventoryCmp.MaxSize; i++)
            {
                inventoryCmp.Items.Add(i,null);
            }
            //背包物品 1.根据活动生成新的物品(登陆送草鞋活动。)
            return inventoryCmp.AddInventoryItem(1, 1);
        }
    } //class_end

}