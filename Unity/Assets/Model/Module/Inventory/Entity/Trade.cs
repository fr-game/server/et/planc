/**
 * 交易实体
 */

using System.Collections.Generic;

namespace ETModel
{
    public class Trade: Entity
    {
        /// <summary>
        /// 发起人
        /// </summary>
        public Unit InitTrader;

        /// <summary>
        /// 交易参与者
        /// </summary>
        public Unit AnotherTrader;

        /// <summary>
        /// 交易状态 我
        /// </summary>
        public TradeState MyTradeState;
        
        /// <summary>
        /// 交易状态 对方
        /// </summary>
        public TradeState AnotherTradeState;
        
        /// <summary>
        /// 记录我商品中的可以交易的物品
        /// </summary>
        public List<Item> MyItemList;

        /// <summary>
        /// 可用金币
        /// </summary>
        public int Gold;

        /// <summary>
        /// 我的交易金额
        /// </summary>
        public int MyTradeGold;

        /// <summary>
        /// 我的交易物品
        /// </summary>
        public List<Item> MyTradeItemList;

        /// <summary>
        ///对方的交易金额
        /// </summary>
        public int AnotherTradeGold;
        /// <summary>
        /// 对方的交易物品
        /// </summary>
        public List<Item> AnotherTradeItemList;
        
         /// <summary>
        /// 放置我的交易物品
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool AddMyTradeItemList(Item item, int num)
        {
            if (!this.MyTradeState.Equals(TradeState.Trading))
            {
                return false;
            }

            //是否满
            if (this.MyTradeItemList.Count > 5)
            {
                return false;
            }

            //这个Item是否已经加入过
            Item tradeItem = null;
            foreach (Item myTradeItem in this.MyTradeItemList)
            {
                if (myTradeItem.Id == item.Id)
                {
                    tradeItem = myTradeItem;
                    break;
                }
            }

            if (tradeItem == null)
            {
                tradeItem = item.CloneItem();
            }

            tradeItem.GetComponent<InventoryDataCmp>().AddNum(num);
            item.GetComponent<InventoryDataCmp>().AddNum(-num);
            if (item.GetComponent<InventoryDataCmp>().CountNum <= 0)
            {
                this.MyItemList.Remove(item);
            }
            return true;
        }

        /// <summary>
        /// 移除我的交易物品
        /// </summary>
        /// <param name="item"></param>
        /// <param name="num"></param>
        /// <returns></returns>
        public bool RemoveMyTradeItemList(Item item, int num)
        {
            if (!this.MyTradeState.Equals(TradeState.Trading))
            {
                return false;
            }

            Item bagItem = null;
            foreach (Item myItem in this.MyItemList)
            {
                if (myItem.Id == item.Id)
                {
                    bagItem = myItem;
                    break;
                }
            }

            if (bagItem == null)
            {
                bagItem = item.CloneItem();
                this.MyItemList.Add(bagItem);
            }

            bagItem.GetComponent<InventoryDataCmp>().AddNum(num);
            item.GetComponent<InventoryDataCmp>().AddNum(-num);
            if (item.GetComponent<InventoryDataCmp>().CountNum <= 0)
            {
                this.MyTradeItemList.Remove(item);
            }
            return true;
        }

        /// <summary>
        /// 更新交易金额
        /// </summary>
        /// <param name="gold"></param>
        /// <returns></returns>
        public bool UpdateMyTradeGold(int gold)
        {
            if (!this.MyTradeState.Equals(TradeState.Trading))
            {
                return false;
            }

            if (gold > this.Gold)
            {
                return false;
            }

            this.MyTradeGold += gold;
            this.Gold -= gold;
            return true;
        }

        /// <summary>
        /// 更新对方的交易商品/金额
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool UpdateAnotherTradeCommodity(Commodity anotherCommodity)
        {
            if (!this.MyTradeState.Equals(TradeState.Trading))
            {
                return false;
            }
            //先销毁之前的
            foreach (Item item in this.AnotherTradeItemList)
            {
                item.Dispose();
            }
            this.AnotherTradeItemList.Clear();
            this.AnotherTradeGold = anotherCommodity.Price;
            this.AnotherTradeItemList = anotherCommodity.ItemList;
            return true;
        }

        
        public override void Dispose()
        {
            if (this.IsDisposed)
            {
                return;
            }
            
            this.MyTradeGold = 0;
            foreach (Item item in this.MyTradeItemList)
            {
                //销毁克隆的Item;
                item.Dispose();
            }
            
            this.MyTradeItemList.Clear();
            this.MyTradeItemList = null;
            this.AnotherTradeGold = 0;
            foreach (Item item in this.AnotherTradeItemList)
            {
                //销毁克隆的Item;
                item.Dispose();
            }
            this.AnotherTradeItemList.Clear();
            this.AnotherTradeItemList = null;
            this.InitTrader = null;
            this.AnotherTrader = null;
            this.AnotherTrader = null;
            base.Dispose();
        }
    }
}