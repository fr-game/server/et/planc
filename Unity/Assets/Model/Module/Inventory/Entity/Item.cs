using System;
using System.Collections.Generic;
using MongoDB.Bson.Serialization.Attributes;
using UnityEngine;

/**
 * 物品实体。
 */
namespace ETModel
{
    [ObjectSystem]
    public class ItemAwakeSystem: AwakeSystem<Item>
    {
        public override void Awake(Item self)
        {
            self.Awake();
        }
    }

    public class Item: Entity
    {
        /// <summary>
        /// 物品配置Id/
        /// </summary>
        [BsonElement("IcID")]
        public long ItemConfigId;

        /// <summary>
        /// 物品配置信息
        /// </summary>
        [BsonIgnore]
        private ItemConfig ItemConfig;

        /// <summary>
        /// 最后使用时间
        /// </summary>
        [BsonIgnore]
        private long LastUseTime;

        /// <summary>
        /// 物品的效果
        /// </summary>
        [BsonIgnore]
        public List<Effect> EffectList;

        /// <summary>
        /// 创建时间
        /// </summary>
        [BsonElement("CT")]
        public long CreateTime;

        //初始化物品
        public void Awake()
        {
            this.LastUseTime = 0;
        }

        /// <summary>
        /// 设置ItemConfig
        /// </summary>
        /// <param name="itemConfig"></param>
        public void SetItemConfig(ItemConfig itemConfig)
        {
            this.ItemConfig = itemConfig;
        }

        /// <summary>
        /// 获取物品配置信息
        /// </summary>
        /// <returns></returns>
        public ItemConfig GetItemConfig()
        {
            return this.ItemConfig;
        }

        /// <summary>
        /// 是否可用
        /// </summary>
        /// <param name="target">要给谁用</param>
        /// <returns></returns>
        public bool CanUse(Unit target)
        {
            return false;
        }

        /// <summary>
        /// 获取物品的名称
        /// </summary>
        /// <returns></returns>
        public string GetName()
        {
            return this.ItemConfig.Name;
        }

        /// <summary>
        /// 获取物品的图标
        /// </summary>
        /// <returns></returns>
        public string GetIcon()
        {
            return this.ItemConfig.IconName;
        }

        /// <summary>
        /// 根据装备的特性计算出背包中的特效
        /// </summary>
        /// <returns></returns>
        public string GetEffectIcon()
        {
            return "";
        }

        /// <summary>
        /// 获取物品名称的颜色。
        /// 100以内 白色
        /// 1000以内蓝色
        /// 10000以内 紫色
        /// </summary>
        /// <returns></returns>
        public Color GetNameColor()
        {
            if (EnumHelper.FromString<ItemType>(this.ItemConfig.ItemType) != ItemType.Equipment)
            {
                if (this.ItemConfigId <= 100)
                {
                    return Color.white;
                }

                if (this.ItemConfigId > 100 && this.ItemConfigId <= 1000)
                {
                    return Color.blue;
                }

                return Color.magenta;
            }
            else
            {
                int score = this.GetComponent<EquipmentDataCmp>().GetScore();
                if (score == 0)
                {
                    return Color.white;
                }

                if (score > 5 && score < 10)
                {
                    return Color.blue;
                }

                return Color.magenta;
            }
        }

        /// <summary>
        /// 获取物品的描述信息
        /// </summary>
        /// <returns></returns>
        public string GetDescription()
        {
            return this.ItemConfig.Description;
        }

        /// <summary>
        /// 获取物品类型
        /// </summary>
        /// <returns></returns>
        public ItemType GetItemType()
        {
            return EnumHelper.FromString<ItemType>(this.ItemConfig.ItemType);
        }

        /// <summary>
        /// 使用物品
        /// </summary>
        /// <param name="target">要给谁用</param>
        /// <returns></returns>
        public bool Use(Unit target)
        {
            return false;
        }

        /// <summary>
        /// 开始倒计时
        /// </summary>
        public void CountDown()
        {
        }

        /// <summary>
        /// 取消使用物品
        /// </summary>
        public void CancelUse()
        {
        }

        public void Hide()
        {
        }

        public void Display()
        {
        }

        /// <summary>
        /// 是否可以叠加
        /// </summary>
        /// <returns></returns>
        public bool CanMany()
        {
            return this.ItemConfig.CanMany == "true";
        }

        /// <summary>
        /// 是否可以一次使用或者出售或者丢弃多个
        /// </summary>
        /// <returns></returns>
        public bool GetCanMany()
        {
            InventoryDataCmp inventoryDataCmp = this.GetComponent<InventoryDataCmp>();
            if (inventoryDataCmp == null)
            {
                return false;
            }

            return this.ItemConfig.CanMany == "true" && inventoryDataCmp.CountNum > 1;
        }

        //销毁事件
        public override void Dispose()
        {
            if (this.IsDisposed)
            {
                return;
            }

            Log.Debug("Item销毁开始");
            this.EffectList.Clear();
            this.EffectList = null;
            this.LastUseTime = 0;
            base.Dispose();
        }

        public Item CloneItem()
        {
            Item item = ItemFactory.CreateItemFromRecord(this.ItemConfigId);
            foreach (Component component in this.GetComponents())
            {
                Entity componentEntity = component.Entity;
                Component componentParent = component.Parent;
                component.Entity = null;
                component.Parent = null;
                string cmpDataJson = JsonHelper.ToJson(component);
                component.Entity = componentEntity;
                component.Parent = componentParent;
                var fromJson = (Component) JsonHelper.FromJson(component.GetType(), cmpDataJson);
                fromJson.EndDeSerialize();
                item.AddComponent(fromJson);
            }

            if (item.GetComponent<InventoryDataCmp>() != null)
            {
                item.GetComponent<InventoryDataCmp>().CountNum = 1;
            }

            return item;
        }
    } //class_end
}