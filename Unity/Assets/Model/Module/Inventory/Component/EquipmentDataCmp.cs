/**
 * 挂载了这个组件的物品就是一个可穿戴的装备了。
 */

using System;
using System.Collections.Generic;
using MongoDB.Bson.Serialization.Attributes;

namespace ETModel
{
    [ObjectSystem]
    public class EquipmentDataCmpAwakeSystem: AwakeSystem<EquipmentDataCmp, long>
    {
        public override void Awake(EquipmentDataCmp self, long equipDataConfigId)
        {
            self.Awake(equipDataConfigId);
        }
    }

    public class EquipmentDataCmp: Component, ISerializeToEntity
    {
        /// <summary>
        /// 强化等级
        /// </summary>
        [BsonElement("l")]
        public int ForgeLevel = 1;

        /// <summary>
        /// 装备普通属性集合
        /// </summary>
        [BsonElement("dl")]
        public List<EquipData> EquipDataList;

        /// <summary>
        /// 装备拓展属性集合
        /// </summary>
        /// <returns></returns>
        [BsonElement("del")]
        public List<EquipData> EquipExtDataList;

        /// <summary>
        /// 装备的镶嵌属性
        /// </summary>
        /// <returns></returns>
        [BsonElement("il")]
        public List<InlayData> InlayDataList;

        /// <summary>
        /// 对应的装备属性配置ID
        /// </summary>
        [BsonElement("cid")]
        public long EquipDataConfigId;

        /// <summary>
        /// 对应的装备属性配置
        /// </summary>
        [BsonIgnore]
        public EquipDataConfig EquipDataConfig;

        /// <summary>
        ///  初始化装备属性
        /// </summary>
        /// <param name="equipDataConfigId">装备属性配置ID</param>
        public void Awake(long equipDataConfigId)
        {
            Log.Debug("初始化装备属性。。。");
            //初始化字段
            this.EquipDataList = new List<EquipData>();
            this.EquipExtDataList = new List<EquipData>();
            this.InlayDataList = new List<InlayData>();
            //获取配置
            EquipDataConfig equipDataConfig = Game.Scene.GetComponent<InventoryConfigMrgCmp>().GetEquipDataConfig(equipDataConfigId);
            //记录配置信息
            this.EquipDataConfig = equipDataConfig;
            //记录配置信息ID
            this.EquipDataConfigId = equipDataConfigId;
            //获取数值配置
            long[] equipDataNumericConfigIds = equipDataConfig.EquipDataNumericConfigIds;
            //基础数值赋值
            for (int i = 0; i < equipDataNumericConfigIds.Length; i++)
            {
                long equipDataNumericConfigId = equipDataNumericConfigIds[i];
                EquipDataNumericConfig equipDataNumericConfig =
                        Game.Scene.GetComponent<InventoryConfigMrgCmp>().GetEquipDataNumericConfig(equipDataConfigId);
                EquipData data = ComponentFactory.Create<EquipData>();
                data.EquipDataNumericConfigId = equipDataConfigId;
                data.CurrentValue = this.GenRandomNumerical(equipDataNumericConfig.MinValue, equipDataNumericConfig.MaxValue);
                data.SetEquipDataNumericConfig(equipDataNumericConfig);
                Log.Debug($"基础数值{i}: {JsonHelper.ToJson(data)}");
                this.EquipDataList.Add(data);
            }

            Log.Debug("初始化装备属性完成");
        }

        /// <summary>
        /// 生成装备的随机数值
        /// </summary>
        /// <param name="min">最大值</param>
        /// <param name="max">最小值</param>
        /// <returns></returns>
        public double GenRandomNumerical(double min, double max)
        {
            return RandomHelper.RandomNumber((int) min, (int) max);
        }

        /// <summary>
        /// 升级装备。
        /// </summary>
        /// <returns></returns>
        public bool Upgrade()
        {
            Log.Debug("升级装备");
            UpgradeConfig upgradeConfig = Game.Scene.GetComponent<InventoryConfigMrgCmp>()
                    .GetUpgradeConfig(((Item) this.Entity).GetItemConfig().UpgradeId);
            int gold = upgradeConfig.Gold;
            double probability = upgradeConfig.Probability;
            var upgradeConfigNeedItemList = upgradeConfig.NeedItemList;
            //todo 
            return false;
        }

        /// <summary>
        /// 装备评分。
        /// </summary>
        /// <returns></returns>
        public int GetScore()
        {
            int score = 0;

            foreach (var data in this.EquipDataList)
            {
                if (data.CurrentValue == data.GetMaxValue())
                {
                    score += 5;
                }
            }

            score += 10 * this.EquipExtDataList.Count;

            return score;
        }

        /// <summary>
        /// 镶嵌
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool InlayItem(Item item)
        {
            return false;
        }

        /// <summary>
        /// 追加属性
        /// </summary>
        /// <returns></returns>
        public bool AppendExtData()
        {
            return false;
        }

        /// <summary>
        /// 反序列化后 。读取
        /// </summary>
        public override void EndDeSerialize()
        {
            //加载配置信息
            this.EquipDataConfig = Game.Scene.GetComponent<InventoryConfigMrgCmp>().GetEquipDataConfig(this.EquipDataConfigId);
            foreach (InlayData inlayData in this.InlayDataList)
            {
                inlayData.ItemConfig = Game.Scene.GetComponent<InventoryConfigMrgCmp>().GetItemConfig(inlayData.ItemConfigId);
            }
            foreach (EquipData equipData in this.EquipDataList)
            {
                equipData.SetEquipDataNumericConfig(Game.Scene.GetComponent<InventoryConfigMrgCmp>()
                                                            .GetEquipDataNumericConfig(equipData.EquipDataNumericConfigId));
            }
            foreach (EquipData equipData in this.EquipExtDataList)
            {
                equipData.SetEquipDataNumericConfig(Game.Scene.GetComponent<InventoryConfigMrgCmp>()
                                                            .GetEquipDataNumericConfig(equipData.EquipDataNumericConfigId));
            }
        }
        
        /// <summary>
        /// 获取拓展属性
        /// </summary>
        /// <returns></returns>
        public string GetExtDataDesc()
        {
            string desc = "";
            foreach (var data in this.EquipExtDataList)
            {
                if ("" != desc)
                {
                    desc += "\n";
                }

                desc = $"{data.GetName()}:{data.CurrentValue}";
            }

            return desc;
        }

        /// <summary>
        /// 析构（对象不用之后）
        /// </summary>
        public override void Dispose()
        {
            if (this.IsDisposed)
            {
                return;
            }

            Log.Debug("析构装备组件");
            //基础属性
            foreach (EquipData equipData in this.EquipDataList)
            {
                equipData.Dispose();
            }

            this.EquipDataList.Clear();
            this.EquipDataList = null;

            //配置信息
            this.EquipDataConfig = null;
            this.EquipDataConfigId = 0;

            //拓展属性
            foreach (EquipData equipData in this.EquipExtDataList)
            {
                equipData.Dispose();
            }

            this.EquipExtDataList.Clear();
            this.EquipExtDataList = null;

            //镶嵌
            foreach (InlayData inlayData in this.InlayDataList)
            {
                inlayData.Dispose();
            }

            this.InlayDataList.Clear();
            this.InlayDataList = null;

            base.Dispose();
        }
    } //class_end

    //镶嵌的属性
    public class InlayData: Entity
    {
        /// <summary>
        /// 物品配置Id/
        /// </summary>
        [BsonElement("i")]
        public long ItemConfigId;

        /// <summary>
        /// 物品配置信息
        /// </summary>
        [BsonIgnore]
        public ItemConfig ItemConfig;

        public string GetName()
        {
            return ItemConfig.Name;
        }

        public string GetIconUrl()
        {
            return this.ItemConfig.IconName;
        }

        public string GetDesc()
        {
            return this.ItemConfig.Description;
        }

        public void GetBuff()
        {
        }

        /// <summary>
        /// 析构
        /// </summary>
        public override void Dispose()
        {
            if (this.IsDisposed)
            {
                return;
            }

            Log.Debug("析构装备镶嵌属性");
            this.ItemConfigId = 0;
            this.ItemConfig = null;
            base.Dispose();
        }
    }

    //装备的属性
    public class EquipData: Entity
    {
        /// <summary>
        /// 对应的装备数值配置信息
        /// </summary>
        [BsonElement("i")]
        public long EquipDataNumericConfigId;

        /// <summary>
        /// 当前值。
        /// </summary>
        [BsonElement("c")]
        public double CurrentValue;

        /// <summary>
        /// 对应的装备数值配置
        /// </summary>
        [BsonIgnore]
        private EquipDataNumericConfig EquipDataNumericConfig;

        public void SetEquipDataNumericConfig(EquipDataNumericConfig equipDataNumericConfig)
        {
            this.EquipDataNumericConfig = equipDataNumericConfig;
        }

        public string GetName()
        {
            return EquipDataNumericConfig.Name;
        }

        public string GetDesc()
        {
            return EquipDataNumericConfig.Desc;
        }

        public double GetMaxValue()
        {
            return EquipDataNumericConfig.MaxValue;
        }

        public double GetMinValue()
        {
            return EquipDataNumericConfig.MinValue;
        }

        /// <summary>
        /// 析构
        /// </summary>
        public override void Dispose()
        {
            if (this.IsDisposed)
            {
                return;
            }

            Log.Debug("析构装备属性");
            this.EquipDataNumericConfigId = 0;
            this.CurrentValue = 0;
            this.EquipDataNumericConfig = null;
            base.Dispose();
        }
    } //class_end
}