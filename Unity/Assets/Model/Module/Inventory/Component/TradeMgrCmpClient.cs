/**
 * 交易管理组件。客户端挂上可以发起交易，服务端挂上可以管理交易 。
 * 
 */

using System.Collections.Generic;
using System.Threading.Tasks;

namespace ETModel
{
    public class TradeMgrCmpClient: Component
    {
        /// <summary>
        /// 客户端管理当前交易
        /// </summary>
        public Trade CurrentTrade;

        /// <summary>
        /// 当前的交易ID;
        /// </summary>
        public long TradeId;

        public void Awake()
        {
            //  CurrentTrades = new Dictionary<long, Trade>();
            // await CheckTrade();
        }

        public Task CheckTrade()
        {
            long currentId = this.InstanceId;
            while (currentId == this.InstanceId)
            {
                //todo
            }

            return Task.CompletedTask;
        }

        /// <summary>
        /// 初始化交易 /创建一笔交易
        /// </summary>
        /// <param name="unitId"></param>
        /// <returns></returns>
        public async Task<bool> InitTrade(long unitId)
        {
            //检查是否进行着交易
            if (this.CurrentTrade != null)
            {
                return false;
            }

            //检查背包状态
            var isLocking = await Game.Scene.GetComponent<InventoryCmp>().IsLocking();
            if (isLocking)
            {
                return false;
            }

            //获取交易对方
            Unit anotherUnit = Game.Scene.GetComponent<UnitComponent>().Get(unitId);
            if (anotherUnit == null)
            {
                return false;
            }

            //todo  一些其他的判断。比如Unit的状态。
            //锁定包
            Game.Scene.GetComponent<InventoryCmp>().Lock();
            //创建Trade
            this.CurrentTrade = ComponentFactory.Create<Trade>();
            this.TradeId = this.CurrentTrade.Id;
            this.CurrentTrade.MyTradeState = TradeState.Init;
            this.CurrentTrade.AnotherTradeState = TradeState.Init;
            this.CurrentTrade.InitTrader = Game.Scene.GetComponent<UnitComponent>().MyUnit;
            this.CurrentTrade.AnotherTrader = anotherUnit;
            //存储可用的Item
            this.CurrentTrade.MyItemList = new List<Item>();

            foreach (Item item in Game.Scene.GetComponent<InventoryCmp>().Items.Values)
            {
                if (item != null)
                {
                    this.CurrentTrade.MyItemList.Add(item);
                }
            }

            //存储可用金币
            this.CurrentTrade.Gold = Game.Scene.GetComponent<InventoryCmp>().Gold;
            //初始化交易物品
            this.CurrentTrade.MyTradeGold = 0;
            this.CurrentTrade.MyTradeItemList = new List<Item>(5);
            this.CurrentTrade.AnotherTradeGold = 0;
            this.CurrentTrade.AnotherTradeItemList = new List<Item>(5);
            return true;
        }

        /// <summary>
        /// 销毁交易。交易终止/完成
        /// </summary>
        public void DestroyTrade()
        {
            this.CurrentTrade?.Dispose();
            Game.Scene.GetComponent<InventoryCmp>().UnLock();
        }

        /// <summary>
        /// 开启一笔交易
        /// </summary>
        /// <returns></returns>
        public async Task<bool> StartTrade(long unitId)
        {
            if (await this.InitTrade(unitId))
            {
                Game.EventSystem.Run(EventIdType.StartTradeEvent, unitId);
                return true;
            }

            return false;
        }

        /// <summary>
        /// 接受到一笔交易的请求
        /// </summary>
        /// <returns></returns>
        public async Task<bool> ReceiveTrade(long unitId)
        {
            if (await this.InitTrade(unitId))
            {
                Game.EventSystem.Run(EventIdType.ReceiveTradeEvent, unitId);
                return true;
            }

            return true;
        }

        /// <summary>
        /// 放置我的交易物品
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool AddMyTradeItemList(Item item, int num)
        {
            bool addMyTradeItemList = this.CurrentTrade.AddMyTradeItemList(item, num);
            Game.EventSystem.Run(EventIdType.PutCommodity);
            return addMyTradeItemList;
        }

        /// <summary>
        /// 移除我的交易物品
        /// </summary>
        /// <param name="item"></param>
        /// <param name="num"></param>
        /// <returns></returns>
        public bool RemoveMyTradeItemList(Item item, int num)
        {
            bool removeMyTradeItemList = this.CurrentTrade.RemoveMyTradeItemList(item, num);
            Game.EventSystem.Run(EventIdType.PutCommodity);
            return removeMyTradeItemList;
        }

        /// <summary>
        /// 更新交易金额
        /// </summary>
        /// <param name="gold"></param>
        /// <returns></returns>
        public bool UpdateMyTradeGold(int gold)
        {
            bool updateMyTradeGold = this.CurrentTrade.UpdateMyTradeGold(gold);
            Game.EventSystem.Run(EventIdType.PutCommodity);
            return updateMyTradeGold;
        }

        /// <summary>
        /// 更新对方的交易商品/金额
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool UpdateAnotherTradeCommodity(Commodity anotherCommodity)
        {
            return this.CurrentTrade.UpdateAnotherTradeCommodity(anotherCommodity);
        }

        /// <summary>
        /// 确认交易
        /// </summary>
        public void ConfirmTrade()
        {
            this.CurrentTrade.MyTradeState = TradeState.WaitingOtherAffirm;
            Game.EventSystem.Run(EventIdType.ConfirmTradeEvent);
        }

        /// <summary>
        /// 对方确认交易
        /// </summary>
        public void AnotherConfirmTrade()
        {
            this.CurrentTrade.AnotherTradeState = TradeState.WaitingOtherAffirm;
        }

        /// <summary>
        /// 取消交易(UI触发，然后抛给事件，事件中发送给server
        /// </summary>
        /// <exception cref="NotImplementedException"></exception>
        public void CancelTrade()
        {
            Game.EventSystem.Run(EventIdType.CancelTradeEvent);
            this.DestroyTrade();
        }

        public override void Dispose()
        {
            if (this.IsDisposed)
            {
                return;
            }

            this.DestroyTrade();
            base.Dispose();
        }
    } //class_end

    //交易物品封装
    public class Commodity
    {
        /// <summary>
        /// 交换的金额
        /// </summary>
        public int Price;

        /// <summary>
        /// 交换的商品
        /// </summary>
        public List<Item> ItemList;

        /// <summary>
        /// 交易人
        /// </summary>
        public Unit Trader;
    } //class_end
}