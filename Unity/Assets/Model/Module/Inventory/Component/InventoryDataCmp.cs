/**
 * * 背包物品数据组件。挂上这个组件的物品就是背包物品
 * 它和WorldItemDataCmp是互斥的。
 */
namespace ETModel
{
    [ObjectSystem]
    public class InventoryDataCmpAwakeSystem:AwakeSystem<InventoryDataCmp>
    {
        public override void Awake(InventoryDataCmp self)
        {
            self.Entity.RemoveComponent<WorldItemDataCmp>();
        }
    }
    
    public class InventoryDataCmp:Component,ISerializeToEntity
    {
        /// <summary>
        /// 物品索引
        /// </summary>
        public int Index;

        /// <summary>
        /// 物品数量
        /// </summary>
        public int CountNum;

        /// <summary>
        /// 增减数量
        /// </summary>
        /// <param name="num"></param>
        public int AddNum(int num)
        {
            this.CountNum += num;
            return this.CountNum;
        }

        /// <summary>
        /// 增减数量后，物品是否满。 
        /// </summary>
        /// <param name="num"></param>
        /// <returns></returns>
        public bool AfterAddIsFull(int num)
        {
            return this.CountNum + num>=((Item)this.Entity).GetItemConfig().MaxNum;
        }

        /// <summary>
        /// 析构
        /// </summary>
        public override void Dispose()
        {
            if (this.IsDisposed)
            {
                return;
            }

            this.Index = 0;
            this.CountNum = 0;
            base.Dispose();
            
        }
    }
}