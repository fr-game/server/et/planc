/**
 * 挂载了这个组件的Item就是世界物品了。
 * 就是一个在世界上可见的物品。它和InventoryDataCmp是互斥的。
 */

using System;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization.Attributes;
using UnityEngine;

namespace ETModel
{
    [ObjectSystem]
    public class WorldItemDataCmpAwakeSystem: AwakeSystem<WorldItemDataCmp>
    {
        public override void Awake(WorldItemDataCmp self)
        {
            self.Entity.RemoveComponent<InventoryDataCmp>();
        }
    }

    public class WorldItemDataCmp: Component, ISerializeToEntity
    {
        /// <summary>
        /// 坐标
        /// </summary>
        [BsonElement("e")]
        public Vector2 Position;

        /// <summary>
        /// 是否锁定
        /// </summary>
        [BsonIgnore]
        public bool Locking;

        /// <summary>
        /// 对当前Unit的状态
        /// </summary>
        [BsonIgnore]
        public WorldItemForTheUnitStatus WorldItemForTheUnitStatus = WorldItemForTheUnitStatus.Normal;

        /// <summary>
        /// 锁定 
        /// </summary>
        public async Task<bool> Lock()
        {
            if (GameMode.net == GameSettings.GameMode)
            {
                //服务器端获取item状态
                Actor_Item_LockItemResponse response =
                        (Actor_Item_LockItemResponse) await Game.Scene.GetComponent<SessionComponent>().Session
                                .Call(new Actor_Item_LockItemRequest());
                if (response.Error != ErrorCode.ERR_Success)
                {
                    return false;
                }

                return true;
            }

            this.Locking = true;
            return true;
        }

        /// <summary>
        /// 解锁
        /// </summary>
        public async Task<bool> UnLock()
        {
            if (GameMode.net == GameSettings.GameMode)
            {
                //服务器端获取item状态
                Actor_Item_UnLockItemResponse response =
                        (Actor_Item_UnLockItemResponse) await Game.Scene.GetComponent<SessionComponent>().Session
                                .Call(new Actor_Item_UnLockItemRequest());
                if (response.Error != ErrorCode.ERR_Success)
                {
                    return false;
                }

                return true;
            }

            this.Locking = false;
            return true;
        }

        /// <summary>
        /// 是否锁定
        /// </summary>
        /// <returns></returns>
        public async Task<bool> IsLocking()
        {
            if (GameMode.net == GameSettings.GameMode)
            {
                //服务器端获取item状态
                Actor_Item_GetItemLockStatusResponse response =
                        (Actor_Item_GetItemLockStatusResponse) await Game.Scene.GetComponent<SessionComponent>().Session
                                .Call(new Actor_Item_GetItemLockStatusRequest());
                if (response.Error != ErrorCode.ERR_Success)
                {
                    throw new Exception("同步Item状态失败！");
                }

                this.Locking = response.Locking > 0;
            }

            return this.Locking;
        }

        /// <summary>
        /// 析构
        /// </summary>
        public override void Dispose()
        {
            if (this.IsDisposed)
            {
                return;
            }

            this.Position = Vector2.zero;
            this.WorldItemForTheUnitStatus = WorldItemForTheUnitStatus.Normal;
            this.Locking = false;
            base.Dispose();
        }
    }
}