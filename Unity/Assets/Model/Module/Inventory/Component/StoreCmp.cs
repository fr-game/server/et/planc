/**
 *
 * 商店组件，挂上这个组件的实体就有了商店功能。
 * 
 */

using System.Collections.Generic;
using System.Threading.Tasks;

namespace ETModel
{
    [ObjectSystem]
    public class StoreCmpAwakeSystem: AwakeSystem<StoreCmp, long>
    {
        public override void Awake(StoreCmp self, long storeConfigId)
        {
            self.LazyAwake(storeConfigId);
        }
    }

    public class StoreCmp: Component
    {
        /// <summary>
        /// 商店配置信息ID
        /// </summary>
        public long StoreConfigId;

        /// <summary>
        /// 商店配置信息
        /// </summary>
        public StoreConfig StoreConfig;

        /// <summary>
        /// 商店出售的物品
        /// </summary>
        public List<Item> ItemList;

        /// <summary>
        /// 加载/初始化商店信息
        /// </summary>
        /// <param name="storeConfigId"></param>
        public void Awake(long storeConfigId)
        {
            this.StoreConfig = Game.Scene.GetComponent<InventoryConfigMrgCmp>().GetStoreConfig(storeConfigId);
            this.LoadItemList();
        }

        /// <summary>
        /// 懒加载商店信息
        /// </summary>
        /// <param name="storeConfigId"></param>
        public void LazyAwake(long storeConfigId)
        {
            this.StoreConfigId = storeConfigId;
        }

        /// <summary>
        /// 加载商店的商品信息
        /// </summary>
        private void LoadItemList()
        {
            this.ItemList = new List<Item>(this.StoreConfig.ItemConfigList.Length);
            for (int i = 0; i < this.StoreConfig.ItemConfigList.Length; i++)
            {
                var itemConfigId = this.StoreConfig.ItemConfigList[i];
                Item item = ItemFactory.CreateItem(itemConfigId);
                item.AddComponent<InventoryDataCmp>().Index = i;
                this.ItemList.Add(item);
            }
        }

        /// <summary>
        /// 购买商品
        /// </summary>
        /// <param name="product">购买的商品</param>
        /// <param name="num">购买的数量</param>
        /// <returns></returns>
        public async Task<int> Buy(Item product, int num)
        {
            InventoryCmp inventoryCmp = Game.Scene.GetComponent<InventoryCmp>();
            if (await inventoryCmp.IsLocking())
            {
                Log.Debug("背包被锁定无法购买");
                return 0;
            }

            inventoryCmp.Lock();
            int price = product.GetItemConfig().Price * num;
            string moneyType = product.GetItemConfig().MoneyType;
            int myMoney = moneyType == "gold"? inventoryCmp.Gold : inventoryCmp.Diamond;
            if (price > myMoney)
            {
                Log.Debug("余额不足");
                inventoryCmp.UnLock();
                return 1;
            }

            List<int> nums = new List<int>(1);
            nums.Add(num);
            List<Item> items = new List<Item>(1);
            items.Add(product);
            if (!inventoryCmp.CanAddInventoryItem(null, nums, items))
            {
                Log.Debug("背包已满");
                inventoryCmp.UnLock();
                return 2;
            }

            bool addItem = false;
            int surplus = 0;
            List<Item> addItemList = new List<Item>();
            do
            {
                addItem = inventoryCmp.AddItem(product.ItemConfigId, num, null, out surplus, out Item item);
                if (item != null)
                {
                    addItemList.Add(item);
                }
                num = surplus;
            }
            while (addItem && surplus > 0);

            if (moneyType == "gold")
            {
                inventoryCmp.Gold -= price;
            }
            else
            {
                inventoryCmp.Diamond -= price;
            }
 
            inventoryCmp.UnLock();
            Game.EventSystem.Run(EventIdType.Buy, addItemList, price,moneyType);
            return 3;
        }

        /// <summary>
        /// 出售商品
        /// </summary>
        /// <param name="product">出售的商品</param>
        /// <param name="num">出售的数量</param>
        /// <returns></returns>
        public async Task<bool> Sales(Item product, int num)
        {
            Log.Debug($"出售的物品{product.GetName()},数量{num}");
            InventoryCmp inventoryCmp = Game.Scene.GetComponent<InventoryCmp>();
            if (await inventoryCmp.IsLocking())
            {
                return false;
            }

            inventoryCmp.Lock();
            //扣税
            int price = product.GetItemConfig().Price * num;
            string moneyType = product.GetItemConfig().MoneyType;
            double storeRate = this.GetStoreRate();
            price = (int) (price * storeRate);
            inventoryCmp.RemoveItem(product.Id, num);
            inventoryCmp.Gold += price;
            inventoryCmp.UnLock();
            Game.EventSystem.Run(EventIdType.Sales, product.Id, num,price);
            return true;
        }

        /// <summary>
        /// 打开商店
        /// </summary>
        /// <returns></returns>
        public bool DisplayStore()
        {
            if (this.StoreConfig == null)
            {
                this.StoreConfig = Game.Scene.GetComponent<InventoryConfigMrgCmp>().GetStoreConfig(this.StoreConfigId);
                this.LoadItemList();
            }

            Game.EventSystem.Run(EventIdType.DisplayStoreEvent, this);
            return true;
        }

        /// <summary>
        /// 获取商店名称
        /// </summary>
        /// <returns></returns>
        public string GetStoreName()
        {
            return this.StoreConfig.StoreName;
        }

        /// <summary>
        /// 获取商店出售价格扣减比例。
        /// </summary>
        /// <returns></returns>
        public double GetStoreRate()
        {
            return this.StoreConfig.TaxRate;
        }

        /// <summary>
        /// 析构
        /// </summary>
        public override void Dispose()
        {
            if (this.IsDisposed)
            {
                return;
            }

            if (this.ItemList != null)
            {
                foreach (Item item in this.ItemList)
                {
                    item.Dispose();
                }

                this.ItemList.Clear();
                this.ItemList = null;
            }

            this.StoreConfig = null;
            this.StoreConfigId = 0;
            base.Dispose();
        }
    } //class_end
}