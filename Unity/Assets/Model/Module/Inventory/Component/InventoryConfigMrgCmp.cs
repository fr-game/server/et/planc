/**
 * 背包系统配置信息管理模块
 */

using System;

namespace ETModel
{
    public class InventoryConfigMrgCmp: Component
    {
        
        public ItemConfig GetItemConfig(long itemConfigId)
        {
            ItemConfig itemConfig = (ItemConfig)Game.Scene.GetComponent<ConfigComponent>().Get(typeof(ItemConfig), (int)itemConfigId);
            if (itemConfig == null)
            {
                throw new Exception($"未找到Id是{itemConfigId}的{typeof(ItemConfig)}配置信息。");
            }
            return itemConfig;
        }

        public Effect GetEffectConfig(long effectId)
        {
            Effect effect = (Effect)Game.Scene.GetComponent<ConfigComponent>().Get(typeof(Effect), (int)effectId);
            if (effect == null)
            {
                throw new Exception($"未找到Id是{effectId}的{typeof(Effect)}配置信息。");
            }

            return effect;

        }

        public StoreConfig GetStoreConfig(long storeConfigId)
        {
            StoreConfig storeConfig = (StoreConfig)Game.Scene.GetComponent<ConfigComponent>().Get(typeof(StoreConfig), (int)storeConfigId);
            if (storeConfig == null)
            {
                throw new Exception($"未找到Id是{storeConfigId}的{typeof(StoreConfig)}配置信息。");
            }

            return storeConfig;
        }

        public UpgradeConfig GetUpgradeConfig(long upGradeConfigId)
        {
            UpgradeConfig upgradeConfig = (UpgradeConfig)Game.Scene.GetComponent<ConfigComponent>().Get(typeof(UpgradeConfig), (int)upGradeConfigId);
            if (upgradeConfig == null)
            {
                throw new Exception($"未找到Id是{upGradeConfigId}的{typeof(UpgradeConfig)}配置信息。");
            }

            return upgradeConfig;
        }

        public EquipDataConfig GetEquipDataConfig(long equipDataConfigId)
        {
            EquipDataConfig equipDataConfig = (EquipDataConfig)Game.Scene.GetComponent<ConfigComponent>().Get(typeof(EquipDataConfig), (int)equipDataConfigId);
            if (equipDataConfig == null)
            {
                throw new Exception($"未找到Id是{equipDataConfigId}的{typeof(UpgradeConfig)}配置信息。");
            }

            return equipDataConfig;
        }

        public EquipDataNumericConfig GetEquipDataNumericConfig(long equipDataDescConfigId)
        {
            EquipDataNumericConfig equipDataDescConfig = (EquipDataNumericConfig)Game.Scene.GetComponent<ConfigComponent>().Get(typeof(EquipDataNumericConfig), (int)equipDataDescConfigId);
            if (equipDataDescConfig == null)
            {
                throw new Exception($"未找到Id是{equipDataDescConfigId}的{typeof(EquipDataNumericConfig)}配置信息。");
            }

            return equipDataDescConfig;
        }
        
    } //class_end
}