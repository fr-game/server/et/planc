/**
 * 背包状态类
 */

namespace ETModel
{
    public enum InventoryStatus
    {
        /// <summary>
        /// 正常
        /// </summary>
        Normal,

        /// <summary>
        /// 交易中
        /// </summary>
        Trading,

        /// <summary>
        /// 锁定
        /// </summary>
        Locking
    }
}