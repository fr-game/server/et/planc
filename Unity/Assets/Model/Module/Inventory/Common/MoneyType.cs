/**
 * 金币类型
 *
 */

namespace ETModel
{
    public enum MoneyType
    {
        /// <summary>
        /// 金币
        /// </summary>
        Gold,

        /// <summary>
        /// 钻石
        /// </summary>
        Diamond,

        /// <summary>
        /// 绑金
        /// </summary>
        PrivateGold
    }
}