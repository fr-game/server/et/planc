/**
 * 交易状态
 */

namespace ETModel
{
    public enum TradeState
    {    
        /// <summary>
        /// 初始化完成
        /// </summary>
        Init,
        /// <summary>
        /// 正常
        /// </summary>
        Normal,
        /// <summary>
        /// 等待对方同意开始交易
        /// </summary>
        WaitingOtherYnc,
        /// <summary>
        /// 交易中
        /// </summary>
        Trading,
        /// <summary>
        /// 等待对方同意交易。
        /// </summary>
        WaitingOtherAffirm
    
    }
}