/**
 * 物品类型枚举类
 * 物品类型枚举类
 */

namespace ETModel
{
    public enum ItemType
    {
        /// <summary>
        /// 无类型
        /// </summary>
        None,

        /// <summary>
        /// 消耗品
        /// </summary>
        CanUse,

        /// <summary>
        /// 任务物品
        /// </summary>
        TaskItem,

        /// <summary>
        /// 装备
        /// </summary>
        Equipment
    }
}