/**
 * 装备位置
 */

namespace ETModel
{
    public enum EquipPart
    {
        /// <summary>
        /// 头
        /// </summary>
        Head,

        /// <summary>
        /// 身体
        /// </summary>
        Body,

        /// <summary>
        /// 手持
        /// </summary>
        Hand,

        /// <summary>
        /// 腿部
        /// </summary>
        Leg,

        /// <summary>
        /// 脚
        /// </summary>
        Foot,

        /// <summary>
        /// 载具
        /// </summary>
        Carrier
    }
}