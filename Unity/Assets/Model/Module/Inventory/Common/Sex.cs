/**
 * 性别
 */

namespace ETModel
{
    public enum Sex
    {
        /// <summary>
        /// 女娃
        /// </summary>
        Girl,

        /// <summary>
        /// 男娃
        /// </summary>
        Boy,

        /// <summary>
        /// 通杀
        /// </summary>
        All
    }
}