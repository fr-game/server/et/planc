/**
 * 世界物品对Unit的状态
 */
namespace ETModel
{
    public enum WorldItemForTheUnitStatus
    {
        /// <summary>
        /// 正常
        /// </summary>
        Normal,
        /// <summary>
        /// 已使用过
        /// </summary>
        Used
    }
}