/**
 * 本地化语言
 */

namespace ETModel
{
    public static class LocalLanguage
    {
        public const string TooOften = "操作频繁 ，稍后再试";
        public const string Equip = "装备";
        public const string Use = "使用";
        public const string AskDrop = "确定要丢弃吗";
        public const string DropItemFailed = "丢弃物品失败";
        public const string DropItemSuccess = "物品已经丢弃";
        public const string DropNum = "丢弃数量";
        public const string OnlyShopCanSale = "在商店中才能出售物品";
        public const string ForgeLevel = "强化等级";
        public const string TradeStarFail = "交易开启失败";
        public static string CantAddItem = "不能交易该物品";
        public static string CantRemoveItem = "不能移除该物品";
        public const string Trade = "交易";
        public const string Cancel = "撤销";
        public const string Close = "关闭";
        public const string EnterNum = "输入交易数量";
        public const string Buy = "购买";
        public const string InventoryLocking = "背包锁定";
        public const string NotSufficientFunds = "余额不足";
        public const string InventoryFull = "背包已满";
        public const string Nothing = "什么事也没有发生";
        public const string Break = "打断";
        public const string Sale = "出售";

        public static string AskTrade(string anotherName)
        {
            return $"确定要和{anotherName}开始交易吗?";
        }
    }
}