using System;
using ETModel;

/**
 *客户端发起交易请求
 */
namespace ETHotfix
{
    [MessageHandler(AppType.Map)]
    public class C2M_InitTradeRequestHandler : AMRpcHandler<C2M_InitTradeRequest, M2C_InitTradeResponse>
    {
        protected override void Run(Session session, C2M_InitTradeRequest message, Action<M2C_InitTradeResponse> reply)
        {
            M2C_InitTradeResponse response = new M2C_InitTradeResponse();
            try
            {
                //创建消息
                bool initTrade = Game.Scene.GetComponent<TradeMgrCmpServer>().InitTrade(
                    message.TradeId, message.SourcePlayerId, message.AnotherPlayerId);
                if (!initTrade)
                {
                    response.Error = ErrorCode.ERR_MyErrorCode;
                }

                reply(response);
                //马上通知另一个客户端要交易了。
                if (initTrade)
                {
                    Unit unit = Game.Scene.GetComponent<UnitComponent>().Get(message.AnotherPlayerId);
                    MessageHelper.SendMsg(unit,
                        new M2C_NoticeInitTrade {SourcePlayerId = unit.Id, TradeId = message.TradeId});
                }
            }
            catch (Exception e)
            {
                ReplyError(response, e, reply);
            }
        }
    }
}