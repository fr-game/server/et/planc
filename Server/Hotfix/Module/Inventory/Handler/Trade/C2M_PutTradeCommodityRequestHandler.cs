using System;
using ETModel;

/**
 *客户端放置交易商品和金币
 */
namespace ETHotfix
{
    [MessageHandler(AppType.Map)]
    public class
        C2M_PutTradeCommodityRequestHandler : AMRpcHandler<C2M_PutTradeCommodityRequest, M2C_PutTradeCommodityResponse>
    {
        protected override void Run(Session session, C2M_PutTradeCommodityRequest message,
            Action<M2C_PutTradeCommodityResponse> reply)
        {
            M2C_PutTradeCommodityResponse response = new M2C_PutTradeCommodityResponse();
            try
            {
                bool updateTradeItems = Game.Scene.GetComponent<TradeMgrCmpServer>().UpdateTredeItems(message.TradeId,
                    MessageUtil.DeSerializeCommodity(message.Commodity));
                if (!updateTradeItems)
                {
                    response.Error = ErrorCode.ERR_MyErrorCode;
                }

                reply(response);
                //通知另一个哥们
                M2C_NoticePutTradeCommodity notice = new M2C_NoticePutTradeCommodity
                {
                    Commodity = message.Commodity, TradeId = message.TradeId
                };

                Trade trade = Game.Scene.GetComponent<TradeMgrCmpServer>().GetTradeById(message.TradeId);
                Unit unit = null;
                if (trade.InitTrader.Id == message.UnitId)
                {
                    unit = trade.AnotherTrader;
                }
                else if (trade.AnotherTrader.Id == message.UnitId)
                {
                    unit = trade.InitTrader;
                }

                if (unit == null)
                {
                    Log.Error("系统异常，交易混乱");
                    return;
                }

                MessageHelper.SendMsg(unit, notice);
            }
            catch (Exception e)
            {
                ReplyError(response, e, reply);
            }
        }
    }
}