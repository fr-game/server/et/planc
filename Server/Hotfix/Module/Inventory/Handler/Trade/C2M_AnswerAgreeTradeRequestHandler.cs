using System;
using ETModel;

/**
 *客户端回应是否同意开始交易。
 */
namespace ETHotfix
{
    [MessageHandler(AppType.Map)]
    public class
        C2M_AnswerAgreeTradeRequestHandler : AMRpcHandler<C2M_AnswerAgreeTradeRequest, M2C_AnswerAgreeTradeResponse>
    {
        protected override void Run(Session session, C2M_AnswerAgreeTradeRequest message,
            Action<M2C_AnswerAgreeTradeResponse> reply)
        {
            M2C_AnswerAgreeTradeResponse response = new M2C_AnswerAgreeTradeResponse();
            try
            {
                if (!message.Agree)
                {
                    Game.Scene.GetComponent<TradeMgrCmpServer>().CancelTrade(message.TradeId);
                    response.Error = ErrorCode.ERR_MyErrorCode;
                    reply(response);
                    return;
                }

                //创建消息
                bool agreeTrade = Game.Scene.GetComponent<TradeMgrCmpServer>()
                    .SetTradeState(message.TradeId, message.UnitId, TradeState.WaitingOtherYnc);
                if (!agreeTrade)
                {
                    response.Error = ErrorCode.ERR_MyErrorCode;
                    reply(response);
                    return;
                }

                //查看双方是否都同意了
                bool tradeState = Game.Scene.GetComponent<TradeMgrCmpServer>()
                    .GetTradeState(message.TradeId, TradeState.WaitingOtherYnc);
                if (!tradeState)
                {
                    response.Error = ErrorCode.ERR_MyErrorCode;
                    reply(response);
                    return;
                }

                Trade trade = Game.Scene.GetComponent<TradeMgrCmpServer>().GetTradeById(message.TradeId);
                if (trade == null)
                {
                    response.Error = ErrorCode.ERR_MyErrorCode;
                    reply(response);
                    return;
                }

                trade.SetTradeState(trade.InitTrader.Id, TradeState.Trading);
                trade.SetTradeState(trade.AnotherTrader.Id, TradeState.Trading);
                reply(response);
                //通知二位交易开始
                MessageHelper.SendMsg(trade.InitTrader, new M2C_BroadcastTradeStart {TradeId = message.TradeId});
                MessageHelper.SendMsg(trade.AnotherTrader, new M2C_BroadcastTradeStart {TradeId = message.TradeId});
            }
            catch (Exception e)
            {
                ReplyError(response, e, reply);
            }
        }
    }
}