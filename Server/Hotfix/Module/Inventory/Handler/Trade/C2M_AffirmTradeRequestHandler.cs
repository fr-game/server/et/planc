using System;
using ETModel;

/**
 *客户端确认交易响应
 */
namespace ETHotfix
{
    [MessageHandler(AppType.Map)]
    public class C2M_AffirmTradeRequestHandler : AMRpcHandler<C2M_AffirmTradeRequest, M2C_AffirmTradeResponse>
    {
        protected override void Run(Session session, C2M_AffirmTradeRequest message,
            Action<M2C_AffirmTradeResponse> reply)
        {
            M2C_AffirmTradeResponse response = new M2C_AffirmTradeResponse();
            try
            {
                reply(response);
                bool confirmTrade = Game.Scene.GetComponent<TradeMgrCmpServer>()
                    .ConfirmTrade(message.TradeId, message.UnitId);
                Trade trade = Game.Scene.GetComponent<TradeMgrCmpServer>().GetTradeById(message.TradeId);
                //是否全部ok
                if (confirmTrade)
                {
                    M2C_NoticeTradeResult msg = new M2C_NoticeTradeResult {TradeId = message.TradeId};
                    MessageHelper.SendMsg(trade.AnotherTrader, msg);
                    MessageHelper.SendMsg(trade.InitTrader, msg);
                    return;
                }

                //通知另一个哥们
                M2C_NoticeAffirmTrade notice = new M2C_NoticeAffirmTrade {TradeId = message.TradeId};

                Unit unit = null;
                if (trade.InitTrader.Id == message.UnitId)
                {
                    unit = trade.AnotherTrader;
                }
                else if (trade.AnotherTrader.Id == message.UnitId)
                {
                    unit = trade.InitTrader;
                }

                if (unit == null)
                {
                    Log.Error("系统异常，交易混乱");
                    return;
                }

                MessageHelper.SendMsg(unit, notice);
            }
            catch (Exception e)
            {
                ReplyError(response, e, reply);
            }
        }
    }
}