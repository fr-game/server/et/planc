using System;
using ETModel;

/**
 *客户端取消交易请求
 */
namespace ETHotfix
{
    [MessageHandler(AppType.Map)]
    public class C2M_CancelTradeRequestHandler : AMRpcHandler<C2M_CancelTradeRequest, M2C_CancelTradeResponse>
    {
        protected override void Run(Session session, C2M_CancelTradeRequest message,
            Action<M2C_CancelTradeResponse> reply)
        {
            M2C_CancelTradeResponse response = new M2C_CancelTradeResponse();
            try
            {
                reply(response);
                Game.Scene.GetComponent<TradeMgrCmpServer>().CancelTrade(message.TradeId);
            }
            catch (Exception e)
            {
                ReplyError(response, e, reply);
            }
        }
    }
}