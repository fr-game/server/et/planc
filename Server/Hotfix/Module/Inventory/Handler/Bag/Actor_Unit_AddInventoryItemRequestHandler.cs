using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ETModel;

/**
 * unit给背包添加物品
 */
namespace ETHotfix
{
    [ActorMessageHandler(AppType.Map)]
    public class Actor_Unit_AddInventoryItemRequestHandler : AMActorRpcHandler<Unit, Actor_Unit_AddInventoryItemRequest,
        Actor_Unit_AddInventoryItemResponse>
    {
        protected override Task Run(Unit unit, Actor_Unit_AddInventoryItemRequest message,
            Action<Actor_Unit_AddInventoryItemResponse> reply)
        {
            Actor_Unit_AddInventoryItemResponse response = new Actor_Unit_AddInventoryItemResponse();
            try
            {
                List<Item> items = new List<Item>();
                for (int i = 0; i < message.Item.Count; i++)
                {
                    items.Add(MessageUtil.DeSerializeItemInfo(message.Item[i]));
                }
                bool addInventoryItem = unit.GetComponent<InventoryCmp>().AddInventoryItemServer(items);
                if (!addInventoryItem)
                {
                    Log.Error($"{unit.Id} 添加物品失败。");
                    response.Error = ErrorCode.ERR_MyErrorCode;
                }

                reply(response);
            }
            catch (Exception e)
            {
                ReplyError(response, e, reply);
            }
            return Task.CompletedTask;
        }
    }
}