using System;
using System.Threading.Tasks;
using ETModel;

/**
 *unit获取背包状态请求
 */
namespace ETHotfix
{
    [ActorMessageHandler(AppType.Map)]
    public class Actor_Unit_GetInventoryStatusRequesttHandler : AMActorRpcHandler<Unit,
        Actor_Unit_GetInventoryStatusRequest, Actor_Unit_GetInventoryStatusResponse>
    {
        protected override Task Run(Unit unit, Actor_Unit_GetInventoryStatusRequest message,
            Action<Actor_Unit_GetInventoryStatusResponse> reply)
        {
            Actor_Unit_GetInventoryStatusResponse response = new Actor_Unit_GetInventoryStatusResponse();
            try
            {
                response.Status = unit.GetComponent<InventoryCmp>().InventoryStatus.ToString();

                reply(response);
            }
            catch (Exception e)
            {
                ReplyError(response, e, reply);
            }

            return Task.CompletedTask;
        }
    }
}