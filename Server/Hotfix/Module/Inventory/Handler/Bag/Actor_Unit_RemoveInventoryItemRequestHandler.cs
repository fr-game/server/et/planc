using System;
using System.Threading.Tasks;
using ETModel;

/**
 * unit给背包移除物品请求
 */
namespace ETHotfix
{
    [ActorMessageHandler(AppType.Map)]
    public class Actor_Unit_RemoveInventoryItemRequestHandler : AMActorRpcHandler<Unit,
        Actor_Unit_RemoveInventoryItemRequest, Actor_Unit_RemoveInventoryItemResponse>
    {
        protected override Task Run(Unit unit, Actor_Unit_RemoveInventoryItemRequest message,
            Action<Actor_Unit_RemoveInventoryItemResponse> reply)
        {
            Actor_Unit_RemoveInventoryItemResponse response = new Actor_Unit_RemoveInventoryItemResponse();
            try
            {
                bool result = unit.GetComponent<InventoryCmp>()
                    .DropInventoryItem(message.ItemId, message.ItemCount);
                if (!result)
                {
                    Log.Error($"{unit.Id} 移除物品失败。");
                    response.Error = ErrorCode.ERR_MyErrorCode;
                }

                reply(response);
            }
            catch (Exception e)
            {
                ReplyError(response, e, reply);
            }

            return Task.CompletedTask;
        }
    }
}