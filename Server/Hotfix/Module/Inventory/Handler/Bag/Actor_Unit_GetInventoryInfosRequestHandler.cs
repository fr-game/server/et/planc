using System;
using System.Threading.Tasks;
using ETModel;

/**
 *  unit获取背包请求
 */
namespace ETHotfix
{
    [ActorMessageHandler(AppType.Map)]
    public class Actor_Unit_GetInventoryInfosRequestHandler : AMActorRpcHandler<Unit,
        Actor_Unit_GetInventoryInfosRequest, Actor_Unit_GetInventoryInfosResponse>
    {
        protected override Task Run(Unit unit, Actor_Unit_GetInventoryInfosRequest message,
            Action<Actor_Unit_GetInventoryInfosResponse> reply)
        {
            Log.Info($"{unit.Id}请求自己的背包信息");
            Actor_Unit_GetInventoryInfosResponse response;
            try
            {
                InventoryCmp inventoryCmp = unit.GetComponent<InventoryCmp>();
                response = MessageUtil.SerializeInventory(inventoryCmp);
                reply(response);
            }
            catch (Exception e)
            {
                Log.Info($"{unit.Id}请求自己的背包信息失败！");
                response = new Actor_Unit_GetInventoryInfosResponse();
                Log.Error(e.Message);
                response.Error = ErrorCode.ERR_Exception;
                ReplyError(response, e, reply);
            }

            return Task.CompletedTask;
        }
    }
}