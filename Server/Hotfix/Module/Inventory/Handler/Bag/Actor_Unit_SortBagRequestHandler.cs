using ETModel;

/**
 * unit给背包整理请求
 */
namespace ETHotfix
{
    [ActorMessageHandler(AppType.Map)]
    public class Actor_Unit_SortBagRequestHandler : AMActorHandler<Unit, Actor_Unit_SortBagRequest>
    {
        protected override void Run(Unit unit, Actor_Unit_SortBagRequest message)
        {
            Log.Info($"{unit.Id}背包整理。");
            unit.GetComponent<InventoryCmp>().SortInventory();
        }
    }
}