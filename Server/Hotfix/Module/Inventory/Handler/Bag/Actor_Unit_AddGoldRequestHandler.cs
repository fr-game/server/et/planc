using System;
using System.Threading.Tasks;
using ETModel;

/**
 *unit给背包更改金币请求
 */
namespace ETHotfix
{
    [ActorMessageHandler(AppType.Map)]
    public class  Actor_Unit_AddGoldRequestHandler : AMActorRpcHandler<Unit, Actor_Unit_AddGoldRequest, Actor_Unit_AddGoldResponse>
    {
        protected override Task Run(Unit unit, Actor_Unit_AddGoldRequest message,
            Action<Actor_Unit_AddGoldResponse> reply)
        {
            Actor_Unit_AddGoldResponse response = new Actor_Unit_AddGoldResponse();
            try
            {
                bool addInventoryGold = unit.GetComponent<InventoryCmp>().AddInventoryGold(message.GoldNum);
                if (!addInventoryGold)
                {
                    response.Error = ErrorCode.ERR_MyErrorCode;
                }

                reply(response);
            }
            catch (Exception e)
            {
                ReplyError(response, e, reply);
            }

            return Task.CompletedTask;
        }
    }
}