using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ETModel;

/**
 *商店出售物品
 */
namespace ETHotfix
{
    [ActorMessageHandler(AppType.Map)]
    public class Actor_Unit_SalesItemRequestHandler : AMActorHandler<Unit, Actor_Unit_SalesItemRequest>
    {
        protected override void  Run(Unit unit, Actor_Unit_SalesItemRequest message )
        {
            try
            {
                bool removeItem = unit.GetComponent<InventoryCmp>().RemoveItem(message.ItemId,message.Num);
                if (!removeItem)
                {
                    Log.Error("删除物品失败");
                }
                else
                {
                    unit.GetComponent<InventoryCmp>().Gold += message.Price;
                }
                
                //保存背包
                Game.EventSystem.Run(EventIdType.InventoryUpdate);
            }
            catch (Exception e)
            {
                Log.Error(e.Message);
            }

        }
    }
}