using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ETModel;

/**
 *商店购买物品
 */
namespace ETHotfix
{
    [ActorMessageHandler(AppType.Map)]
    public class Actor_Unit_BuyItemRequestHandler : AMActorHandler<Unit, Actor_Unit_BuyItemRequest>
    {
        protected override void  Run(Unit unit, Actor_Unit_BuyItemRequest message )
        {
            try
            {
                List<Item> items = new List<Item>();
                foreach (var t in message.Item)
                {
                    items.Add(MessageUtil.DeSerializeItemInfo(t));
                }

                unit.GetComponent<InventoryCmp>().AddInventoryItemServer(items);
                if (message.MoneyType == "gold")
                {
                    unit.GetComponent<InventoryCmp>().Gold -= message.Money;
                }
                else
                {   
                    unit.GetComponent<InventoryCmp>().Diamond -= message.Money;
                }
                //保存背包
                Game.EventSystem.Run(EventIdType.InventoryUpdate);
            }
            catch (Exception e)
            {
                Log.Error(e.Message);
            }

        }
    }
}