/**
 * 仓库持久化
 */

using ETModel;

namespace ETHotfix
{
    [Event(EventIdType.InventoryUpdate)]
    public class InventoryUpdateEvent : AEvent<InventoryCmp>
    {
        public override void Run(InventoryCmp inventory)
        {
            Game.Scene.GetComponent<DBProxyComponent>().Save(inventory);
        }
    }
}