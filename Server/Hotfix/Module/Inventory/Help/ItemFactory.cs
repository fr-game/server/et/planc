/**
 * 物品工厂。
 */

using System.Collections.Generic;
using System.Numerics;
using System.Threading.Tasks;
using ETModel;

namespace ETHotfix
{
    public static class ItemFactory
    {
        /// <summary>
        /// 新建Item
        /// </summary>
        /// <param name="itemConfigId"></param>
        /// <returns></returns>
        public static Item CreateItem(long itemConfigId)
        {
            Log.Debug("创建实体");
            //创建实体
            Item item = ComponentFactory.Create<Item>();
            //记录配置Id
            item.ItemConfigId = itemConfigId;
            //加载配置
            ItemConfig itemConfig = Game.Scene.GetComponent<InventoryConfigMrgCmp>().GetItemConfig(itemConfigId);
            //记录配置信息
            item.SetItemConfig(itemConfig);
            //特效
            item.EffectList = new List<Effect>();
            for (int i = 0; i < itemConfig.EffectTypes.Length; i++)
            {
                if (itemConfig.EffectTypes[i] > 0)
                {
                    Effect effect = Game.Scene.GetComponent<InventoryConfigMrgCmp>().GetEffectConfig(itemConfig.EffectTypes[i]);
                    item.EffectList.Add(effect);
                }
            }

            //装备属性
            if (itemConfig.ItemType == ItemType.Equipment.ToString())
            {
                item.AddComponent<EquipmentDataCmp, long>(itemConfig.EquipDataConfigId);
            }

            //创建时间
            item.CreateTime = TimeHelper.Now();
            return item;
        }

        /// <summary>
        /// 从记录信息中创建Item
        /// </summary>
        /// <param name="itemConfigId"></param>
        /// <returns></returns>
        public static Item CreateItemFromRecord(long itemConfigId)
        {
            //创建实体
            Item item = ComponentFactory.Create<Item>();
            //记录配置Id
            item.ItemConfigId = itemConfigId;
            //加载配置
            ItemConfig itemConfig = Game.Scene.GetComponent<InventoryConfigMrgCmp>().GetItemConfig(itemConfigId);
            //记录配置信息
            item.SetItemConfig(itemConfig);
            return item;
        }

        /// <summary>
        /// 创建世界物品
        /// </summary>
        /// <param name="itemConfigId"></param>
        /// <param name="pos"></param>
        /// <param name="CreateTime"></param>
        /// <returns></returns>
        public static async Task<Item> CreateWorldItem(long itemConfigId, Vector2 pos, long CreateTime = 0)
        {
            Log.Debug("创建实体");
            //创建实体
            Item item = ComponentFactory.Create<Item>();
            //记录配置Id
            item.ItemConfigId = itemConfigId;
            //加载配置
            ItemConfig itemConfig = Game.Scene.GetComponent<InventoryConfigMrgCmp>().GetItemConfig(itemConfigId);
            //记录配置信息
            item.SetItemConfig(itemConfig);
            //特效
            item.EffectList = new List<Effect>();
            for (int i = 0; i < itemConfig.EffectTypes.Length; i++)
            {
                if (itemConfig.EffectTypes[i] > 0)
                {
                    Effect effect = Game.Scene.GetComponent<InventoryConfigMrgCmp>().GetEffectConfig(itemConfig.EffectTypes[i]);
                    item.EffectList.Add(effect);
                }
            }
            //装备属性
            if (itemConfig.ItemType == ItemType.Equipment.ToString())
            {
                item.AddComponent<EquipmentDataCmp, long>(itemConfig.EquipDataConfigId);
            }
            //世界物品的相关属性
            WorldItemDataCmp worldItemDataCmp = item.AddComponent<WorldItemDataCmp>();
            //坐标
            worldItemDataCmp.Position = pos;
            //获取服务端物品对当前unit的状态
            worldItemDataCmp.WorldItemForTheUnitStatus = WorldItemForTheUnitStatus.Normal;
            //创建时间
            item.CreateTime = TimeHelper.Now();
            return item;
        }
    } //class_end
}