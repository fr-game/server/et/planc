/**
 * 本地序列化模型
 */
namespace ETModel
{
     /// <summary>
    /// 背包硬盘数据
    /// </summary>
    public class InventoryDistData
    {
        private int gold_;

        /// <summary>
        ///金币数量
        /// </summary>
        public int Gold
        {
            get
            {
                return gold_;
            }
            set
            {
                gold_ = value;
            }
        }

        private int diamond_;

        /// <summary>
        ///钻石数量
        /// </summary>
        public int Diamond
        {
            get
            {
                return diamond_;
            }
            set
            {
                diamond_ = value;
            }
        }

        private string inventoryState_ = "";

        /// <summary>
        ///背包状态
        /// </summary>
        public string InventoryState
        {
            get
            {
                return inventoryState_;
            }
            set
            {
                inventoryState_ = value;
            }
        }

        private int maxSize_;

        /// <summary>
        ///背包尺寸 （单元格数量）
        /// </summary>
        public int MaxSize
        {
            get
            {
                return maxSize_;
            }
            set
            {
                maxSize_ = value;
            }
        }

        private ItemInfo[] itemInfos_ = null;

        /// <summary>
        ///背包物品
        /// </summary>
        public ItemInfo[] ItemInfos
        {
            get
            {
                return itemInfos_;
            }
            set
            {
                itemInfos_ = value;
            }
        }
    } //class_end

    public class ItemInfo
    {
        private long itemId_;

        /// <summary>
        ///物品ID;
        /// </summary>
        public long ItemId
        {
            get
            {
                return itemId_;
            }
            set
            {
                itemId_ = value;
            }
        }

        private long itemConfigId_;

        /// <summary>
        ///物品配置信息ID
        /// </summary>
        public long ItemConfigId
        {
            get
            {
                return itemConfigId_;
            }
            set
            {
                itemConfigId_ = value;
            }
        }

        private long createTime_;

        /// <summary>
        ///创建时间。
        /// </summary>
        public long CreateTime
        {
            get
            {
                return createTime_;
            }
            set
            {
                createTime_ = value;
            }
        }

        private ComponentData[] componentDatas_ = null;

        /// <summary>
        ///Item上的组件
        /// </summary>
        public ComponentData[] ComponentDatas
        {
            get
            {
                return componentDatas_;
            }
            set
            {
                componentDatas_ = value;
            }
        }
    }

    public class ComponentData
    {
        private string typeName_ = "";

        /// <summary>
        ///组件的类型。
        /// </summary>
        public string TypeName
        {
            get
            {
                return typeName_;
            }
            set
            {
                typeName_ = value;
            }
        }

        private byte[] cmpDataBytes_ ;

        /// <summary>
        /// 编码后的组件
        /// </summary>
        public byte[]  CmpDataBytes
        {
            get
            {
                return cmpDataBytes_;
            }
            set
            {
                cmpDataBytes_ = value;
            }
        }
    }
}