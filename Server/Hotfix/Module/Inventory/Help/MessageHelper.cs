using System;
using System.Collections.Generic;
using ETModel;
using Google.Protobuf.Collections;

namespace ETHotfix
{
    public static class MessageUtil
    {
        /// <summary>
        /// 反序列化背包
        /// </summary>
        /// <param name="res"></param>
        public static void DeSerializeInventory(Actor_Unit_GetInventoryInfosResponse res)
        {
            //已经创建好的背包组件。
            InventoryCmp inventoryCmp = ETModel.Game.Scene.GetComponent<InventoryCmp>();
            //背包基础属性
            inventoryCmp.Gold = res.Gold;
            inventoryCmp.Diamond = res.Diamond;
            inventoryCmp.MaxSize = res.MaxSize;
            inventoryCmp.InventoryStatus = EnumHelper.FromString<InventoryStatus>(res.InventoryState);
            inventoryCmp.Items = new SortedDictionary<int, Item>();
            //背包物品 1.加载物品
            List<Item> itemList = new List<Item>();
            foreach (ItemInfo info in res.ItemInfos)
            {
                Item item = MessageUtil.DeSerializeItemInfo(info);
                itemList.Add(item);
            }

            //背包物品 2.放入背包
            for (int i = 0; i < res.MaxSize; i++)
            {
                inventoryCmp.Items[i] = null;
                foreach (Item item in itemList)
                {
                    if (item.GetComponent<InventoryDataCmp>()?.Index == i)
                    {
                        inventoryCmp.Items[i] = item;
                    }
                }

                if (inventoryCmp.Items[i] != null)
                {
                    itemList.Remove(inventoryCmp.Items[i]);
                }
            }
        }

        /// <summary>
        /// 序列化背包
        /// </summary>
        /// <returns></returns>
        public static Actor_Unit_GetInventoryInfosResponse SerializeInventory(InventoryCmp inventoryCmp)
        {
            //返回的结果
            Actor_Unit_GetInventoryInfosResponse response = new Actor_Unit_GetInventoryInfosResponse
            {
                Gold = inventoryCmp.Gold,
                Diamond = inventoryCmp.Diamond,
                MaxSize = inventoryCmp.MaxSize,
                InventoryState = inventoryCmp.InventoryStatus.ToString(),
                ItemInfos = new RepeatedField<ItemInfo>()
            };
            //背包基础属性
            foreach (var item in inventoryCmp.Items.Values)
            {
                if (item == null)
                {
                    continue;
                }
                response.ItemInfos.Add(SerializeItemInfo(item));
            }
            return response;
        }

       /// <summary>
       /// 序列化交易商品
       /// </summary>
       /// <param name="itemList"></param>
       /// <param name="price"></param>
       /// <param name="tradeId"></param>
       /// <returns></returns>
        public static ETHotfix.Commodity SerializeCommodity(List<Item> itemList, int price,long tradeId)
        {
            ETHotfix.Commodity commodity = new ETHotfix.Commodity();
            commodity.Price = price;
            commodity.TraderId = tradeId;
            commodity.Items = new RepeatedField<ETHotfix.ItemInfo>();
            foreach (Item item in itemList)
            {
                ETHotfix.ItemInfo itemInfo = SerializeItemInfo(item);
                commodity.Items.Add(itemInfo);
            }

            return commodity;
        }

        /// <summary>
        /// 反序列化交易商品
        /// </summary>
        /// <param name="itemList"></param>
        /// <param name="price"></param>
        /// <returns></returns>
        public static ETModel.Commodity DeSerializeCommodity(ETHotfix.Commodity commodity)
        {
            ETModel.Commodity commodityModel = new ETModel.Commodity
            {
                Price = commodity.Price, TraderId = commodity.TraderId, ItemList = new List<Item>()
            };
            foreach (ItemInfo itemInfo in commodity.Items)
            {
                Item item = DeSerializeItemInfo(itemInfo);
                commodityModel.ItemList.Add(item);
            }

            return commodityModel;
        }

        /// <summary>
        /// 序列化交易用信息
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public static ItemInfo SerializeItemInfo(Item item)
        {
            ItemInfo itemInfo = new ItemInfo();
            if(item == null)
            {
                return itemInfo;
            }
            itemInfo.CreateTime = item.CreateTime;
            itemInfo.ItemConfigId = item.ItemConfigId;
            itemInfo.ItemId = item.Id;
            itemInfo.ComponentDatas = new RepeatedField<ComponentData>();
            foreach (ETModel.Component cmp in item.GetComponents())
            {
                itemInfo.ComponentDatas.Add(SerializeComponent(cmp));
            }

            return itemInfo;
        }

        /// <summary>
        /// 反序列化商品信息
        /// </summary>
        /// <param name="itemInfo"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public static Item DeSerializeItemInfo(ItemInfo itemInfo)
        {
            if (itemInfo.ItemId <= 0)
            {
                return null;
            }
            Item item = ItemFactory.CreateItemFromRecord(itemInfo.ItemConfigId);
            item.Id = itemInfo.ItemId;
            item.CreateTime = itemInfo.CreateTime;
            foreach (ComponentData componentData in itemInfo.ComponentDatas)
            {
                item.AddComponent(DeSerializeComponent(componentData));
            }

            return item;
        }

        /// <summary>
        /// 序列化组件
        /// </summary>
        /// <param name="cmp"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public static ComponentData SerializeComponent(ETModel.Component cmp)
        {
            ComponentData tmpCmpData = new ComponentData();
            tmpCmpData.TypeName = cmp.GetType().ToString();
            ETModel.Entity componentEntity = cmp.Entity;
            ETModel.Component componentParent = cmp.Parent;
            cmp.Entity = null;
            cmp.Parent = null;
            tmpCmpData.CmpDataBytes = JsonHelper.ToJson(cmp);
            cmp.Entity = componentEntity;
            cmp.Parent = componentParent;
            return tmpCmpData;
        }

        /// <summary>
        /// 反序列化组件信息
        /// </summary>
        /// <param name="componentData"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public static ETModel.Component DeSerializeComponent(ComponentData componentData)
        {
            var fromJson = (ETModel.Component) ETModel.JsonHelper.FromJson(Type.GetType(componentData.TypeName), componentData.CmpDataBytes);
            fromJson.EndDeSerialize();
            return fromJson;
        }
    }
}