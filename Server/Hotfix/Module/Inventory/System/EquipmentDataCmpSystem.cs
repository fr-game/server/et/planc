/**
 * 挂载了这个组件的物品就是一个可穿戴的装备了。
 */

using System;
using System.Collections.Generic;
using ETModel;
using MongoDB.Bson.Serialization.Attributes;

namespace ETHotfix
{
    [ObjectSystem]
    public class EquipmentDataCmpAwakeSystem : AwakeSystem<EquipmentDataCmp, long>
    {
        public override void Awake(EquipmentDataCmp self, long equipDataConfigId)
        {
            self.Awake(equipDataConfigId);
        }
    }

    [ObjectSystem]
    public class EquipmentDataCmpDestorySystem : DestroySystem<EquipmentDataCmp>
    {
        public override void Destroy(EquipmentDataCmp self)
        {
            self.Dispose();
        }
    }

    public static class EquipmentDataCmpSystem
    {
        /// <summary>
        ///  初始化装备属性
        /// </summary>
        /// <param name="equipDataConfigId">装备属性配置ID</param>
        public static void Awake(this EquipmentDataCmp self, long equipDataConfigId)
        {
            Log.Debug("初始化装备属性。。。");
            //初始化字段
            self.EquipDataList = new List<EquipData>();
            self.EquipExtDataList = new List<EquipData>();
            self.InlayDataList = new List<InlayData>();
            //获取配置
            EquipDataConfig equipDataConfig =
                Game.Scene.GetComponent<InventoryConfigMrgCmp>().GetEquipDataConfig(equipDataConfigId);
            //记录配置信息
            self.EquipDataConfig = equipDataConfig;
            //记录配置信息ID
            self.EquipDataConfigId = equipDataConfigId;
            //获取数值配置
            long[] equipDataNumericConfigIds = equipDataConfig.EquipDataNumericConfigIds;
            //基础数值赋值
            for (int i = 0; i < equipDataNumericConfigIds.Length; i++)
            {
                long equipDataNumericConfigId = equipDataNumericConfigIds[i];
                EquipDataNumericConfig equipDataNumericConfig =
                    Game.Scene.GetComponent<InventoryConfigMrgCmp>().GetEquipDataNumericConfig(equipDataConfigId);
                EquipData data = ComponentFactory.Create<EquipData>();
                data.EquipDataNumericConfigId = equipDataConfigId;
                data.CurrentValue =
                    GenRandomNumerical(equipDataNumericConfig.MinValue, equipDataNumericConfig.MaxValue);
                data.SetEquipDataNumericConfig(equipDataNumericConfig);
                Log.Debug($"基础数值{i}: {JsonHelper.ToJson(data)}");
                self.EquipDataList.Add(data);
            }

            Log.Debug("初始化装备属性完成");
        }


        /// <summary>
        /// 生成装备的随机数值
        /// </summary>
        /// <param name="min">最大值</param>
        /// <param name="max">最小值</param>
        /// <returns></returns>
        public static double GenRandomNumerical(double min, double max)
        {
            return RandomHelper.RandomNumber((int) min, (int) max);
        }

        /// <summary>
        /// 升级装备。
        /// </summary>
        /// <returns></returns>
        public static bool Upgrade(this EquipmentDataCmp self)
        {
            Log.Debug("升级装备");

            //todo 
            return false;
        }

        /// <summary>
        /// 装备评分。
        /// </summary>
        /// <returns></returns>
        public static int GetScore(this EquipmentDataCmp self)
        {
            int score = 0;

            foreach (var data in self.EquipDataList)
            {
                if (data.CurrentValue == data.GetMaxValue())
                {
                    score += 5;
                }
            }


            score += 10 * self.EquipExtDataList.Count;

            return score;
        }

        /// <summary>
        /// 镶嵌
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public static bool InlayItem(this EquipmentDataCmp self, Item item)
        {
            return false;
        }

        /// <summary>
        /// 追加属性
        /// </summary>
        /// <returns></returns>
        public static bool AppendExtData(this EquipmentDataCmp self)
        {
            return false;
        }

        /// <summary>
        /// 析构（对象不用之后）
        /// </summary>
        public static void Dispose(this EquipmentDataCmp self)
        {
            if (self.IsDisposed)
            {
                return;
            }

            Log.Debug("析构装备组件");
            //基础属性
            foreach (EquipData equipData in self.EquipDataList)
            {
                equipData.Dispose();
            }

            self.EquipDataList.Clear();
            self.EquipDataList = null;

            //配置信息
            self.EquipDataConfig = null;
            self.EquipDataConfigId = 0;

            //拓展属性
            foreach (EquipData equipData in self.EquipExtDataList)
            {
                equipData.Dispose();
            }

            self.EquipExtDataList.Clear();
            self.EquipExtDataList = null;

            //镶嵌
            foreach (InlayData inlayData in self.InlayDataList)
            {
                inlayData.Dispose();
            }

            self.InlayDataList.Clear();
            self.InlayDataList = null;

            self.Dispose();
        }
    } //class_end
}