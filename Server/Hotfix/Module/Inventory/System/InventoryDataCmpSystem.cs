/**
 * * 背包物品数据组件。挂上这个组件的物品就是背包物品
 * 它和WorldItemDataCmp是互斥的。
 */

using ETModel;

namespace ETHotfix
{
    [ObjectSystem]
    public class InventoryDataCmpAwakeSystem : AwakeSystem<InventoryDataCmp>
    {
        public override void Awake(InventoryDataCmp self)
        {
            self.Entity.RemoveComponent<WorldItemDataCmp>();
        }
    }

    [ObjectSystem]
    public class InventoryDataCmpDestorySystem : DestroySystem<InventoryDataCmp>
    {
        public override void Destroy(InventoryDataCmp self)
        {
            self.Dispose();
        }
    }

    public static class InventoryDataCmpSystem
    {
        /// <summary>
        /// 增减数量
        /// </summary>
        /// <param name="num"></param>
        public static int AddNum(this InventoryDataCmp self, int num)
        {
            self.CountNum += num;
            return self.CountNum;
        }

        /// <summary>
        /// 增减数量后，物品是否满。 
        /// </summary>
        /// <param name="num"></param>
        /// <returns></returns>
        public static bool AfterAddIsFull(this InventoryDataCmp self, int num)
        {
            return self.CountNum + num >= ((Item) self.Entity).GetItemConfig().MaxNum;
        }

        /// <summary>
        /// 析构
        /// </summary>
        public static void Dispose(this InventoryDataCmp self)
        {
            if (self.IsDisposed)
            {
                return;
            }

            self.Index = 0;
            self.CountNum = 0;
            self.Dispose();
        }
    }
}