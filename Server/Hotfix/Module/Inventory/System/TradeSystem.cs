/**
 * 交易实体
 */

using System.Collections.Generic;
using ETModel;

namespace ETHotfix
{
    [ObjectSystem]
    public class TradeDestorySystem : DestroySystem<Trade>
    {
        public override void Destroy(Trade self)
        {
            self.Dispose();
        }
    }

    public static class TradeSystem
    {
        /// <summary>
        /// 设置对应选手的交易状态
        /// </summary>
        /// <param name="self"></param>
        /// <param name="unitId"></param>
        /// <param name="tradeState"></param>
        public static void SetTradeState(this Trade self, long unitId, TradeState tradeState)
        {
            if (!self.TradeStates.TryAdd(unitId, tradeState))
            {
                self.TradeStates[unitId] = tradeState;
            }
        }

        /// <summary>
        /// 设置交易的物品
        /// </summary>
        /// <param name="self"></param>
        /// <param name="unitId"></param>
        /// <param name="commodity"></param>
        public static void SetCommodity(this Trade self, long unitId, ETModel.Commodity commodity)
        {
            if (!self.TradeCommodity.TryAdd(unitId, commodity))
            {
                self.TradeCommodity[unitId] = commodity;
            }
        }

        /// <summary>
        /// 同意交易，如果二个人都同意交易，则交易完成
        /// </summary>
        /// <param name="self"></param>
        /// <param name="tradeId"></param>
        /// <returns></returns>
        public static bool ConfirmTrade(this Trade self, long tradeId)
        {
            self.SetTradeState(tradeId, TradeState.WaitingOtherAffirm);
            int flag = 0;
            foreach (var state in self.TradeStates.Values)
            {
                if (state == TradeState.WaitingOtherAffirm)
                {
                    flag++;
                }
            }

            return flag == 2;
        }

        /// <summary>
        /// 交易结束 ，交换物品。
        /// </summary>
        /// <param name="self"></param>
        /// <returns></returns>
        public static bool ChangeItem(this Trade self)
        {
            //先检查两位的背包是否能承下
            ETModel.Commodity anotherCommodity = self.TradeCommodity[self.AnotherTrader.Id];
            List<Item> anotherCommodityItemList = anotherCommodity.ItemList;
            List<int> anotherCommodityNums = new List<int>();
            foreach (Item item in anotherCommodityItemList)
            {
                anotherCommodityNums.Add(item.GetComponent<InventoryDataCmp>().CountNum);
            }

            bool initerCanadd = self.InitTrader.GetComponent<InventoryCmp>()
                .CanAddInventoryItem(null, anotherCommodityNums, anotherCommodityItemList);

            ETModel.Commodity initerCommodity = self.TradeCommodity[self.InitTrader.Id];
            List<Item> initerCommodityItemList = initerCommodity.ItemList;
            List<int> interCommodityNums = new List<int>();
            foreach (Item item in initerCommodityItemList)
            {
                interCommodityNums.Add(item.GetComponent<InventoryDataCmp>().CountNum);
            }

            bool anotherCanAdd = self.AnotherTrader.GetComponent<InventoryCmp>()
                .CanAddInventoryItem(null, interCommodityNums, initerCommodityItemList);

            if (!initerCanadd || !anotherCanAdd)
            {
                return false;
            }

            //先扣减2位的金币/货物
            foreach (Item item in initerCommodityItemList)
            {
                self.InitTrader.GetComponent<InventoryCmp>()
                    .RemoveItem(item.Id, item.GetComponent<InventoryDataCmp>().CountNum);
            }

            self.InitTrader.GetComponent<InventoryCmp>().Gold -= initerCommodity.Price;
            foreach (Item item in anotherCommodityItemList)
            {
                self.AnotherTrader.GetComponent<InventoryCmp>()
                    .RemoveItem(item.Id, item.GetComponent<InventoryDataCmp>().CountNum);
            }

            self.AnotherTrader.GetComponent<InventoryCmp>().Gold -= anotherCommodity.Price;
            //增加2位的货物
            foreach (Item item in anotherCommodityItemList)
            {
                self.InitTrader.GetComponent<InventoryCmp>()
                    .AddItem(0, item.GetComponent<InventoryDataCmp>().CountNum, item, out int i);
            }

            self.InitTrader.GetComponent<InventoryCmp>().Gold += anotherCommodity.Price;
            foreach (Item item in initerCommodityItemList)
            {
                self.AnotherTrader.GetComponent<InventoryCmp>()
                    .AddItem(0, item.GetComponent<InventoryDataCmp>().CountNum, item, out int i);
            }

            self.AnotherTrader.GetComponent<InventoryCmp>().Gold += initerCommodity.Price;
            return true;
        }

        public static void Dispose(this Trade self)
        {
            if (self.IsDisposed)
            {
                return;
            }

            self.InitTrader?.GetComponent<InventoryCmp>()?.UnLock();
            self.AnotherTrader?.GetComponent<InventoryCmp>()?.UnLock();
            M2C_NoticeTradeCanceled msg = new M2C_NoticeTradeCanceled();
            msg.TradeId = self.Id;
            //通知两位
            MessageHelper.SendMsg(self.InitTrader, msg);
            MessageHelper.SendMsg(self.AnotherTrader, msg);

            self.TradeStates.Clear();
            self.TradeCommodity.Clear();
            self.InitTrader = null;
            self.AnotherTrader = null;
            self.Dispose();
        }
    }
}