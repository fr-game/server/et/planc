/**
 * 背包组件。Unit挂上之后就可以使用Inventory功能了。
 */

using System.Collections.Generic;
using System.Threading.Tasks;
using ETModel;

namespace ETHotfix
{
    [ObjectSystem]
    public class InventoryCmpAwakeSystem : AwakeSystem<InventoryCmp, long>
    {
        public override void Awake(InventoryCmp self, long playerId)
        {
            self.Awake(playerId);
        }
    }

    [ObjectSystem]
    public class InventoryCmpDestorySystem : DestroySystem<InventoryCmp>
    {
        public override void Destroy(InventoryCmp self)
        {
            self.Dispose();
        }
    }

    public static class InventoryCmpSystem
    {
        /// <summary>
        /// 初始化背包
        /// </summary>
        /// <param name="mode">游戏模式</param>
        public static void Awake(this InventoryCmp self, long playerId)
        {
            self.InitInventory(playerId);
        }

        /// <summary>
        /// 初始化背包
        /// </summary>
        /// <param name="playerId"></param>
        public static async void InitInventory(this InventoryCmp self, long playerId)
        {
            //加载背包
            self.LastSortTime = 0;
            self.PlayerId = playerId;
        }

        /// <summary>
        /// 更新设置的金币数量
        /// </summary>
        /// <param name="self"></param>
        /// <param name="goldNum"></param>
        /// <returns></returns>
        public static bool AddInventoryGold(this InventoryCmp self, int goldNum)
        {
            if (self.IsLocking())
            {
                Log.Info("背包锁定中，添加物品到背包失败");
                return false;
            }

            //上锁
            self.Lock();

            int surplus = 0;
            self.Gold += goldNum;
            //解锁
            self.UnLock();

            //触发背包添加物品的事件(持久化到数据库。)
            Game.EventSystem.Run(EventIdType.InventoryUpdate, self);
            return true;
        }

        /// <summary>
        /// 添加物品到背包
        /// </summary>
        /// <param name="itemConfigId">配置Id</param>
        /// <param name="num">数量</param>
        /// <param name="item">物品对象。</param>
        /// <returns></returns>
        public static bool AddInventoryItem(this InventoryCmp self, long itemConfigId, int num, Item item = null)
        {
            if (self.IsLocking())
            {
                Log.Info("背包锁定中，添加物品到背包失败");
                return false;
            }

            //上锁
            self.Lock();

            int surplus = 0;
            bool addItemRes;
            do
            {
                addItemRes = self.AddItem(itemConfigId, num, item, out surplus);
                num = surplus;
            } while (addItemRes && surplus > 0);

            //解锁
            self.UnLock();

            //触发背包添加物品的事件(持久化到数据库。)
            Game.EventSystem.Run(EventIdType.InventoryUpdate, self);
            return true;
        }
        
         /// <summary>
         /// 添加物品到背包
         /// </summary>
         /// <param name="self"></param>
         /// <param name="items"></param>
         /// <returns></returns>
        public static bool AddInventoryItemServer(this InventoryCmp self, List<Item> items)
        {
            if (self.IsLocking())
            {
                Log.Info("背包锁定中，添加物品到背包失败");
                return false;
            }

            //上锁
            self.Lock();
            foreach (Item item in items)
            {
                long itemId = item.Id;
                var itemById = self.GetItemById(itemId);
               
                if (itemById != null)
                {
                    //已经有的。将数量加到现有Item中
                    itemById.GetComponent<InventoryDataCmp>().CountNum = item.GetComponent<InventoryDataCmp>().CountNum;
                    //销毁掉
                    item.Dispose();
                }
                else
                {
                    //不存在的，按照item的索引直接加入到列表中。
                    self.Items[item.GetComponent<InventoryDataCmp>().Index] = item;
                }
            }            
            //解锁
            self.UnLock();

            //触发背包添加物品的事件(持久化到数据库。)
            Game.EventSystem.Run(EventIdType.InventoryUpdate, self);
            return true;
        }

        /// <summary>
        /// 丢弃物品
        /// </summary>
        /// <param name="item">要丢弃的为item对象</param>
        /// <param name="num">数量</param>
        /// <returns></returns>
        public static bool DropInventoryItem(this InventoryCmp self, long itemId, int num)
        {
            if (itemId == 0)
            {
                Log.Debug("丢弃物品失败，物品ID为0");
                return false;
            }

            if (self.IsLocking())
            {
                Log.Debug("背包锁定中，丢弃物品失败");
                return false;
            }

            //上锁
            self.Lock();
            Item itemById = self.GetItemById(itemId);
            if (itemById == null)
            {
                self.UnLock();
                Log.Debug("丢弃物品失败，物品不在背包内");
            }

            bool removeItem = self.RemoveItem(itemId, num);
            //解锁
            self.UnLock();

            if (removeItem)
            {
                //持久化到数据库
                Game.EventSystem.Run(EventIdType.InventoryUpdate, self);
            }

            return removeItem;
        }

        /// <summary>
        /// 添加物品
        /// </summary>
        /// <param name="itemConfigId">配置Id</param>
        /// <param name="num">数量</param>
        /// <param name="item">物品对象。</param>
        /// <param name="surplus">out 添加后的剩余数量</param>
        /// <returns>返回true，代表可以添加商品。surplus代表添加后的num剩余的商品</returns>
        public static bool AddItem(this InventoryCmp self, long itemConfigId, int num, Item item, out int surplus)
        {
            surplus = 0;
            //item不为空需要直接放入一个物品格子。
            if (item != null && self.InventoryIsFull())
            {
                Log.Debug("背包已满，加入item失败");
                return false;
            }

            //传入的item=null,首先从背包中找一个未满的同类item.
            if (item == null)
            {
                List<Item> findItemsByItemConfigId = self.FindItemsByItemConfigId(itemConfigId);
                item = self.GetNoFullAndCanManyItemInList(findItemsByItemConfigId, num);
            }

            //背包中没有未满的同类item，则创建一个，创建之前检查一下有没有合适的位置/
            if (item == null)
            {
                if (self.InventoryIsFull())
                {
                    Log.Debug("背包已满，加入item失败");
                    return false;
                }

                int availableIndex = self.GetAvailableIndex();
                if (availableIndex < 0)
                {
                    Log.Debug("背包已满，加入item失败");
                    return false;
                }

                item = ItemFactory.CreateItem(itemConfigId);
                InventoryDataCmp inventoryDataCmp = item.AddComponent<InventoryDataCmp>();
                inventoryDataCmp.Index = availableIndex;
                inventoryDataCmp.CountNum = 0;
            }

            //可以增加的数量 99
            int canAddNum = item.GetItemConfig().MaxNum - item.GetComponent<InventoryDataCmp>().CountNum;
            //99-100
            int thisTimeNum = canAddNum - num >= 0 ? num : canAddNum;
            //99-1
            surplus = num - thisTimeNum;
            item.GetComponent<InventoryDataCmp>().AddNum(thisTimeNum);
            //放入背包
            self.Items[item.GetComponent<InventoryDataCmp>().Index] = item;


            return true;
        }

        /// <summary>
        /// 移除物品
        /// </summary>
        /// <param name="itemId">Item对象的Id</param>
        /// <param name="num">移除的数量</param>
        /// <returns></returns>
        public static bool RemoveItem(this InventoryCmp self, long itemId, int num)
        {
            //根据itemId找到item；
            Item item = self.GetItemById(itemId);
            if (item == null)
            {
                return false;
            }

            //校验item数量
            InventoryDataCmp inventoryDataCmp = item.GetComponent<InventoryDataCmp>();
            if (inventoryDataCmp.CountNum < num)
            {
                return false;
            }

            //更新item数量
            int countNum = inventoryDataCmp.AddNum(-num);
            if (countNum <= 0)
            {
                //移除item
                self.Items[inventoryDataCmp.Index] = null;
                item.Dispose();
            }

            return true;
        }

        /// <summary>
        /// 根据id获取实体。
        /// </summary>
        /// <param name="itemId"></param>
        /// <returns></returns>
        private static Item GetItemById(this InventoryCmp self, long itemId)
        {
            foreach (Item item in self.Items.Values)
            {
                if (item != null && item.Id == itemId)
                {
                    return item;
                }
            }

            return null;
        }

        /// <summary>
        /// 根据位置获取item
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public static Item GetItemByIndex(this InventoryCmp self, int index)
        {
            return self.Items[index];
        }

        /// <summary>
        /// 从传入的item列表中找一个未满的可叠加item.
        /// </summary>
        /// <param name="items"></param>
        /// <param name="num"></param>
        /// <returns></returns>
        public static Item GetNoFullAndCanManyItemInList(this InventoryCmp self, List<Item> items, int num)
        {
            foreach (Item item in items)
            {
                if (!item.CanMany())
                {
                    continue;
                }

                if (item.GetComponent<InventoryDataCmp>().CountNum + num <= item.GetItemConfig().MaxNum)
                {
                    return item;
                }
            }

            return null;
        }

        /// <summary>
        /// 背包是否已满？
        /// 无法获取可用位置，则代表背包已满
        /// </summary>
        /// <returns></returns>
        public static bool InventoryIsFull(this InventoryCmp self)
        {
            return self.GetAvailableIndex() < 0;
        }

        /// <summary>
        /// 使用物品
        /// </summary>
        /// <param name="itemId">Item对象的Id</param>
        /// <param name="num">使用的数量</param>
        /// <param name="target">给谁使用</param>
        /// <returns></returns>
        public static string UseItem(this InventoryCmp self, long itemId, int num, Unit target)
        {
            //todo
            return "Success";
        }

        /// <summary>
        /// 根据Item配置的id查询相关的Item集合
        /// </summary>
        /// <param name="itemConfigId"></param>
        /// <returns></returns>
        public static List<Item> FindItemsByItemConfigId(this InventoryCmp self, long itemConfigId)
        {
            List<Item> result = new List<Item>();
            foreach (Item item in self.Items.Values)
            {
                if (item == null)
                {
                    continue;
                }

                if (item.ItemConfigId == itemConfigId)
                {
                    result.Add(item);
                }
            }

            return result;
        }

        /// <summary>
        /// 锁定背包
        /// </summary>
        public static void Lock(this InventoryCmp self)
        {
            self.InventoryStatus = InventoryStatus.Locking;
        }

        /// <summary>
        /// 解锁背包
        /// </summary>
        public static void UnLock(this InventoryCmp self)
        {
            self.InventoryStatus = InventoryStatus.Normal;
        }

        /// <summary>
        /// 是否锁定
        /// </summary>
        /// <returns></returns>
        public static bool IsLocking(this InventoryCmp self)
        {
            return self.InventoryStatus == InventoryStatus.Locking;
        }

        /// <summary>
        /// 获取背包可用位置
        /// </summary>
        /// <returns></returns>
        public static int GetAvailableIndex(this InventoryCmp self)
        {
            foreach (int key in self.Items.Keys)
            {
                if (self.Items[key] == null)
                {
                    return key;
                }
            }

            return -1;
        }

        /// <summary>
        /// 是否可以添加这些物品到背包内。
        /// itemConfigIds items 两个参数是互斥的。
        /// </summary>
        /// <param name="itemConfigIds">配置Id</param>
        /// <param name="nums">数量</param>
        /// <param name="items">物品对象。</param>
        /// <returns></returns>
        public static bool CanAddInventoryItem(this InventoryCmp self, List<long> itemConfigIds, List<int> nums,
            List<Item> items)
        {
            if (itemConfigIds != null)
            {
                //记录背包的剩余空间。
                int nullCell = 0;
                //已有item的剩余空间
                Dictionary<long, int> itemNumSurplus = new Dictionary<long, int>();
                foreach (var item in self.Items.Values)
                {
                    if (item == null)
                    {
                        nullCell += 1;
                        continue;
                    }

                    if (!item.CanMany())
                    {
                        continue;
                    }

                    if (item.GetComponent<InventoryDataCmp>().CountNum < item.GetItemConfig().MaxNum)
                    {
                        int surplusTmp = item.GetItemConfig().MaxNum - item.GetComponent<InventoryDataCmp>().CountNum;
                        if (!itemNumSurplus.TryGetValue(item.ItemConfigId, out int surplus))
                        {
                            itemNumSurplus.Add(item.ItemConfigId, surplusTmp);
                        }
                        else
                        {
                            itemNumSurplus[item.ItemConfigId] = surplus + surplusTmp;
                        }
                    }
                }

                //每个商品减去一定的可用空间
                for (int i = 0; i < itemConfigIds.Count; i++)
                {
                    //物品的配置
                    ItemConfig itemConfig = Game.Scene.GetComponent<InventoryConfigMrgCmp>()
                        .GetItemConfig(itemConfigIds[i]);
                    //物品的数量
                    int num = nums[i];
                    //先从已有Item的剩余空间中减去
                    if (itemNumSurplus.TryGetValue(itemConfigIds[i], out int surplus))
                    {
                        //剩余可用空间
                        int tmp = surplus - num < 0 ? 0 : num - surplus;
                        if (tmp == 0)
                        {
                            itemNumSurplus.Remove(itemConfigIds[i]);
                        }

                        //剩余需要存放的数量
                        num = num - surplus < 0 ? 0 : num - surplus;
                    }

                    //从剩余空间中获取
                    if (num > 0)
                    {
                        //可以叠加的物品先叠加再使用空的单元格，而且需要把空的单元格剩余数量放到itemNumSurplus
                        if (itemConfig.CanMany == "true")
                        {
                            //获取物品的叠加最大值数量
                            int maxNum = itemConfig.MaxNum;
                            //剩余数量需要的单元格
                            int needCell = num % maxNum > 0 ? num / maxNum + 1 : num / maxNum;
                            //剩余空单元格。
                            nullCell -= needCell;
                            if (nullCell < 0)
                            {
                                return false;
                            }

                            //未满单元格的数量。
                            int newItemNumSurplus = needCell * maxNum - num;
                            if (newItemNumSurplus > 0)
                            {
                                itemNumSurplus.Add(itemConfigIds[i], newItemNumSurplus);
                            }
                        }
                        //非叠加物品需要检查剩余单元格数量
                        else
                        {
                            //剩余空单元格。
                            nullCell -= num;
                            if (nullCell < 0)
                            {
                                return false;
                            }
                        }
                    }
                }
            }
            else
            {
                int nullCell = 0;
                foreach (var item in self.Items.Values)
                {
                    if (item == null)
                    {
                        nullCell += 1;
                    }
                }

                if (items.Count > nullCell)
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 排序、整理背包。
        /// </summary>
        public static bool SortInventory(this InventoryCmp self)
        {
            //1546613620618
            if (TimeHelper.Now() - self.LastSortTime < 1000 * 10)
            {
                Log.Debug($"操作过于频繁,稍后再试{TimeHelper.Now()}；{self.LastSortTime}");
                return false;
            }

            //获取所有背包中的Item;
            List<Item> listTemp = new List<Item>();
            foreach (Item item in self.Items.Values)
            {
                if (item != null)
                {
                    listTemp.Add(item);
                }
            }

            //根据配置的Id排序1.合并同类Item
            foreach (Item item in listTemp)
            {
                if (item.IsDisposed)
                {
                    continue;
                }

                if (!item.CanMany())
                {
                    continue;
                }

                if (item.GetComponent<InventoryDataCmp>().CountNum >= item.GetItemConfig().MaxNum)
                {
                    continue;
                }

                foreach (Item itemTemp in listTemp)
                {
                    if (itemTemp.IsDisposed)
                    {
                        continue;
                    }

                    if (item.GetComponent<InventoryDataCmp>().Index == itemTemp.GetComponent<InventoryDataCmp>().Index)
                    {
                        continue;
                    }

                    if (item.ItemConfigId == itemTemp.ItemConfigId)
                    {
                        //这个物品的可以用剩余空间 98
                        int surplus = item.GetItemConfig().MaxNum - item.GetComponent<InventoryDataCmp>().CountNum;
                        int thisTimeAdd = surplus > itemTemp.GetComponent<InventoryDataCmp>().CountNum
                            ? itemTemp.GetComponent<InventoryDataCmp>().CountNum
                            : surplus;
                        item.GetComponent<InventoryDataCmp>().AddNum(thisTimeAdd);
                        int itemTempNowCount = itemTemp.GetComponent<InventoryDataCmp>().AddNum(-thisTimeAdd);

                        if (itemTempNowCount <= 0)
                        {
                            itemTemp.Dispose();
                        }
                    }
                }
            }

            //根据配置的Id排序2.排序
            for (int i = 0; i < listTemp.Count; i++)
            {
                if (listTemp[i].IsDisposed)
                {
                    continue;
                }

                Item iItem = listTemp[i];
                for (int j = listTemp.Count - 1; j >= i; j--)
                {
                    if (listTemp[j].IsDisposed)
                    {
                        continue;
                    }

                    Item jItem = listTemp[j];
                    if (jItem.ItemConfigId < iItem.ItemConfigId)
                    {
                        listTemp[j] = iItem;
                        listTemp[i] = jItem;
                        iItem = jItem;
                    }
                }
            }

            //依次重新放入背包。
            //背包物品 2.放入背包
            for (int i = 0; i < self.MaxSize; i++)
            {
                self.Items[i] = null;
            }

            for (int i = 0; i < listTemp.Count; i++)
            {
                self.Items[i] = listTemp[i].IsDisposed ? null : listTemp[i];
            }

            listTemp.Clear();
            //记录最后的执行时间
            self.LastSortTime = TimeHelper.Now();
            //入库
            Game.EventSystem.Run(EventIdType.InventoryUpdate, self);
            return true;
        }
    } //class_end
}