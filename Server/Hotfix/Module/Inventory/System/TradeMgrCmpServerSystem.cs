/**
 * 交易管理组件。客户端挂上可以发起交易，服务端挂上可以管理交易 。
 * 
 */

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ETModel;

namespace ETHotfix
{
    [ObjectSystem]
    public class TradeMgrCmpServerAwakeSystem : AwakeSystem<TradeMgrCmpServer>
    {
        public override void Awake(TradeMgrCmpServer self)
        {
            self.Awake();
        }
    }

    [ObjectSystem]
    public class TradeMgrCmpServerDestroySystem : DestroySystem<TradeMgrCmpServer>
    {
        public override void Destroy(TradeMgrCmpServer self)
        {
            self.Dispose();
        }
    }

    public static class TradeMgrCmpServerSystem
    {
        /// <summary>
        /// 初始化组件
        /// </summary>
        /// <param name="self"></param>
        public static void Awake(this TradeMgrCmpServer self)
        {
            self.CurrentTradeS = new Dictionary<long, Trade>();
            //开启异常检查
            self.CheckTrade();
            Log.Info("服务端交易管理组件开启.");
        }

        /// <summary>
        /// 检查异常交易。
        /// </summary>
        /// <param name="self"></param>
        /// <returns></returns>
        public static async Task CheckTrade(this TradeMgrCmpServer self)
        {
            long currentId = self.InstanceId;
            while (currentId == self.InstanceId)
            {
                //1分钟检查一次
                await Game.Scene.GetComponent<TimerComponent>().WaitAsync(1000 * 60);
                Log.Debug("1分钟检查一次异常交易");
                //关闭超过一小时的。或者那些掉线的小朋友的交易。
                foreach (var trade in self.CurrentTradeS.Values.ToArray())
                {
                    //todo 有没有掉线
                    //超过一小时
                    if (TimeHelper.Now() > trade.CreateTime + 1000 * 60 * 60 * 60)
                    {
                        self.CancelTrade(trade.Id);
                    }
                }
            }
        }

        /// <summary>
        /// 初始化交易 /创建一笔交易
        /// </summary>
        /// <param name="self"></param>
        /// <param name="tradeId"></param>
        /// <param name="unitId"></param>
        /// <param name="anotherId"></param>
        /// <returns></returns>
        public static bool InitTrade(this TradeMgrCmpServer self, long tradeId, long unitId, long anotherId)
        {
            //检查是否进行着交易
            if (self.CurrentTradeS.TryGetValue(tradeId, out Trade trade))
            {
                Log.Info($"该交易已经存在");
                return false;
            }

            //获取Unit
            Unit unit = Game.Scene.GetComponent<UnitComponent>().Get(unitId);
            if (unit == null)
            {
                Log.Info($"未找到Unit");
                return false;
            }

            //获取交易对方
            Unit anotherUnit = Game.Scene.GetComponent<UnitComponent>().Get(unitId);
            if (anotherUnit == null)
            {
                Log.Info($"未找到Unit");
                return false;
            }

            //检查背包状态
            var isLocking = unit.GetComponent<InventoryCmp>().IsLocking();
            var anotherIsLocking = anotherUnit.GetComponent<InventoryCmp>().IsLocking();
            if (isLocking || anotherIsLocking)
            {
                Log.Info($"包锁定");
                return false;
            }

            //todo  一些其他的判断。比如Unit的状态。
            //锁定包
            unit.GetComponent<InventoryCmp>().Lock();
            anotherUnit.GetComponent<InventoryCmp>().Lock();
            //创建Trade
            Trade CurrentTrade = ComponentFactory.Create<Trade>();
            CurrentTrade.Id = tradeId;
            CurrentTrade.TradeStates = new Dictionary<long, TradeState>();
            CurrentTrade.TradeCommodity = new Dictionary<long, ETModel.Commodity>();
            CurrentTrade.InitTrader = unit;
            CurrentTrade.AnotherTrader = anotherUnit;
            CurrentTrade.SetTradeState(unitId, TradeState.WaitingOtherYnc);
            CurrentTrade.SetTradeState(anotherId, TradeState.Init);
            CurrentTrade.SetCommodity(unitId, new ETModel.Commodity {TraderId = unit.Id});
            CurrentTrade.SetCommodity(unitId, new ETModel.Commodity {TraderId = anotherUnit.Id});
            self.CurrentTradeS.Add(tradeId, CurrentTrade);
            return true;
        }

        /// <summary>
        /// 设置交易状态
        /// </summary>
        /// <param name="self"></param>
        /// <param name="tradeId"></param>
        /// <param name="unitId"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        public static bool SetTradeState(this TradeMgrCmpServer self, long tradeId, long unitId, TradeState state)
        {
            if (!self.CurrentTradeS.TryGetValue(tradeId, out Trade trade))
            {
                Log.Info($"交易不存在");
                return false;
            }

            trade.SetTradeState(unitId, state);
            return true;
        }

        /// <summary>
        /// 当前交易状态
        /// </summary>
        /// <param name="self"></param>
        /// <param name="tradeId"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        public static bool GetTradeState(this TradeMgrCmpServer self, long tradeId, TradeState state)
        {
            if (!self.CurrentTradeS.TryGetValue(tradeId, out Trade trade))
            {
                Log.Info($"交易不存在");
                return false;
            }

            int num = 0;
            bool flag = false;
            foreach (var tradeState in trade.TradeStates.Values)
            {
                flag = tradeState == state;
                num++;
            }

            return num == 2 && flag;
        }

        /// <summary>
        /// 更改交易物品
        /// </summary>
        /// <param name="self"></param>
        /// <param name="tradeId"></param>
        /// <param name="commodity"></param>
        /// <returns></returns>
        public static bool UpdateTredeItems(this TradeMgrCmpServer self, long tradeId, ETModel.Commodity commodity)
        {
            if (!self.CurrentTradeS.TryGetValue(tradeId, out Trade trade))
            {
                Log.Info($"交易不存在");
                return false;
            }

            trade.SetCommodity(tradeId, commodity);
            return true;
        }


        /// <summary>
        /// 确认交易
        /// </summary>
        public static bool ConfirmTrade(this TradeMgrCmpServer self, long tradeId, long unitId)
        {
            if (!self.CurrentTradeS.TryGetValue(tradeId, out Trade trade))
            {
                Log.Info($"交易不存在");
                return false;
            }

            return trade.ConfirmTrade(unitId);
        }


        /// <summary>
        /// 取消交易 
        /// </summary>
        public static void CancelTrade(this TradeMgrCmpServer self, long tradeId)
        {
            if (!self.CurrentTradeS.TryGetValue(tradeId, out Trade trade))
            {
                Log.Info($"交易不存在");
                return;
            }

            trade.Dispose();
            self.CurrentTradeS.Remove(tradeId);
        }

        /// <summary>
        /// 获取交易。
        /// </summary>
        /// <param name="self"></param>
        /// <param name="tradeId"></param>
        /// <returns></returns>
        public static Trade GetTradeById(this TradeMgrCmpServer self, long tradeId)
        {
            if (!self.CurrentTradeS.TryGetValue(tradeId, out Trade trade))
            {
                Log.Info($"交易不存在");
            }

            return trade;
        }

        public static void Dispose(this TradeMgrCmpServer self)
        {
            if (self.IsDisposed)
            {
                return;
            }

            foreach (Trade trade in self.CurrentTradeS.Values)
            {
                trade.Dispose();
            }

            self.CurrentTradeS.Clear();
            self.Dispose();
        }
    } //class_end
}