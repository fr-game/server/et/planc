/**
 * 背包系统配置信息管理模块
 */

using ETModel;

namespace ETHotfix
{
    public static class InventoryConfigMrgCmpSystem
    {
        public static ItemConfig GetItemConfig(this InventoryConfigMrgCmp self, long itemConfigId)
        {
            ItemConfig itemConfig = (ItemConfig) Game.Scene.GetComponent<ConfigComponent>()
                .Get(typeof(ItemConfig), (int) itemConfigId);
            if (itemConfig == null)
            {
                Log.Error($"未找到Id是{itemConfigId}的{typeof(ItemConfig)}配置信息。");
            }

            return itemConfig;
        }

        public static Effect GetEffectConfig(this InventoryConfigMrgCmp self, long effectId)
        {
            Effect effect = (Effect) Game.Scene.GetComponent<ConfigComponent>().Get(typeof(Effect), (int) effectId);
            if (effect == null)
            {
                Log.Error($"未找到Id是{effectId}的{typeof(Effect)}配置信息。");
            }

            return effect;
        }

        public static StoreConfig GetStoreConfig(this InventoryConfigMrgCmp self, long storeConfigId)
        {
            StoreConfig storeConfig = (StoreConfig) Game.Scene.GetComponent<ConfigComponent>()
                .Get(typeof(StoreConfig), (int) storeConfigId);
            if (storeConfig == null)
            {
                Log.Error($"未找到Id是{storeConfigId}的{typeof(StoreConfig)}配置信息。");
            }

            return storeConfig;
        }

        public static UpgradeConfig GetUpgradeConfig(this InventoryConfigMrgCmp self, long upGradeConfigId)
        {
            UpgradeConfig upgradeConfig = (UpgradeConfig) Game.Scene.GetComponent<ConfigComponent>()
                .Get(typeof(UpgradeConfig), (int) upGradeConfigId);
            if (upgradeConfig == null)
            {
                Log.Error($"未找到Id是{upGradeConfigId}的{typeof(UpgradeConfig)}配置信息。");
            }

            return upgradeConfig;
        }

        public static EquipDataConfig GetEquipDataConfig(this InventoryConfigMrgCmp self, long equipDataConfigId)
        {
            EquipDataConfig equipDataConfig = (EquipDataConfig) Game.Scene.GetComponent<ConfigComponent>()
                .Get(typeof(EquipDataConfig), (int) equipDataConfigId);
            if (equipDataConfig == null)
            {
                Log.Error($"未找到Id是{equipDataConfigId}的{typeof(UpgradeConfig)}配置信息。");
            }

            return equipDataConfig;
        }

        public static EquipDataNumericConfig GetEquipDataNumericConfig(this InventoryConfigMrgCmp self,
            long equipDataDescConfigId)
        {
            EquipDataNumericConfig equipDataDescConfig = (EquipDataNumericConfig) Game.Scene
                .GetComponent<ConfigComponent>().Get(typeof(EquipDataNumericConfig), (int) equipDataDescConfigId);
            if (equipDataDescConfig == null)
            {
                Log.Error($"未找到Id是{equipDataDescConfigId}的{typeof(EquipDataNumericConfig)}配置信息。");
            }

            return equipDataDescConfig;
        }
    } //class_end
}