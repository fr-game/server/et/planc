/**
 * 物品实体。逻辑。
 */

using ETModel;

namespace ETHotfix
{
    [ObjectSystem]
    public class ItemAwakeSystem : AwakeSystem<Item>
    {
        public override void Awake(Item self)
        {
            self.Awake();
        }
    }

    [ObjectSystem]
    public class ItemDestorySystem : DestroySystem<Item>
    {
        public override void Destroy(Item self)
        {
            self.Dispose();
        }
    }

    public static class ItemSystem
    {
        //初始化物品
        public static void Awake(this Item self)
        {
            self.LastUseTime = 0;
        }

        /// <summary>
        /// 设置ItemConfig
        /// </summary>
        /// <param name="itemConfig"></param>
        public static void SetItemConfig(this Item self, ItemConfig itemConfig)
        {
            self.ItemConfig = itemConfig;
        }

        /// <summary>
        /// 获取物品配置信息
        /// </summary>
        /// <returns></returns>
        public static ItemConfig GetItemConfig(this Item self)
        {
            return self.ItemConfig;
        }

        /// <summary>
        /// 是否可用
        /// </summary>
        /// <param name="target">要给谁用</param>
        /// <returns></returns>
        public static bool CanUse(this Item self, Unit target)
        {
            //TODO
            return false;
        }

        /// <summary>
        /// 获取物品的名称
        /// </summary>
        /// <returns></returns>
        public static string GetName(this Item self)
        {
            return self.ItemConfig.Name;
        }

        /// <summary>
        /// 获取物品的描述信息
        /// </summary>
        /// <returns></returns>
        public static string GetDescription(this Item self)
        {
            return self.ItemConfig.Description;
        }

        /// <summary>
        /// 获取物品类型
        /// </summary>
        /// <returns></returns>
        public static ItemType GetItemType(this Item self)
        {
            return EnumHelper.FromString<ItemType>(self.ItemConfig.ItemType);
        }

        /// <summary>
        /// 使用物品
        /// </summary>
        /// <param name="target">要给谁用</param>
        /// <returns></returns>
        public static bool Use(this Item self, Unit target)
        {
            return false;
        }


        /// <summary>
        /// 是否可以叠加
        /// </summary>
        /// <returns></returns>
        public static bool CanMany(this Item self)
        {
            return self.ItemConfig.CanMany == "true";
        }

        /// <summary>
        /// 是否可以一次使用或者出售或者丢弃多个
        /// </summary>
        /// <returns></returns>
        public static bool GetCanMany(this Item self)
        {
            InventoryDataCmp inventoryDataCmp = self.GetComponent<InventoryDataCmp>();
            if (inventoryDataCmp == null)
            {
                return false;
            }

            return self.ItemConfig.CanMany == "true" && inventoryDataCmp.CountNum > 1;
        }

        //销毁事件
        public static void Dispose(this Item self)
        {
            if (self.IsDisposed)
            {
                return;
            }

            Log.Debug("Item销毁开始");
            self.EffectList.Clear();
            self.EffectList = null;
            self.LastUseTime = 0;
            self.Dispose();
        }

        public static Item CloneItem(this Item self)
        {
            Item item = ItemFactory.CreateItemFromRecord(self.ItemConfigId);
            foreach (Component component in self.GetComponents())
            {
                Entity componentEntity = component.Entity;
                Component componentParent = component.Parent;
                component.Entity = null;
                component.Parent = null;
                string cmpDataJson = JsonHelper.ToJson(component);
                component.Entity = componentEntity;
                component.Parent = componentParent;
                var fromJson = (Component) JsonHelper.FromJson(component.GetType(), cmpDataJson);
                fromJson.EndDeSerialize();
                item.AddComponent(fromJson);
            }

            if (item.GetComponent<InventoryDataCmp>() != null)
            {
                item.GetComponent<InventoryDataCmp>().CountNum = 1;
            }

            return item;
        }
    } //class_end
}