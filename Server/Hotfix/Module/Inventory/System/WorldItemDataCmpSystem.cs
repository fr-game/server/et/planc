/**
 * 挂载了这个组件的Item就是世界物品了。
 * 就是一个在世界上可见的物品。它和InventoryDataCmp是互斥的。
 */

using System.Numerics;
using ETModel;

namespace ETHotfix
{
    [ObjectSystem]
    public class WorldItemDataCmpAwakeSystem : AwakeSystem<WorldItemDataCmp>
    {
        public override void Awake(WorldItemDataCmp self)
        {
            self.Entity.RemoveComponent<InventoryDataCmp>();
        }
    }

    [ObjectSystem]
    public class WorldItemDataCmpDestorySystem : DestroySystem<WorldItemDataCmp>
    {
        public override void Destroy(WorldItemDataCmp self)
        {
            self.Dispose();
        }
    }

    public static class WorldItemDataCmpSystem
    {
        /// <summary>
        /// 锁定 
        /// </summary>
        public static bool Lock(this WorldItemDataCmp self)
        {
            self.Locking = true;
            return true;
        }

        /// <summary>
        /// 解锁
        /// </summary>
        public static bool UnLock(this WorldItemDataCmp self)
        {
            self.Locking = false;
            return true;
        }

        /// <summary>
        /// 是否锁定
        /// </summary>
        /// <returns></returns>
        public static bool IsLocking(this WorldItemDataCmp self)
        {
            return self.Locking;
        }

        /// <summary>
        /// 析构
        /// </summary>
        public static void Dispose(this WorldItemDataCmp self)
        {
            if (self.IsDisposed)
            {
                return;
            }

            self.Position = Vector2.Zero;
            self.WorldItemForTheUnitStatus = WorldItemForTheUnitStatus.Normal;
            self.Locking = false;
            self.Dispose();
        }
    }
}