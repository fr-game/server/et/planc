using System.Collections.Generic;
using ETModel;

namespace ETHotfix
{
    [ObjectSystem]
    public class SpaceManagerComponentAwakeSystem : AwakeSystem<SpaceManagerComponent>
    {
        public override void Awake(SpaceManagerComponent self)
        {
            self.Load();
        }
    }
    
    [ObjectSystem]
    public class  SpaceManagerComponentDestroySystem:DestroySystem<SpaceManagerComponent>
    {
        public override void Destroy(SpaceManagerComponent self)
        {
            self.Destroy();
        }
    }

    /// <summary>
    /// 空间管理组件。
    /// Game.Scene挂在上这个组件就可以管理空间了。
    /// </summary>
    public static class SpaceManagerComponentSystem
    {
        public static void Load(this SpaceManagerComponent self)
        {
            self.Spaces = new Dictionary<long, Space>();
            //todo 
            Log.Info("初始化空间管理组件开始");
            Log.Debug("加载地图配置：");
            Log.Debug("加载【新手村】网格地图完成");
            Log.Debug("加载【新手村】AOI空间完成");
            Log.Debug("加载【新手村】NPC完成");
            Log.Debug("-------------------------------");
            Log.Debug("加载【降龙坑】网格地图完成");
            Log.Debug("加载【降龙坑】AOI空间完成");
            Log.Debug("加载【降龙坑】NPC完成");
            Space xinshoucun = ComponentFactory.CreateWithId<Space>(1);
            self.Spaces.Add(xinshoucun.Id,xinshoucun);
            Log.Info("初始化空间管理组件结束");
        }

        /// <summary>
        /// 根据Id获取一个地图空间
        /// </summary>
        /// <param name="self"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static Space GetSpace(this SpaceManagerComponent self, long id)
        {
            self.Spaces.TryGetValue(id, out Space space);
            return space;
        }

        public static void Destroy(this SpaceManagerComponent self)
        {
            if (self.IsDisposed)
            {
                return;
            }

            foreach (Space space in self.Spaces.Values)
            {
                space.Dispose();
            }
            
            self.Spaces.Clear();
        }
    }
}