using System.Numerics;
using ETModel;

namespace ETHotfix
{
    [ObjectSystem]
    public class SpaceTestCompnentAwakeSystem:AwakeSystem<SpaceTestComponent>
    {
        public override void Awake(SpaceTestComponent self)
        {
            self.Test();
        }
    }

    public static class SpaceTestComponentSystem
    {
        public static async void Test(this SpaceTestComponent self)
        {
            Log.Debug("测试介入");
            if (self.IsDisposed)
            {
                return;
            }
            Vector2 distance = new Vector2(50,50);
            
            Log.Info("开始疯狂进入新手村");
            long now = TimeHelper.Now();
            for (int i = 0; i < 2000; i++)
            {
                EnterUnit(distance);
            }
            Log.Info($"进入新手村2000人，耗时{TimeHelper.Now() - now}ms");

            now = TimeHelper.Now();
            EnterUnit(distance);
            Log.Info($"进入新手村第2001人，耗时{TimeHelper.Now() - now}ms");
            now = TimeHelper.Now();
            Space space = Game.Scene.GetComponent<SpaceManagerComponent>().GetSpace(1);
            
            
            Unit unit = space.GetComponent<UnitComponent>().GetAll()[0];
            for (int i = 0; i < 2000; i++)
            {
                unit.Space.Move(unit.Id);
            }
            
            Log.Info($"在新手村行走了2000下，耗时{TimeHelper.Now() - now}ms");
             
            for (int i = 0; i < space.GetComponent<UnitComponent>().GetAll().Length; i++)
            {
                
                unit.Space.Leave(space.GetComponent<UnitComponent>().GetAll()[i].Id);
            }
            
            Log.Info($"在新手村离开了2000下，耗时{TimeHelper.Now() - now}ms");
        }

        public static async void EnterUnit(  Vector2 distance)
        {
            Unit unit = ComponentFactory.Create<Unit,UnitType>(UnitType.Npc);
            unit.AddComponent<InventoryCmp>();
            unit.SpaceId = 1;
            unit.Position = new Vector3(RandomHelper.RandomNumber(1,500),RandomHelper.RandomNumber(1,500),0);
            await unit.AddComponent<MailBoxComponent>().AddLocation();
            unit.AddComponent<UnitGateComponent, long>(456789);
            Space space = Game.Scene.GetComponent<SpaceManagerComponent>().GetSpace(unit.SpaceId);
            space.EnterSpace(unit,distance);
        }
    }
}