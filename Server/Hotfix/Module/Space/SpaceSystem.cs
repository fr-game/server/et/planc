using System.Numerics;
using ETModel;

namespace ETHotfix
{
    [ObjectSystem]
    public class SpaceAwakeSystem : AwakeSystem<Space>
    {
        public override void Awake(Space self)
        {
            self.Awake();
        }
    }
    
    [ObjectSystem]
    public class SpaceDestroySystem:DestroySystem<Space>
    {
        public override void Destroy(Space self)
        {
            self.Dispose();
        }
    }

    /// <summary>
    /// 地图空间。一个Space对应一个场景。
    /// 场景下有物件，也就是Unit。使用UnitComponent管理
    /// 场景的空间使用啥管理呢？就是AOI
    /// </summary>
    public static class SpaceSystem
    {
        public static  Vector2 DefaultDistance =  new Vector2(10, 10);

        public static void Awake(this Space self)
        {
            self.space = new AoiSpace();
            self.AddComponent<UnitComponent>();
        }

        
        
        /// <summary>
        /// unit加入到新的空间
        /// </summary>
        /// <param name="self"></param>
        /// <param name="unitId"></param>
        /// <param name="aoiDistance"></param>
        /// <returns></returns>
        public static void EnterSpace(this Space self, Unit unit,Vector2 aoiDistance)
        {
            unit.AOIDistance = aoiDistance;
            unit.Space = self;
            unit.SpaceId = self.Id;
            self.GetComponent<UnitComponent>().Add(unit);
            AoiNode aoiNode = self.space.Enter(unit.Id,unit.Position.X,unit.Position.Y);
            AoiNode updateNode = self.space.UpdateNode(aoiNode, aoiDistance, unit.Position.X, unit.Position.Y);
            MessageHelper.BroadAOI(updateNode, self,unit);
        }
        
        /// <summary>
        /// 同步unit的状态给AOI
        /// </summary>
        /// <param name="self"></param>
        /// <param name="unitId"></param>
        public static void UpdateMyState(this Space self, long unitId)
        {
            Unit unit = self.GetComponent<UnitComponent>().Get(unitId);
            if (unit == null)
            {
                Log.Error($"unit[{unitId}不存在空间中]");
                return;
            }

            var updateNode = self.space.UpdateNode(unitId,unit.AOIDistance,unit.Position.X,unit.Position.Y);
            MessageHelper.BroadAOI(updateNode, self,unit);
        }
        
        /// <summary>
        /// 移动unit的位置
        /// </summary>
        /// <param name="self"></param>
        /// <param name="unitId"></param>
        public static void Move(this Space self, long unitId)
        {
            Unit unit = self.GetComponent<UnitComponent>().Get(unitId);
            if (unit == null)
            {
                Log.Error($"unit[{unitId}不存在空间中]");
                return;
            }

            var updateNode = self.space.UpdateNode(unitId,unit.AOIDistance,unit.Position.X,unit.Position.Y);
            MessageHelper.BroadAOI(updateNode, self,unit);
        }

        /// <summary>
        /// 离开空间
        /// </summary>
        /// <param name="self"></param>
        /// <param name="unitId"></param>
        public static void Leave(this Space self, long unitId)
        {
            Unit unit = self.GetComponent<UnitComponent>().Get(unitId);
            if (unit == null)
            {
                Log.Error($"unit[{unitId}不存在空间中]");
                return;
            }
            self.GetComponent<UnitComponent>().RemoveNoDispose(unit.Id);
            long[] leaveNode = self.space.LeaveNode(unitId);
            MessageHelper.BrodAOILeave(leaveNode,self,unit);
        }

        public static void Dispose(this Space self)
        {
            if (self.IsDisposed)
            {
                return;
            }

            self.space = null;
            
            self.Dispose();
        }
    }
}