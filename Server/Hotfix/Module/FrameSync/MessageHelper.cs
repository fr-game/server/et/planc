﻿using System.Collections.Generic;
using ETModel;

namespace ETHotfix
{
	public static class MessageHelper
	{
		public static void Broadcast(IActorMessage message)
		{
			Unit[] units = Game.Scene.GetComponent<UnitComponent>().GetAll();
			ActorMessageSenderComponent actorLocationSenderComponent = Game.Scene.GetComponent<ActorMessageSenderComponent>();
			foreach (Unit unit in units)
			{
				UnitGateComponent unitGateComponent = unit.GetComponent<UnitGateComponent>();
				if (unitGateComponent.IsDisconnect)
				{
					continue;
				}

				ActorMessageSender actorMessageSender = actorLocationSenderComponent.Get(unitGateComponent.GateSessionActorId);
				actorMessageSender.Send(message);
			}
		}
		
		public static void SendMsg(Unit unit,IActorMessage message)
		{
			ActorMessageSenderComponent actorLocationSenderComponent = Game.Scene.GetComponent<ActorMessageSenderComponent>();
			UnitGateComponent unitGateComponent = unit.GetComponent<UnitGateComponent>();
			if (unitGateComponent.IsDisconnect)
			{
				return;
			}

			ActorMessageSender actorMessageSender = actorLocationSenderComponent.Get(unitGateComponent.GateSessionActorId);
			actorMessageSender.Send(message);
		}
	
		public static void BroadAOI(AoiNode updateNode, Space space,Unit unit)
		{
			//todo 发送消息
			Log.Debug($"进入的人：space[{space.Id}]:Unit:{updateNode.Id}({updateNode.Position.X},{updateNode.Position.Y})");
			UnitComponent unitComponent = space.GetComponent<UnitComponent>();
			HashSet<long> updateNodeEnters = updateNode.AoiInfo.EntersSet;
			HashSet<long> updateNodeLeaves = updateNode.AoiInfo.LeavesSet;
			HashSet<long> updateNodeMovesSet = updateNode.AoiInfo.MovesSet;
			HashSet<long> updateNodeOnlyMovesSet = updateNode.AoiInfo.MoveOnlySet;
			Log.Debug($"通知这些人我来了：");
			foreach (long enterId in updateNodeEnters)
			{
				if (enterId == unit.Id)
				{
					//这是我自己
					Log.Info("这是我自己");
				}
				Unit unitt = unitComponent.Get(enterId);
			//	Log.Debug($"Unit:{unitt.Id}({unitt.Position.X},{unitt.Position.Y})");
			}
			Log.Debug($"通知这些人我离开了：");
			foreach (long unitId in updateNodeLeaves)
			{
				if (unitId == unit.Id)
				{
					//这是我自己
					Log.Info("这是我自己");
				}
				Unit unitt = unitComponent.Get(unitId);
				//Log.Debug($"Unit:{unitt.Id}({unitt.Position.X},{unitt.Position.Y})");
			}
			Log.Debug($"通知这些人我移动了：");
			foreach (long unitId in updateNodeOnlyMovesSet)
			{
				if (unitId == unit.Id)
				{
					//这是我自己
					Log.Info("这是我自己");
				}
				Unit unitt = unitComponent.Get(unitId);
			//	Log.Debug($"Unit:{unitt.Id}({unitt.Position.X},{unitt.Position.Y})");
			}
			Log.Debug($"通知周围的人我移动了：");
			foreach (long unitId in updateNodeMovesSet)
			{
				if (unitId == unit.Id)
				{
					//这是我自己
					Log.Info("这是我自己");
				}
				Unit unitt = unitComponent.Get(unitId);
				//Log.Debug($"Unit:{unitt.Id}({unitt.Position.X},{unitt.Position.Y})");
			}
			
		}

		public static void BrodAOILeave(long[] leaveNode,Space space, Unit unit)
		{
			Log.Debug($"通知这些人我[{unit.Id}]离开了：");
			foreach (long unitId in leaveNode)
			{
				Unit unitAnother = space.GetComponent<UnitComponent>().Get(unitId);
				if (unitAnother != null)
				{
					Log.Debug($"Unit:{unitAnother.Id}({unitAnother.Position.X},{unitAnother.Position.Y})");
				}
			}
		}
	}
}
