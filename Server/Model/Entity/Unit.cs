﻿using System.Numerics;
using MongoDB.Bson.Serialization.Attributes;

namespace ETModel
{
	public enum UnitType
	{
		Hero,
		Npc,
		Item
	}

	[ObjectSystem]
	public class UnitSystem : AwakeSystem<Unit, UnitType>
	{
		public override void Awake(Unit self, UnitType a)
		{
			self.Awake(a);
		}
	}

	public sealed class Unit: Entity
	{
		public UnitType UnitType { get; private set; }
		
		[BsonIgnore]
		public Vector3 Position { get; set; }
		
		[BsonIgnore]
		public Vector2 AOIDistance { get; set; }
		
		/// <summary>
		/// 所在空间
		/// </summary>
		[BsonIgnore]
		public Space Space { get; set; }

		/// <summary>
		/// 所在空间的ID
		/// </summary>
		[BsonElement("sID")]
		public long SpaceId;
		
		public void Awake(UnitType unitType)
		{
			this.UnitType = unitType;
		}

		public override void Dispose()
		{
			if (this.IsDisposed)
			{
				return;
			}

			base.Dispose();
		}
	}
}