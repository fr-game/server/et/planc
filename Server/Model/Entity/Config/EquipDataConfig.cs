namespace ETModel
{
	[Config(AppType.ClientM )]
	public partial class EquipDataConfigCategory : ACategory<EquipDataConfig>
	{
	}

	public class EquipDataConfig: IConfig
	{
		public long Id { get; set; }
		public string EquipPart;
		public string Sex;
		public long[] EquipDataNumericConfigIds;
	}
}
