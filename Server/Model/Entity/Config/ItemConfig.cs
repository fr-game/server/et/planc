namespace ETModel
{
	[Config(AppType.ClientM)]
	public partial class ItemConfigCategory : ACategory<ItemConfig>
	{
	}

	public class ItemConfig: IConfig
	{
		public long Id { get; set; }
		public string Name;
		public string PrefabName;
		public string IconName;
		public int Price;
		public string CanMany;
		public string MoneyType;
		public string Description;
		public string ItemType;
		public long LifeTime;
		public int MaxNum;
		public int UsingLevel;
		public int CDTime;
		public int CastTime;
		public string OnlyBattle;
		public long[] EffectTypes;
		public long EquipDataConfigId;
		public long UpgradeId;
	}
}
