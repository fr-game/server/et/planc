using System.Collections.Generic;

namespace ETModel
{
    /// <summary>
    /// 空间管理组件。
    /// Game.Scene挂在上这个组件就可以管理空间了。
    /// </summary>
    public class SpaceManagerComponent :Component
    {
        public Dictionary<long,Space> Spaces;
    }
}