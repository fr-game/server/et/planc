namespace ETModel
{
    /// <summary>
    /// 地图空间。一个Space对应一个场景。
    /// 场景下有物件，也就是Unit。使用UnitComponent管理
    /// 场景的空间使用啥管理呢？就是AOI
    /// </summary>
    public class Space :Entity
    {
        public AoiSpace space;
    }
}