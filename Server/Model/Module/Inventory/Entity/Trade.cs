/**
 * 交易实体
 */

using System.Collections.Generic;

namespace ETModel
{
    public class Trade : Entity
    {
        /// <summary>
        /// 发起人
        /// </summary>
        public Unit InitTrader;

        /// <summary>
        /// 交易参与者
        /// </summary>
        public Unit AnotherTrader;

        /// <summary>
        /// 交易状态 
        /// </summary>
        public Dictionary<long, TradeState> TradeStates;

        /// <summary>
        /// 交易物品和金额
        /// </summary>
        public Dictionary<long, Commodity> TradeCommodity;

        /// <summary>
        /// 创建时间
        /// </summary>
        public long CreateTime;
    }
}