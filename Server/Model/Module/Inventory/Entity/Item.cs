using System.Collections.Generic;
using MongoDB.Bson.Serialization.Attributes;

/**
 * 物品实体。
 */
namespace ETModel
{
    public class Item : Entity
    {
        /// <summary>
        /// 物品配置Id/
        /// </summary>
        [BsonElement("IcID")] public long ItemConfigId;

        /// <summary>
        /// 物品配置信息
        /// </summary>
        [BsonIgnore] public ItemConfig ItemConfig;

        /// <summary>
        /// 最后使用时间
        /// </summary>
        [BsonIgnore] public long LastUseTime;

        /// <summary>
        /// 物品的效果
        /// </summary>
        [BsonIgnore] public List<Effect> EffectList;

        /// <summary>
        /// 创建时间
        /// </summary>
        [BsonElement("CT")] public long CreateTime;

        /// <summary>
        /// 设置ItemConfig
        /// </summary>
        /// <param name="itemConfig"></param>
        public void SetItemConfig(ItemConfig itemConfig)
        {
            this.ItemConfig = itemConfig;
        }

        /// <summary>
        /// 获取物品配置信息
        /// </summary>
        /// <returns></returns>
        public ItemConfig GetItemConfig()
        {
            return this.ItemConfig;
        }

        /// <summary>
        /// 是否可用
        /// </summary>
        /// <param name="target">要给谁用</param>
        /// <returns></returns>
        public bool CanUse(Unit target)
        {
            return false;
        }

        /// <summary>
        /// 获取物品的名称
        /// </summary>
        /// <returns></returns>
        public string GetName()
        {
            return this.ItemConfig.Name;
        }

        /// <summary>
        /// 获取物品的图标
        /// </summary>
        /// <returns></returns>
        public string GetIcon()
        {
            return this.ItemConfig.IconName;
        }

        /// <summary>
        /// 根据装备的特性计算出背包中的特效
        /// </summary>
        /// <returns></returns>
        public string GetEffectIcon()
        {
            return "";
        }

        /// <summary>
        /// 使用物品
        /// </summary>
        /// <param name="target">要给谁用</param>
        /// <returns></returns>
        public bool Use(Unit target)
        {
            return false;
        }

        /// <summary>
        /// 开始倒计时
        /// </summary>
        public void CountDown()
        {
        }

        /// <summary>
        /// 是否可以叠加
        /// </summary>
        /// <returns></returns>
        public bool CanMany()
        {
            return this.ItemConfig.CanMany == "true";
        }

        /// <summary>
        /// 是否可以一次使用或者出售或者丢弃多个
        /// </summary>
        /// <returns></returns>
        public bool GetCanMany()
        {
            InventoryDataCmp inventoryDataCmp = this.GetComponent<InventoryDataCmp>();
            if (inventoryDataCmp == null)
            {
                return false;
            }

            return this.ItemConfig.CanMany == "true" && inventoryDataCmp.CountNum > 1;
        }

        //销毁事件
        public override void Dispose()
        {
            if (this.IsDisposed)
            {
                return;
            }

            Log.Debug("Item销毁开始");
            this.EffectList.Clear();
            this.EffectList = null;
            this.LastUseTime = 0;
            base.Dispose();
        }
    } //class_end
}