/**
 * 交易管理组件。客户端挂上可以发起交易，服务端挂上可以管理交易 。
 * 
 */

using System.Collections.Generic;
using System.Threading.Tasks;

namespace ETModel
{
    public class TradeMgrCmpServer : Component
    {
        /// <summary>
        /// 服务端所有的交易都在这里
        /// key：交易ID.Value交易实体。
        /// </summary>
        public Dictionary<long, Trade> CurrentTradeS;
    } //class_end

    //交易物品封装
    public class Commodity
    {
        /// <summary>
        /// 交换的金额
        /// </summary>
        public int Price;

        /// <summary>
        /// 交换的商品
        /// </summary>
        public List<Item> ItemList;

        /// <summary>
        /// 交易人
        /// </summary>
        public long TraderId;
    } //class_end
}