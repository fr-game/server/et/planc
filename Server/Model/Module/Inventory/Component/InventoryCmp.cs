/**
 * 背包组件。Unit挂上之后就可以使用Inventory功能了。
 */

using System.Collections.Generic;
using MongoDB.Bson.Serialization.Attributes;

namespace ETModel
{
    public class InventoryCmp : ComponentWithId
    {
        /// <summary>
        /// 玩家Id player对象的ID. 
        /// </summary>
        [BsonElement("pId")] public long PlayerId;

        /// <summary>
        /// 背包物品集合。
        /// </summary>
        [BsonElement("ms")] public SortedDictionary<int, Item> Items;

        /// <summary>
        /// 金币
        /// </summary>
        [BsonElement("G")] public int Gold;

        /// <summary>
        /// 钻石
        /// </summary>
        [BsonElement("D")] public int Diamond;

        /// <summary>
        /// 背包当前状态。
        /// </summary>
        [BsonElement("t")] public InventoryStatus InventoryStatus = InventoryStatus.Normal;

        /// <summary>
        /// 最大容量
        /// </summary>
        [BsonIgnore] public int MaxSize = 65;

        /// <summary>
        /// 最后整理时间。
        /// </summary>
        [BsonIgnore] public long LastSortTime;

        public override void Dispose()
        {
            if (IsDisposed)
            {
                return;
            }

            this.PlayerId = 0;
            foreach (var item in Items.Values)
            {
                item?.Dispose();
            }

            Items.Clear();
            Gold = 0;
            Diamond = 0;
            LastSortTime = 0;
            base.Dispose();
        }
    } //class_end
}