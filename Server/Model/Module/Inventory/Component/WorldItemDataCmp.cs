/**
 * 挂载了这个组件的Item就是世界物品了。
 * 就是一个在世界上可见的物品。它和InventoryDataCmp是互斥的。
 */

using System;
using System.Numerics;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization.Attributes;

namespace ETModel
{
    public class WorldItemDataCmp : Component, ISerializeToEntity
    {
        /// <summary>
        /// 坐标
        /// </summary>
        [BsonElement("e")] public Vector2 Position;

        /// <summary>
        /// 是否锁定
        /// </summary>
        [BsonIgnore] public bool Locking;

        /// <summary>
        /// 对当前Unit的状态
        /// </summary>
        [BsonIgnore] public WorldItemForTheUnitStatus WorldItemForTheUnitStatus = WorldItemForTheUnitStatus.Normal;
    }
}