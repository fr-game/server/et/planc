/**
 * 挂载了这个组件的物品就是一个可穿戴的装备了。
 */

using System;
using System.Collections.Generic;
using MongoDB.Bson.Serialization.Attributes;

namespace ETModel
{
    public class EquipmentDataCmp : Component, ISerializeToEntity
    {
        /// <summary>
        /// 强化等级
        /// </summary>
        [BsonElement("l")] public int ForgeLevel = 1;

        /// <summary>
        /// 装备普通属性集合
        /// </summary>
        [BsonElement("dl")] public List<EquipData> EquipDataList;

        /// <summary>
        /// 装备拓展属性集合
        /// </summary>
        /// <returns></returns>
        [BsonElement("del")] public List<EquipData> EquipExtDataList;

        /// <summary>
        /// 装备的镶嵌属性
        /// </summary>
        /// <returns></returns>
        [BsonElement("il")] public List<InlayData> InlayDataList;

        /// <summary>
        /// 对应的装备属性配置ID
        /// </summary>
        [BsonElement("cid")] public long EquipDataConfigId;

        /// <summary>
        /// 对应的装备属性配置
        /// </summary>
        [BsonIgnore] public EquipDataConfig EquipDataConfig;


        /// <summary>
        /// 析构（对象不用之后）
        /// </summary>
        public override void Dispose()
        {
            if (this.IsDisposed)
            {
                return;
            }

            Log.Debug("析构装备组件");
            //基础属性
            foreach (EquipData equipData in this.EquipDataList)
            {
                equipData.Dispose();
            }

            this.EquipDataList.Clear();
            this.EquipDataList = null;

            //配置信息
            this.EquipDataConfig = null;
            this.EquipDataConfigId = 0;

            //拓展属性
            foreach (EquipData equipData in this.EquipExtDataList)
            {
                equipData.Dispose();
            }

            this.EquipExtDataList.Clear();
            this.EquipExtDataList = null;

            //镶嵌
            foreach (InlayData inlayData in this.InlayDataList)
            {
                inlayData.Dispose();
            }

            this.InlayDataList.Clear();
            this.InlayDataList = null;

            base.Dispose();
        }
    } //class_end

    //镶嵌的属性
    public class InlayData : Entity
    {
        /// <summary>
        /// 物品配置Id/
        /// </summary>
        [BsonElement("i")] public long ItemConfigId;

        /// <summary>
        /// 物品配置信息
        /// </summary>
        [BsonIgnore] public ItemConfig ItemConfig;

        public string GetName()
        {
            return ItemConfig.Name;
        }

        public string GetIconUrl()
        {
            return this.ItemConfig.IconName;
        }

        public string GetDesc()
        {
            return this.ItemConfig.Description;
        }

        public void GetBuff()
        {
        }

        /// <summary>
        /// 析构
        /// </summary>
        public override void Dispose()
        {
            if (this.IsDisposed)
            {
                return;
            }

            Log.Debug("析构装备镶嵌属性");
            this.ItemConfigId = 0;
            this.ItemConfig = null;
            base.Dispose();
        }
    }

    //装备的属性
    public class EquipData : Entity
    {
        /// <summary>
        /// 对应的装备数值配置信息
        /// </summary>
        [BsonElement("i")] public long EquipDataNumericConfigId;

        /// <summary>
        /// 当前值。
        /// </summary>
        [BsonElement("c")] public double CurrentValue;

        /// <summary>
        /// 对应的装备数值配置
        /// </summary>
        [BsonIgnore] private EquipDataNumericConfig EquipDataNumericConfig;

        public void SetEquipDataNumericConfig(EquipDataNumericConfig equipDataNumericConfig)
        {
            this.EquipDataNumericConfig = equipDataNumericConfig;
        }

        public string GetName()
        {
            return EquipDataNumericConfig.Name;
        }

        public string GetDesc()
        {
            return EquipDataNumericConfig.Desc;
        }

        public double GetMaxValue()
        {
            return EquipDataNumericConfig.MaxValue;
        }

        public double GetMinValue()
        {
            return EquipDataNumericConfig.MinValue;
        }

        /// <summary>
        /// 析构
        /// </summary>
        public override void Dispose()
        {
            if (this.IsDisposed)
            {
                return;
            }

            Log.Debug("析构装备属性");
            this.EquipDataNumericConfigId = 0;
            this.CurrentValue = 0;
            this.EquipDataNumericConfig = null;
            base.Dispose();
        }
    } //class_end
}