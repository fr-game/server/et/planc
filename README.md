#planc-超级机兵

#### 项目介绍
一款2dRPG，特色是复古+现代元素的整合。
使用复古的像素画质加上现代mmorpg高级的元素（例如自动寻路、社交元素）使玩家在回忆、社交、剧情等元素中无法自拔。


#### 项目初始概念
如果《重装机兵》能和小伙伴组队消灭诺亚会不会很爽。

#### 核心玩法介绍
1.剧情
2.物品收集
3.团队合作
4.社交经商

#### 软件架构
项目为cs架构，客户端使用unity+et开发，服务器端使用et开发。
开发语言：c# 等。
客户端目标平台：pc andriod ios
拓扑结构节点：客户端、文件服务器、应用服务器、数据库。
客户端 :FairyGui + 2D Unity技术

#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1.相关美术项目、音乐素材移动到svn,地址


#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)